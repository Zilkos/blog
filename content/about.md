---
authors: ["zilkos"]
title: À propos
date: 2012-02-08
lastmod: 2018-02-09
menu: "main"
toc: true
weight: 8000
---


Parce que c'est obligé, j'indique ici les points "légaux" concernant le site. Plus bas, vous trouverez des points sur le contenus, sur la portée des propos, les licences des codes sources éventuels, et quelques dispositions pour votre vie privée numérique ainsi que quelques points sur le partage du contenu présent sur ces quelques pages.

## Mentions légales

Unixmail.fr est un service de communication au public en ligne édité à titre non professionnel au sens de l’[article 6, III, 2° de la loi 2004-575 du 21 juin 2004](http://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=JORFTEXT000000801164&idArticle=LEGIARTI000006421546&dateTexte=&categorieLien=cid). En conformité avec cet article, l’éditeur de ce service a décidé de rester anonyme et est exempt d’une déclaration [CNIL](http://www.cnil.fr/).

Ce service est hébergé par OVH:

> SAS au capital de 10 000 000 €  
> RCS Roubaix – Tourcoing 424 761 419 00045  
> Code APE 6202A  
> N° TVA : FR 22 424 761 419  
> Siège social : 2 rue Kellermann – 59100 Roubaix – France.  

Pour toutes réclamations, je vous invite à contacter l’auteur et seul mainteneur de ce service via l'icône de courriel en bas de cette page.

Pour le nom de domaine, le nécessaire a été fait auprès de l’[AFNIC](http://www.afnic.fr), les informations sur le nom de domaine du service sont disponibles via l’outil whois de l’AFNIC que [vous pouvez consulter ici](http://www.afnic.fr/fr/produits-et-services/services/whois/).

## Licence du contenu

Tous les contenus présents sur le site sont, **sauf mention contraire** sous licence Creative Commons BY-SA:

{{% center %}}
![Licence CC-BY-SA](/static/img/about/cc-by-sa.png)  
Ce(tte) œuvre est mise à disposition selon les termes de la [Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/).
{{% /center %}}

N'hésitez pas à me contacter si vous partagez du contenu, je suis disponible pour collaborer le cas échéant.

Pour les codes sources, la licence est précisée sur chaque article. Si toutefois elle n’est pas précisée, c’est la [WTFPL](http://www.wtfpl.net/about/) qui s’applique par défaut (vous avez donc tous les droits sur le code).

## Portée des propos

Tous les contenus présents sur Unixmail.fr sont à la seule initiative de l’auteur. Les opinions exprimées ici n’engagent donc que moi, et pas mon employeur présent ou mes employeurs passés.

Le contenu de ce blog est parfois corrigé ou proposé par la communauté grâce au dépôt d'articles qui est ouvert à chacun. Si vous proposez des améliorations du contenu ou du contenu de votre cru, merci de m'indiquer si la licence globalement appliquée au site vous dérange, si tel est le cas, je ferai une précision de la licence que vous souhaitez utiliser pour vos écrits. Vous aurez aussi le libre choix de voir votre article dans le dépôt selon votre convenance et d'en accepter ou rejeter les éventuelles modifications proposées par la communauté.

Aucun article est et sera un partenariat commercial.

## Tracking

Il n'y a strictement aucun système de suivi de visite sur ce site, même libre. Il n'y a pas de publicité non plus. Toutes les ressources (CSS, JS, images, vidéos), sont servies par le serveur Unixmail uniquement.

Les deux seuls cookies présents sont indépendants de ma volonté et dépendent des hébergements mutualisés OVH.

Unixmail est fortement engagé concernant le tracking numérique. Aussi, si vous ne savez pas ce que c'est, je vous invite fortement à vous documenter sur le sujet et à paramétrer votre navigateur en conséquence.

J'essaye de proposer un site aussi rapide que possible, consultable partout et sur tous les navigateurs, même en mode texte (w3m et consorts).

## Me contacter

Si vous avez une interrogation, aussi bien sur le contenu que sur les licences, ou si vous avez des idées ou des soumissions de sujet d'article, n'hésitez pas à me contacter ou à aller faire un tour sur le dépôt d'articles (lien disponible en bas de page).

Pour ceux nécessitant un peu de confidentialité, ma clef PGP est [ici](https://pgp.mit.edu/pks/lookup?op=vindex&search=0x4E914CE5D9034A4E).

Je reste également disponible sur Twitter, via le compte [Unixmail](https://twitter.com/unixmail) ou via mon [compte personnel](https://twitter.com/zilkos).

Aussi, vous pouvez me contacter via Jabber à  **zilkos@jabber.lqdn.fr**
