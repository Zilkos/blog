---
authors: ["zilkos"]
date: 2016-12-01
lastmod: 2018-11-18
title: 'Bonjour monsieur 04273'
categories: ["Entreprise"]
tags: ["Bullshit", "On en a gros"]
---

_Nous sommes une entreprise à taille humaine_. _Un employé heureux, c'est un employé productif_. Toutes ces phrases, nous les entendons souvent, tous les jours, et encore plus en période de recherche d'emploi. Oui, les recruteurs se soucient de votre bien-être. Vraiment.

## Mais vous n'êtes qu'un numéro.

Même pas un numéro sur un humain, non, juste un numéro, QUE un numéro.

Aujourd'hui on va donc parler de recrutement, principalement dans des grosses sociétés. On va aussi voir comment faire pour être aussi salopard qu'eux.

### Des processus de recrutement "humains"

Vous sortez votre plus belle plume numérique, vous rédigez votre CV de façon franche, honnête, pour partir d'un bon pied avec celui qui lira vos expériences et compétences diverses. C'est tout à votre honneur. Après tout, une société "à taille humaine" se soucie de l'humain non ?

On parle du _e-recrutement_ ?

### Mais il est trop cool ce poste !

Un poste vraiment chouette, même si le descriptif est _bullshit_ au plus haut point, avec l'habitude on finit par comprendre ce qu'ils veulent, du moins quand ils veulent quelque chose. Pas de salaire indiqué, de toute façon vous travaillez pour le plaisir, au pire vous demanderez lors de l'entretien (si vous l'osez).

Pour postuler, un formulaire web suffit, vous indiquez toutes vos coordonnées, vous joignez votre CV, votre super lettre de motivation même pas pompée sur le web et rédigée exprès pour l'occasion, parce que vraiment ce poste est cool et la boite est en plein essor.

Et vous attendez. Une semaine, puis deux.

Puis un jour, vous avez un mail de la société en question. Mail très cordial, qui indique que non, vous ne faites pas l'affaire, mais que vraiment on vous remercie tout plein pour votre candidature, que ça nous touche et qu'on sort les violons et les mouchoirs tellement on est tristes de ne pas **pouvoir** vous prendre. Le mieux étant d'avoir une phrase de fin en guise de citation, entendue dans une conférence RH (la RMSCONF d'octobre dernier), comme celle-ci : _"Un candidat non retenu pour un poste ne signifie pas qu'il est mauvais mais qu'il n'est pas le meilleur à l'instant T"_.

Si c'est pas se foutre littéralement de votre gueule quand même.

Parce que le mieux dans tout ça, c'est que personne n'a lu votre CV, et personne n'a rédigé le mail. Personne.

### Personne n'a vu votre CV

Non, vraiment, pas un seul humain n'a vu, ni lu votre CV. Vous, votre motivation, vos expériences, tout ce que vous avez comme "qualités humaines", tout ça, c'est uniquement jugé par un logiciel.

Ca se résume en trois lettres : ATS, pour _Applicant tracking system_, un simple système de suivi des candidats. Sauf qu'il fait plus que suivre, il automatise complètement le process de recrutement.

Ca se présente sous forme de logiciel tout prêt généralement en SaaS, il analyse la banque de données des CV récoltés via les postulations en ligne (seul moyen de postuler, ça permet de ne pas échapper au système), tri les CV selon les paramétrages indiqués par les "RH" ou même les "DigiRH", ça écarte toutes les candidatures jugées "éloignées" par le logiciel, ça mail automatiquement les refus, et le reste des CV est "noté".

C'est juste un algorithme qui va comparer la liste de vos expériences, diplômes et compétences avec la fiche de poste et qui va définir une note à chaque CV. Si il y a trop de CV retenus, on corse un peu les comparaisons pour affiner encore plus les résultats et avoir moins de CV en sortie. Chaque CV dispose d'un "score" et ceux-là uniquement sont regardés par un humain. D'autres logiciels vont plus loin, ils proposent également des tests via des quizz pour cerner le comportement et la personnalité du candidat. Donc que la boite reçoive 50.000 CV ou 30, le logiciel en sortira 10. Pas plus, pas moins.

C'est tout bénéfique pour eux, ils y passent nettement moins de temps parce que _"vous comprenez, on reçoit des dizaines de milliers CV par an !"_ alors que Google en reçoit quelques millions par an et qu'ils trient **tout** à la main, pour justement repérer les CV atypiques, parce qu'ils aiment ça.

Ces logiciels qui décident de votre avenir professionnel (en partie), il y en a plein, coûtent relativement cher et sont extrêmement utilisés dans les grosses boites. Certaines PME commencent également à s'y mettre...

Ces logiciels sont nombreux : CATS, Taleo (le plus connu par chez nous), Kenexa, Jobvite, ICIMS, Peopleclick, et bien d'autres...

### Soyez aussi bête qu'un robot

Il existe encore un moyen pour être presque sûr que quelqu'un lise votre CV, si on enlève les techniques violentes de soumission (qui au passage sont répréhensibles, je le rappelle). Il suffit de jouer avec le système et d'être aussi stupide que lui, tant qu'il n'est pas encore mis à jour.

Rédigez votre CV comme d'habitude. Passez la couleur de la police d'écriture en blanc et bourrez votre CV de mots clefs "invisibles" jusqu'à vomir. Vous rentrerez dans "toutes" les cases quand l'algorithme qui décide si vous allez pouvoir manger dans les mois prochains va parcourir votre CV. Mettez-y plein de bullshit, des compétences que vous ne maitrisez pas, des technologies que vous ne connaissez pas non plus, on s'en fout, le but est de parler à un humain pour exprimer vos motivations, ou du moins, qu'il voit votre CV. Et pour faire en sorte qu'il le voit, il faut un gros score. Mais le fait qu'il le voit n'implique pas forcément un entretien, hein, restons cohérents.

Le but de cette technique étant uniquement d'avoir une super note de la part du logiciel pour passer le _"pré-tri automatisé"_. Mais ensuite, si vous rencontrez un humain, sachez qu'ils sont aussi fourbes que leurs logiciels de recrutement...

### "Chez nous, on aime l'humain"

Peu importe la facade, partez du principe que tout est faux. Il n'y a qu'à se balader un peu partout, notamment dans des conférences, lire des blogs spécialisés pour se rendre compte que les personnes les moins humaines, sont ceux qui sont responsables des humains, et des relations entre ces humains.

Petit florilège de conseils donnés aux apprentis RH, pris dans des cours, blogs spécialisés et interview de RH :

  - Affiner au maximum la fiche de poste

Ca permet tout simplement de limiter le nombre de CV que sortira le robot, pour écarter toutes les candidatures atypiques, sans même laisser à la personne une chance d'expliquer son parcours.

  - Devenir un détective privé pour épelucher toute sa vie

Point sémantique, nos chers apprentis RH distinguent Facebook de Linkedin à l'aide de termes simples : Facebook est un réseau **social** alors que Linkedin est un réseau **professionnel**.
Toutes ces saloperies pseudo-sociales (ou professionnelles, au choix) sont votre terrain de chasse une fois qu'un CV a été correctement noté. Apprennez tout sur lui, qui il fréquente, quels sont ses hobbies, ses relations, soyez au courant si il est en relation avec d'autres entreprises, si il a eu des périodes de chômage. Appellez ses anciens employeurs, on ne sait jamais, c'est peut être un usurpateur, et malgré vos formations, vous n'arriverez pas à le détecter à l'entretien. Vous êtes RH, pas psychologue. Il n'a aucun compte en ligne ? Il est donc par défaut louche, pas sociable, non intégré. Au pire, investissez dans un ATS qui a une option de crawling web pour que le logiciel le fasse à votre place.

  - L'argent, c'est tabou, dans tout les cas le postulant est perdant

Si vous avez peu de budget pour le poste, dites le. Si vous avez un gros salaire à proposer, indiquez que vu le salaire qu'il va toucher, il sera disponible H24, jour et nuit, week-end, congés. Restez dans le vague avec des phrases de ce style : _"le poste nécessite un investissement personnel conséquent et de tous les instants"_.

  - Si c'est un besoin ponctuel, prenez un stagiaire

Le stagiaire est une ressource relativement qualifiée et pas chère, pour ne pas dire gratuite si le stage n'excède pas deux mois. Utilisez-le pour un besoin de courte durée.

  - Un test de personnalité qui ne laisse pas le choix

Proposez un questionnaire avec à chaque question, deux choix qui laisseront transparaitre sa personnalité : soit c'est un dominant, soit un dominé. Libre à vous de faire le choix selon le poste. Une foultitude de tests sont à votre disposition, avec des niveaux de psychologie parfois proche du bar tabac du coin. On va parler de "tests d'intelligence" (??), des mises en situation (nommées _assessment center_), tests psychométriques, tests linguistiques, tests cliniques (pour la personnalité hein, pas pour l'internement).

  - Laissez la place au doute

Le candidat à l'air bon, mais vous avez un présentiment ? Vous sentez quelque chose de louche ? Arrêtez-tout et n'allez pas plus loin. Ca peut révéler une personnalité instable et peut poser des soucis par la suite. Même si vous n'êtes pas sûr, autant ne pas prendre de risque, écartez-le.

  - Un bisou au service juridique

Un contrat de travail, ce n’est pas un document à prendre à la légère. Faites vous conseiller par un avocat ou un juriste du travail dans sa rédaction, histoire de ne pas vous retrouver coincé si ça se passe mal. Il y a aussi des éléments essentiels à ne pas négliger, auxquels les seuls à penser sont les avocats et autres juristes spécialisés dans le droit du travail et qui ont, par exemple, l’habitude des procès aux Prud’hommes.

Je te fais la promesse, Visiteur, que cette liste de "conseils" n'a pas été modifiée, un poil reformulée, mais le fond est là et est parfois bien plus "méchant" que ce que je présente ici.

Le _"recrutement digital"_, c'est de l'esclavagisme moderne 2.0.

### Fin

Cet article est volontairement orienté contre ces pratiques que je juge tout sauf "humaines", j'insiste volontairement sur certains points et pourtant, tout est vrai. Vous le savez, vous en avez probablement fait les frais. Attention par contre à ne pas généraliser. J'ai trouvé mon job actuel il y a quelques années (6 pour être précis), sur le seul fait que j'étais passionné par le domaine et autodidacte. Par contre, c'est pas un RH qui m'a recruté, mais ce qui allait devenir un futur collègue. J'espère juste que malgré ces pratiques, les personnes en chargent du recrutement disposent d'un peu de discernement pour éviter de transformer et de continuer à déshumaniser encore plus ce domaine.

N'oubliez pas également d'être cohérent, évitez de jeter la faute sur la boite quand c'est vous qui ne faites pas l'affaire. Un CV moisi reste un CV moisi, même lu par un humain.

P.S : Inutile d'arracher la chemise de votre DRH, vous pourriez prendre de la prison inutilement.

_Un merci tout particulier à une lectrice qui termine ses études dans ce domaine, en s'énervant de temps à autre sur ces sujets-là et qui fut une source intarissable sur le sujet._
