---
authors: ["zilkos"]
title: 'Du lol chez les cravateux'
date: 2018-07-07
lastmod: 2018-11-18
categories: ["Entreprise"]
tags: ["Bullshit", "Digital"]
---

Dis donc visiteur, ça fait longtemps que nous n'avons pas causé du monde de l'entreprise toi et moi. Si tu le veux bien, on va faire un brin de causette sur les quelques changements professionnels récents me concernant et comment je suis arrivé prestataire dans une entreprise. Attention, les vraies entreprises, pas les minables PME du coin qui ont encore une âme. Non, non, on va parler des... GrossBwat.

_Visiteur, dois-je te rappeler que tout ce qui se trouve dans cet article est purement fictif ?_ ;-)

## #JeSuisPrestataire

Il y a un an, j'étais sous le coup d'un licenciement économique. Je ne t'ai toujours pas raconté cette magnifique histoire, faudrait y penser un jour. En gros, je me suis gavé pendant 5 mois avec tes sous, lecteur. Mais comme rester chez moi en profitant des cotisations des contribuables pour gagner plus que mon ancien salaire c'était moyen en terme de d'acceptation de soi, je me suis mis en quête d'un travail.

M'étant fait dégager au profit d'un "prestataire" qui venait de temps à autre, je me suis dit que le bon plan pour ne pas me faire voler mon emploi une fois de plus, c'était de devenir prestataire également. Simple question de survie, et puis c'est un domaine que je n'avais jamais essayé. Je ne suis pas du genre à être effrayé par le travail, bien au contraire, j'ai touché à quelques domaines. À la chaîne dans une fonderie de Zamak, en saison dans les restaurants, barman de nuit, musicien de jour, technicien électrique, technicien informatique, formateur informatique pour les vieux, etc. Alors bon, un bon vieux boulot de prestataire informatique en pige chez un client, même pas peur !

Hop, on remet le CV à jour et on part en quête d'un employeur. Personnellement je n'ai pas choisi de candidater à des offres, mais juste d'envoyer des CV dans certaines boites pour tâter le terrain (pas d'entretien depuis 7 ans, j'étais un peu rouillé). Si toutefois il y a des gens qui veulent me rencontrer, ils me serviront d'entraînement pour les entretiens (désolé les copines RH). J'ai visé par défaut les SSII pas reluisantes, euh pardon, les ESN. En vrai c'est la même chose, c'est juste le nom qui change pour oublier la merde du passé.

J'ai donc passé des entretiens dans des boites minables avec des DRH, RH, stagiaire RH stupides qui regardaient si elles pouvaient mettre des croix dans les bonnes cases. À ma grande surprise, toutes ces boites ont voulu m'embaucher. Plutôt que de refuser de manière ferme et définitive, et pour ruser côté Pôle Emploi, j'expliquais à la personne qui s'occupait de ~~moi~~ mon dossier que ça ne l'avait pas fait pour telle ou telle raison. Bref, c'est très simple d'embobiner Pôle Emploi parce que "vous savez, dans l'informatique y'a plein de boulot, je vais trouver vite" avec une tête de premier de la classe. Même si la pression était un peu plus forte parce que j'avais un genre de pass VIP de Pôle Emploi...

Bon d'accord j'vous explique.

## Call me CSP

J'étais en CSP. Contrat de sécurisation professionnelle. Ca veut dire quoi ? Pas grand chose, pour la faire courte :

  - Tu gardes ton salaire de quand tu étais employé en guise d'indemnité chômage
  - Tes cotisations sont moindres (donc le salaire net est plus haut)
  - Ça dure un an (vraiment)
  - Si au bout d'un an tu n'as pas retrouvé de travail, tu bascules sur les ARE classiques
  - T'as open bar sur toutes les formations que tu veux, gratos
  - Une personne super compétente s'occupe de toi (_fun fact_, la personne est prestataire et est rémunérée au nombre de gens qui retrouvent un boulot grâce à leurs services)

Et la graine de sésame sur le burger au tofu, si tu retrouves un emploi dans les 6 premiers mois, on te donne en guise de prime 50% des mois restants.

Donc oui, ma période de chômage était nettement plus lucrative que de travailler. Sauf que vraiment, être au chômage, même en étant généreusement arrosé d'argent, ça ne passe pas (du moins dans mon cas). Simple lutte interne entre moi et moi. Et soyons franc, avoir un trou d'un an dans le CV à moins de 30 ans, ça ne le fait pas du tout pour la suite professionnelle. Il fallait donc trouver du travail et après m'être entraîné sur les pires SSI du marché, je suis parti en quête de la boite sympa, idéalement pas trop grosse.

Au final j'en ai trouvé une chouette, petite, avec encore un peu d'humain dedans. Bien évidemment je ne vais pas la citer, même si je l'aime énormément. Et donc, mon "commercial" m'a trouvé une mission en accord avec ce que je voulais faire, en sachant que c'était un peu bâtard comme envie, à savoir :

  - De l'adminsys sous Linux au moins à 80%
  - Un peu de virtu
  - Du dev ponctuel en Perl
  - Apprendre des choses que je ne connais pas

Les critères sont respectés (ok il y a un peu de Java dans l'équation mais rien d'inquiétant).

"Parfait !" me dis-je.

## Bienvenue chez GrossBwat'®

Chez GrossBwat, on est agiles. Agile dans le sens où on peut toucher avec sa langue le bout de son coude. En bref, on fait n'importe quoi. Plus c'est gros, plus ça passe.

Y'a du pognon chez GrossBwat, donc les postes de travail sont plutôt pépères, double écran pour tout le monde, bonne machine de travail, tout le service informatique peut être administrateur de son poste, 5 marques de machines à café différentes, un frigo, des babyfoots un peu partout parce qu'il faut bien du "bonheur au travail", bref, un environnement sympa.

Et puis un jour on te demande de déboguer la chaîne technique de traitement des salaires des internes. Sentant le test arriver, je refuse sous prétexte de confidentialité, que je suis prestataire et que les données internes sensibles sur les employés ne me regardent pas au regard de la clause de mon contrat entre la boite qui m'embauche et mon client. Après tout si on a pas de compte applicatif sur ces logiciels c'est bien pour qu'on n'y mette pas les pieds.

_"Nan mais on s'en fout."_

Ce n'était pas un test.

Donc du jour au lendemain, t'as un CSV avec les informations personnelles des gens (RIB, nom, prénom, adresse, salaire brut, net, cotisations, enfin tout ce que tu trouves sur une fiche de paye). Tout ceci ne dérange personne. Ports USB non bloqués, proxy poussif qui bloque pas grand chose et facilement contournable, en bref, on préfère sécuriser avec une armée d'avocats une fois le mal fait plutôt que de sécuriser en amont.

Et puis les aberrations continuent, avec des choses encore moins sécurisées, des scripts perl a déboguer écrits en 2002 et toujours en production, des process lourds que peu de monde ne comprend, quand enfin arrive le jour sacré, celui où tu as suffisamment fait tes preuves et que par chance la main d'oeuvre manque : on te met sur un projet. Non pas parce que tu le mérites, juste il faut fournir de la viande, et il se trouve qu'on en a sous la main.

## Jean-Michel Digital

T'es invité par Jean-Michel, 55 ans, à une réunion de lancement qu'on prendra soin de mettre sous la forme un acronyme du style "COLPRO" pour Comité de Lancement de Projet. T'as jamais croisé Jean-Michel, parce que d'une part son bureau est vide depuis 14 ans car il passe son temps en réunion et surtout parce qu'il ne parle pas au bas peuple (donc toute personne inférieure à son grade). Jean-Michel n'aime pas les externes, vous serez donc "une ressource du projet" avec un joli qualificatif en fonction de vos obligations, du style "l'architecte système", "l'ingénieur système", "l'intégrateur", "le concepteur", etc.

Content qu'on t'invite enfin à sociabiliser avec tes compères, tu files en réunion, avec ton t-shirt de geek et ton bloc-notes. Et oui, tu as un poste fixe, toi, donc pas un ordinateur portable.

Entouré d'encravatés, tout le monde se présente. Ils sont tous plus ou moins chef de quelque chose qui se rapporte "au digital". Jean-Michel arrive avec 10 minutes de retard parce qu'il était occupé dans une autre réunion de chefs ultra importante, pose son ordinateur portable, branche l'HDMI, se connecte au hotspot wifi et... branche le câble réseau. Bien évidemment, chez GrossBwat Powerpoint est une religion et tout le monde est expert en présentation. 48 _slides_ bardées de texte en police 10 avec des cliparts des années 90 et Jean-Michel qui passe 45 minutes à lire ses _slides_ de manière monotone. Tous les autres participants font autre chose sur leur machine portable pendant que toi tu peaufines tes techniques de handspinning au dessus de ton bloc-notes désespérément vierge.

À la fin, tour de table. Chacun dit ce qu'il fait, du moins ce pourquoi il est là s'il en a une idée. Pour les plus peureux ou les externes on se contentera d'un "NOM - FONCTION". Rassurez-vous, personne ne se souviendra de votre nom.

## L'innovation agile

Agile. L'agilité chez GrossBwat, c'est un chef de projet transverse, un chef de projet technique, un chef de projet fonctionnel et un chef référant par domaine (réseau, BDD, dev, intégration, déploiement, etc.) et une armée de sous-chefs à la con, sans compter les responsables de service et autres gueux qui viennent se greffer au projet parce que _"j'ai plus rien à faire et que le chef de projet transverse, il me le faut dans la poche"_. Hors de question de parler au N+2, il n'entend que si c'est ton N+1 qui lui parle. Pourquoi ? Tout simplement parce qu'il ne connaît rien de technique, donc il préfère parler avec un autre chef qui ne connait rien plutôt que d'avoir honte devant un simple bout de viande.

Donc les décisions techniques, les architectures, sont décidées par des gens qui ne font pas la différence entre AIX, Linux ou Windows. Pour des gens comme eux, un ingénieur système qui va installer quelque chose, c'est juste _"cliquer 3 fois sur suivant"_.

Git, c'est beaucoup trop rigide, les versions sont gérées avec Sharepoint et la doc est faite avec Word, accessible sur un lecteur réseau. Pas de cahier des charges à ta portée, t'installes ce qu'on te demande. Même pas besoin de réfléchir.

Au moindre problème, il faut montrer que Jean-Michel est le patron. Un souci de compatibilité avec une bibliothèque quelconque ? Un mail de trop à la MOA et hop, c'est un "point de blocage". Et là Jean-Michel, on ne l'arrête plus.

Réunion de crise avec énormément de monde d'invité, plan d'action et tout, ça mobilise plein de gens pour pas grand chose car quand on te demande le problème technique que tu rencontres, tu réponds de la manière suivante :

"_On utilise une version un peu ancienne de la bibliothèque Z qui permet de faire X. L'éditeur pensait qu'on était sur une version corrigée et stable, et non pas sur une version obsolète avec des failles de sécurité, donc faut qu'il adapte son produit."_

Et immédiatement, on lance un appel d'offre pour changer d'éditeur parce que lui, il est nul, il ne sait même pas gérer "_le truc des livres_" (véridique...).

Surtout, ne jamais se poser de question ni même remettre en cause l'existant pour essayer d'aller de l'avant.

Et tout le long du projet (on parle en années), ça va être comme ça. Des réunions où tout le monde perd son temps pour des sujets mineurs et incompris. Interdiction formelle d'émettre des modifications pour essayer de faire les choses mieux, plus sécurisées, plus fiables, plus souples. Juste tu la fermes et tu fais ce qu'on te demande.

Au final chez GrossBwat c'est pas compliqué, t'as juste à cliquer 3 fois sur Suivant.

Et toi Visiteur, tu es probablement client de cette GrossBwat.

Dans le coup, le 31 mai, j'ai démissionné et en septembre je commence quelque chose de nouveau, donc je te parlerai sous peu ici-même.

Porte-toi bien Visiteur, ma vie change mais je pense toujours à toi. <3

_Inutile de t'indiquer que les [ML](/communaute) sont disponibles si t'as besoin !_
