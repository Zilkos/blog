---
authors: ["zilkos"]
title: 'On peut discuter ?'
date: '2018-11-21'
toc: 'true'
categories: ["Pensées"]
tags: ["Libre"]
mathjax: false
emoji: false
---

Salut Visiteur, à la base je voulais faire un article sur les gilets jaunes (il est presque terminé d'ailleurs). Mais bon, je me suis dit qu'ils s'en prennent assez comme ça, ce n'était peut-être pas utile d'en rajouter une couche. Donc on va parler d'un autre sujet, il s'agit de l'état de la mentalité des communautés du libre, ou comment essayer de parler avec des gens...

## Disclaimer

C'est dans ces moments-là que je suis content d'avoir un blog trop merdique pour proposer des commentaires, parce que généralement ça fait crier. Je tiens donc à rappeler que c'est **uniquement** mon avis, je parle au nom de personne d'autre que moi, si tu n'es pas d'accord sache que tu as le droit de ne pas être d'accord. Sache également que tu as tout à fait le droit de ne pas lire cet article. Tu es donc **libre** de faire ce que tu veux, tant que ça ne nuit pas à autrui (et non, les cochons n'ont rien avoir là-dedans).

Ces saloperies d'usages étant émises, on peut gueuler en toute légalité.

## C'est triste

Non, plus sérieusement que dans le *disclaimer*, c'est quand même assez triste. Les blogs parlant du libre ne sont plus aussi nombreux qu'il y a quelques années. Pour en tenir un depuis environ 2012, franchement c'est compliqué. Bien pour ça que je l'ai refait, sans doute trop de fois pour avoir un lectorat stable. Le dernier tri dans les articles m'a clairement indiqué que les temps sont durs, surtout quand je vois le peu d'article qu'il reste. Vous le savez, je n'ai même pas de statistique de visite, tout simplement parce que mon plaisir ici, c'est d'écrire quand j'en ai envie. Même si personne ne lit, ça m'est égal. De toute façon j'ai bien conscience qu'il n'y a pas grand chose d'utile sur ce blog et que ça prend la douce tournure d'un journal geeko-intime avec une qualité éditoriale digne d'un *skyblog*. Mais promis, je fais des efforts (c'est faux).

C'est triste parce que donner son avis sur telle ou telle chose est devenu un engagement signé avec son sang, si bien que les gens qui veulent discuter calmement préfèrent se taire, de peur de rentrer dans une joute verbale inutile et interminable. Je passe les Jean-Michel Précis qui feront tout pour ne pas être d'accord avec toi, juste pour le fun. J'ai arrêté depuis longtemps de répondre à ces gens-là, et pourtant j'en croise encore (sur Twitter notamment). C'est pourtant pas compliqué de discuter calmement, d'émettre des arguments, de préciser des choses. Sauf que cette discussion n'est pas du tout compatible avec les outils d'aujourd'hui (comme Twitter par exemple), tout simplement parce qu'expliquer un point de vue sur un sujet prend du temps, des caractères et qu'actuellement c'est mieux de lancer une phrase assassine en 200 caractères que de prendre le temps de pondre un pavé gentil pour expliquer le pourquoi du comment. Magie de l'instantanéité qui au final réduit les sujets à peau de chagrin si bien qu'on n'a quasiment rien à dire sur un sujet tellement il est étriqué de part sa formulation originale. La "discussion" devient donc un champ de bataille avec des assertions en guise de boulets rouges.

## Et après ça vient chialer avec des codes de conduite

En quelques mois, énormément de projet ont été obligés de mettre en place des codes de conduite pour pallier les ressentis de tout le monde, et notamment de ceux qui pensent être harcelés par des mots. Attention, je ne dis pas que personne ne subit rien, juste que parfois, il faut faire la part des choses et prendre un peu de recul.

Même sur ce blog, je pense que certains se sentent oppressés par manque d'écriture inclusive. Je ne pratique pas cette écriture et je n'en ressens pas le besoin, non pas parce que je suis un anti-féminisme crapuleux, mais juste parce que je ne parle pas à des femmes ou à des hommes, je parle à des humains. Que tu sois noir, blanc, gris, vert à pois jaunes, que tu fasses l'amour avec des hommes, des femmes, des anges ou des chèvres, je n'en ai rien à foutre, tu es avant tout un **humain**, et je m'adresse à cet **humain**, peu importe ce que tu as entre les jambes ou dans la tête. Je ne te connais pas, donc je ne changerai pas de mot sous prétexte que "*suicide*" ça te rappelle le suicide de ta tante et que donc tu te sens oppressé. C'est juste une histoire de contexte, mais certains prennent trop souvent la mouche et certains autres la cherchent un peu trop, et donc des dérives arrivent.

Certaines personnes stupides jouent de ça et provoquent, au final elles ne valent pas mieux que celles qui se sentent outrageusement oppressées par une phrase du style *"J'ai de la chatte, ça a fonctionné du premier coup".* ou celles n'étant pas capable de faire la différence entre de l'humour et de vrai propos. Et au pire si ça vous titille, précisez-le à votre auditoire, si la personne ne change pas sa manière de s'exprimer, c'est qu'il est inutile d'essayer de débattre calmement avec elle, ne perdez pas votre temps.

Donc pour calmer tout ça, pour aseptiser les relations de tout ce beau monde, il faut des codes de conduite pour normaliser "la façon de se conduire" avec les autres. Un genre d'épée de Damoclès qui va vous trancher la tête au moindre écart ou à la moindre parole mal interprétée par votre interlocuteur.

Franchement ?

Vous ne discutez avec personne dans la vie ? Vous n'avez jamais blessé quelqu'un par mégarde ou méconnaissance de son passé ? Sommes-nous forcément obligés de normaliser le moindre mot d'un commit si bien qu'on passe plus de temps à écrire le message du commit que le code pour être absolument certain de ne blesser personne ? C'est trop compliqué de respecter la bienséance même si c'est chiant ? C'est trop compliqué d'appliquer certaines règles pour faire taire les gens qui n'arrivent pas à se conformer (oui c'est un CoC implicite social) dans une discussion d'adulte responsable ?

Pour répondre à cette stupidité, certains font des codes de conduite qui indiquent qu'il n'y a pas de code de conduite : [No Code of Conduct](https://github.com/domgetter/NCoC) (ou autre [règle de ce genre](http://sl4.org/crocker.html)) tout simplement.

> 1. We are all adults. Capable of having adult discussions.
>
> 2. We accept everyones contributions, we don't care if you're liberal or conservative, black or white, straight or gay, or anything in between! In fact, we won't bring it up, or ask. We simply do not care.
>
> 3. Nothing else matters!

En gros, on discute entre gens adultes, avec des **humains**, peu importe leur orientation sexuelle, couleur de peau, avis politique ou autre. Tous réunis autour d'un projet, si vous n'êtes pas capable de mettre de côté votre homophobie, racisme ou autres tares pour quelque chose que vous aimez faire parce que Bidule est noir et Martine est bisexuelle, posez vous des questions.

Donc on fait la part belle à la personnalité de la personne, à sa manière de s'exprimer et d'expliquer des choses, au lieu de tout normaliser, aseptiser. La diversité, c'est bien ce qui fait la richesse de l'humain non ? Discutez avec les gens que vous n'aimez pas ou que vous pensez différents de vous, faites cet effort et vous verrez qu'au final vous aurez peut-être des choses à apprendre d'eux. 

## Ouais super, et le libre dans tout ça ?

Justement, les gens. Ça fait déjà quelques temps que je ne participe plus à rien, tout simplement parce que je suis agacé du type de personne dont on a parlé au-dessus.

#### Puritains et rigoristes libristes

En toute honnêteté je l'ai été quelques temps, dans mes jeunes années (la fougue inutile et bruyante d'un connard qui sait mieux que tout le monde mais qui en fait, n'en sait pas tant que ça). Sauf que certains approchent la quarantaine et qu'ils restent mordicus dans ce mode de pensée, hurlant à qui veut l'entendre l'impureté de tel ou tel logiciel, manière de faire, voire même, manière de penser.

Cette catégorie de personne intervient rarement du côté des débutants (typiquement le forum Ubuntu pour les nouveaux venus par exemple) et tant mieux. Même si certains s'y risquent, quelques administrateurs veillent au grain pour ne pas repousser le nouvel utilisateur venu pour découvrir le libre. Par contre, dans des coins un peu plus avancés, ils sont nombreux et polluent toute forme de discussion avec des assertions émanant de l'enfer parce que la machine qui a servi à graver ton processeur ne dispose pas de plan sous licence Creative Commons pour construire la même dans ton garage.

Le pire c'est que certains blogueurs le font et j'en ai probablement fait partie. Bien évidemment, je tairais les noms ici, le but n'étant pas de désigner, d'accuser ou quoique ce soit d'autre de néfaste. Il ne me semble pas utile d'insulter ou de caractériser avec violence les utilisateurs de certaines distributions sous prétexte que *"faut vraiment être con pour compiler tous les paquets dont on a besoin, c'est juste des gros nazis libristes barbus qui ne sortent jamais de chez eux"*. Ça existe, des gens produisent bénévolement ces logiciels et d'autres y trouvent leur compte en les utilisant, alors à quoi bon mettre le doigt dessus de manière agressive ? Tu ne t'en sers pas parce que tu n'en as pas besoin, soit, ce n'est pas une raison pour stigmatiser ceux qui l'utilisent dans le plus grand des silences.


#### Jean-Michel Contredit et Grand Inquisiteur du Libre

Quoique vous fassiez, sachez que c'est pas bon, nul, mauvais. Dans le coup, vous ne faites plus rien et ces mêmes personnes vont crier que rien n'avance parce que plus personne contribue.

Ce type de personne qui a tellement la science infuse qu'il sait mieux que vous que gNewSense **est** ce qu'il vous faut pour ne pas que votre âme soit pervertie par quelconque système privateur.

Ce type de personne qui va te gueuler dessus parce que pour te simplifier la vie tu vas utiliser une application avec une bibliothèque non libre qui gère du JSON mieux que toi tu peux le faire.

Ces mêmes personnes qui utilisent des [services propriétaires et surveillés](https://linuxfr.org/sondages/quel-fournisseur-de-courrier-electronique-utilisez-vous) mais quand c'est eux, c'est en parfaite connaissance de cause. Si c'est vous, vous êtes une sous-merde, un [traitre à la cause](https://www.unixmail.fr/histoire/proselytisme-ou-traitre/) qu'il faut mettre au bûcher. Attachez-le à un poteau et fouettez-le avec la licence GPL. 

Ces mêmes gens qui passent tout leur temps libre à expliquer aux gens pourquoi ils ont tort, à emmerder le moindre CM d'une marque sur Twitter pour faire un rappel à l'ordre concernant le RGPD, concernant un cypher qui est supporté dans la sécurité de la connexion alors qu'il ne devrait pas, et j'en passe... Certes, dites les choses qui ne vont pas, mais vous n'êtes pas obligés d'assortir ça d'insultes gosier grand ouvert en place publique.


## Bref, faut pas s'étonner

On ne va pas faire toutes les catégories de pénible qui existent, j'aimerai bien dormir cette nuit.

Donc le monde du libre est pollué, certains arrêtent de contribuer, pas forcément au niveau du code, mais en terme de contenu présentant et vulgarisant le libre, que ce soit via des blogs ou des chaînes Youtube. Je ne sais pas pourquoi ces personnes ont une telle portée, une telle visibilité et un tel pouvoir de nuisance. Il me semble bien qu'il y a 10 ans il y avait aussi ce genre de personne agaçantes et pourtant, on n'y prêtait moins attention.

Peut-être qu'à force d'embêter le monde, le monde s'en va et seuls les pénibles restent ?

Dans le coup on fait comment ? On continue de se taper dessus comme des gaulois un soir de beuverie autour d'un sanglier (désolé les végé*) ou on discute comme des humains adultes et responsables pour faire avancer le chariot sur les rails (sans diesel hein) ?

Honnêtement je ne sais pas, mais quand certains blogueurs / Youtubeur libristes lâchent les armes et partent presque en dépression (peu importe la qualité de leur contenu), ou que certains autres ferment les commentaires de leur blog à cause de pénibles beaucoup trop nombreux, il y a franchement de quoi se poser des questions, et *a minima* de commencer à en discuter sérieusement entre gens adultes.

Argh, j'ai écrit cet article avec Visual Studio Code pour le tester, et [c'est pas libre](https://carlchenet.com/le-binaire-de-visual-studio-code-nest-pas-du-logiciel-libre-voila-pourquoi/)...

Bon bah je reviens, je vais me flageller.

À plus.