---
authors: ["zilkos"]
title: 'Prosélytisme ou traite à la cause ?'
date: 2018-03-17
lastmod: 2018-11-18
categories: ["Pensées", "#MaVie"]
tags: ["Libre"]
---

Ça fait déjà un certain temps que je ne milite plus pour une informatique libre ou d'autres sujets connexes, comme la protection des données personnelles ou  l'hygiène numérique. Ma réflexion sur ces sujets n'arrive pas à avancer, donc je couche ça sur un article pour y voir plus clair. Ne vous attendez pas à une argumentation complète, bien articulée. Ça sera divers points çà et là pour résumer des choses et éventuellement faire apparaître d'autres articles une fois la réflexion un peu plus poussée. C'est aussi l'occasion d'ouvrir un débat à ce sujet si vous le souhaitez.

### Non, je ne milite plus

Ce n'est pas parce que je fais un blog qui respecte vos libertés numériques que je milite. Ce n'est pas parce que je suis sous GNU/Linux que je milite. Ce n'est pas non plus parce que j'ai du LineageOS sans Google sur mon téléphone que je milite. Mettons nous d'accord sur une chose: Utiliser n'est pas militer. "Wow, VLC est installé partout le monde se libère !", non la majorité des utilisateurs ne savent pas que VLC est libre, et n'ont pas connaissance de ce qu'est un logiciel libre. Des gens utilisent un lecteur multimédia non libre ? "Cramez les !" Je ne vais pas m'abstenir de parler à des gens parce qu'ils utilisent tel produit ou logiciel. Ceux qui font ce genre de choses sont des gens stupides et frustrés. C'est stupide parce qu'outre finir seul, ça renvoie une mauvaise image de la cause que la personne pense défendre. Au final elle ne défend rien, pire elle agave l'image des acteurs de cette cause, acteurs qui peuvent être nettement moins stupides que ces personnes frustrées.

Expliquer n'est pas militer. Certes ça peut en faire partie, tout dépend de comment l'explication se déroule. Il y a une différence entre expliquer factuellement les choses et tomber dans l'angoisse ou instiguer la peur à l'interlocuteur. Ce qui me dérange chez certains libristes acharnés, c'est qu'ils ne laissent pas le choix. C'est forcément noir ou blanc. Si tu utilises tel truc t'es mon ami, si tu utilises ça tu es un traître et donc banni à tout jamais. Forcer les gens dans un sens n'apporte rien de bon sur le long terme mis à part les frustrer. En se sentant obligés, ils vont perdre des choses, sans comprendre ce qu'ils gagnent. Et tout le souci est là: ils ne comprennent pas.


### J'explique

Donc non, je ne milite plus, j'explique. J'explique aux gens pourquoi j'utilise ceci, pourquoi je raisonne comme ça, sans parler d'idéologie. Je ne les juge pas. Je leur explique pourquoi je n'utilise pas tel objet connecté parce que les risques sont les suivants. J'indique que ce sont des risques, et non pas une réalité avérée (sauf pour certains cas) et qu'en ce qui me concerne le risque est tellement élevé que je ne préfère pas le prendre. Je ne leur gueule pas dessus directement en leur sommant de jeter leur dernière acquisition.

J'explique simplement les risques à utiliser tel logiciel, équipement ou service en ligne. Et j'explique **uniquement quand on me demande mon avis**. En premier lieu de manière globale, puis si la personne est curieuse, je vais plus loin. Avec des mots français, des phrases compréhensibles et sans trop de jargon technique. S'ils ne sont pas réceptifs, j'arrête et je ne leur bourre pas le crâne avec des propos qu'ils ne veulent pas entendre. Certaines personnes sont conscientes des risques qu'ils prennent, mais ce risque est vu comme un "paiement" pour utiliser les produits ou services. "Je prends le risque" est une phrase qui sort assez souvent de la bouche de ces personnes. Au moins elles savent ce qui peut éventuellement se passer, elles en sont conscientes et accepte que ça peut mal finir. À ce moment la personne est donc au courant et décide en pleine conscience. C'est souvent que ces personnes reviennent vous parler de ces fameux risques après quelques temps, juste le temps que vos explications mûrissent et que les gens réfléchissent à la chose. Il suffit de leur laisser le temps au final, plutôt que de les confronter à un choix sur l'instant "Utilise ou n'utilise pas".

Certes, ça fait mal de voir des personnes ruiner leur vie privée et leurs données personnelles juste par simple "effet de mode", mais c'est leur choix. Ce choix il faut le respecter qu'il vous plaise ou non. Insister dans le sens "prive toi de ce service, ça sera bon pour toi" est inutile. La personne sera frustrée de passer à côté de quelque chose sans voir concrètement ce qu'elle gagne à ne pas l'utiliser. Au final, c'est ça qui est compliqué...


### Le concret

Dites à un enfant têtu de ne pas toucher une plaque de cuisson brûlante, s'il le fait ne serait-ce qu'une seule fois, il s'en rappellera. Tout simplement parce que la réponse est immédiate : ça brûle. Une action qui entraîne une réponse immédiate indiquant "Non, vraiment, ne fait pas ça".

Se priver de certains produits et services pour mieux se protéger soi-même n'apporte pas de bénéfice visible immédiat. Utiliser ces services n'apporte pas de grosse baffe visible et immédiate. Ça ne "brûle" pas de suite.

Un peu comme la cigarette. "Fumer tue". Non, fumer ne tue pas, du moins pas à l'instant où je vais allumer une cigarette. Fumer va me tuer dans quelques années si j'ai de la chance, d'une mort lente et douloureuse. Pas de réponse négative immédiate, le danger paraît moins réel et immédiat.  
Donc il faut rendre le danger concret pour que les explications deviennent percutantes et paraissent légitimes, non nébuleuses, non "idéologiques". Certains le font très bien, via des reportages du type "Nothing to hide" ou d'autres expériences dans des cafés vie privée ou _crytoparties_. Je ne parlerai pas des GULL, vu que j'ai eu que des expériences désastreuses à ce niveau-là. Je n'en fais pas une généralité mais j'ai trouvé ça beaucoup trop politisé pour être agréable, accueillant et ouvert. Donner un PC reconditionné sous Linux avec un tract de la France Insoumise, c'est stupide, manipulateur et propagandiste (peu importe le nom sur le tract hein). Je suis apolitique et me retrouver dans ce genre d'endroit qui au final souille la cause première (logiciel libre) est encore une fois contre-productif. Certes, tous les GULL ne sont pas comme ça, je le sais bien, c'est pourquoi j'en cherche encore un qui serait "propre" et neutre politiquement parlant.


### La fourberie du tournevis

> Pour la majorité des gens, l'informatique est un outil. Un peu comme un moyen de transport ou un outil pour bricoler. Ce n'est pas leur passion. Donc de base, quand on commence à aborder les risques d'utiliser tel ou tel produit, ils commencent déjà à s'en foutre royalement. C'est un peu comme si quelqu'un me parlait de tournevis pendant 10 minutes, j'en ai strictement rien à carrer. Je veux juste visser, m'en fiche du reste.

Le paragraphe du dessus est un discours qu'on entend souvent. Trop souvent. Parce que ce discours il est complètement faux aujourd'hui.

Un tournevis j'en utilise souvent oui. Le seul souci est le suivant :

Je ne demande pas à mon tournevis de gérer mes comptes en banque, mes documents, je ne lui confie pas mes plus grands secrets, je ne gère pas mes impôts avec, ne gère pas ma vie professionnelle, ni toutes mes correspondances administratives ou personnelles.

Donc la différence entre le numérique est le tournevis, c'est que le numérique **est partout**. Il régie vos vies, vos études et vos relations. Alors arrêtez de penser que l'informatique est juste un outil.

Plus "l'outil" fait de choses et plus la surface de risque est grande. Mettez-moi sous surveillance quand j'utilise un tournevis ou un ordinateur, vous verrez nettement que l'un apportera beaucoup d'informations que l'autre.

---

Tout ceci pour dire que le grand public doit prendre conscience de ça. L'informatique est un élément primordial de nos vies, qu'on le veuille ou non. Expliquons donc aux gens qui le souhaitent de manière rationnelle, sans idéologie, sans prosélytisme, en leur laissant le choix de décider ce qu'ils veulent. S'ils sont suffisamment intrigués par vos propos, la réflexion fera son chemin et il y a fort à parier qu'ils reviendront vers vous.

Ils font l'autruche ? Ils ne veulent pas changer ? C'est leur problème. Tant que vous avez expliqué clairement les risques, avantages et bénéfices, ils sont maîtres de leurs choix et vous n'avez pas à les juger pour cela.

#### Mot de la fin

Comme dit en introduction, la réflexion n'est pas terrible, part dans tous les sens. Ce n'est ni très construit ni très argumenté mais ça reste un début.

À suivre.
