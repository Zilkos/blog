---
authors: ["zilkos"]
title: "Smartphone: L'expérience personnelle - Episode 1"
date: 2013-12-27
lastmod: 2018-11-18
categories: ["Expériences"]
tags: ["Smartphone"]
---

J’entame cette nouvelle année avec une expérience, la première que je relate sur Unixmail. Cette expérience, c’est me séparer de mon smartphone !

En cette fin d’année, sans doute un peu venteuse pour nos amis bretons (Demat !), j’ai dans l’idée d’entamer cette nouvelle année avec une petite expérience personnelle que je vais relater ici. Ça touche pas beaucoup à l’informatique ou à la plupart des sujets abordés ici, certes, c’est donc plus du #MaVie qu’autre chose, avec un poil de (vieille) technologie.

En fait, c’est plus du ressenti par rapport aux outils qu’autre chose. Je suis dans une période ou informatiquement parlant, j’ai fait le tri dans les outils utilisés et j’ai volontairement orienté mes choix sur les fonctionnalités, même si les GUI ou fonctionnalités « lolesques » son moches / absentes (le terminal, c’est la vie).

Le juste nécessaire est donc, à mon sens, vital pour avancer correctement et perdre le moins de temps possible en futilité. Un joli bureau 3D qui, quand on change de bureau, tourne poussivement et fait fumer le processeur, je trouve ça dommage quand un simple `Alt+Ctrl+T` vous ouvre un nouvel onglet dans votre émulateur de terminal favori (oui, c’est pas super bon pour votre sociabilité par contre). Un clicodrôme, c’est chiant, les raccourcis clavier c’est mieux.

Et si il était possible d’appliquer ces principes dans la vie et notamment dans la téléphonie ?

### Bon, c'est quoi ton expérience ?

Voir si je suis atteint de FOMO et voir si l’utilisation que j’ai de mon smartphone est judicieuse, nécessaire, utile et/ou éventuellement néfaste. En gros: puis-je me passer d’un smartphone, et est-il est néfaste pour moi ? Est-ce que je gagne du temps ou est-ce que j’en perds ? Et mes données, sont-elles en sécurité ?

### Atteint de quoi .. ?

De FOMO, pour _Fear of missing out_ qui représente la peur de manquer quelque chose. Typiquement, ça touche bon nombre d’utilisateurs de téléphone portable et de réseaux sociaux et plus largement de bidules électroniques. Si vous avez connu les Tamagotchis, qui faisaient fureur dans la cour de récré quand je portais des culottes courtes, c’est typiquement un virus FOMO pour le propriétaire du machin. Tu le regardes toutes les 10 secondes pour savoir si ton machin a fait popo dans un coin ou si la bestiole couine parce qu’elle a faim. En bref, tu vis dans la peur que ta saloperie de bestiole crève sans prévenir.

Un exemple (oui, j’aime les exemples): Quand vous regardez une émission qui rend con grâce à votre super téléviseur qui a une durée de vie volontairement limitée, si sur 1h30 d’émission vous ne checkez pas vos mails, Twitter, TroncheLivre, insérez un réseau social ici, félicitations. Soit l’émission était intéressante, soit vous n’êtes pas atteint de FOMO mais vous aimez vous faire du mal avec la télévision.

C’est typiquement le tic qui vous fait sortir votre téléphone intelligent de votre poche toute les dix minutes pour regarder si vous n’avez pas de notifications. Ce même tic qui vous fera appuyer 42 fois sur le bouton refresh de votre client mail. Ce même tic qui fera que le monde s’arrêtera de tourner lorsque vous entendrez un bruit plus ou moins ressemblant à un quelconque avertissement sonore informatique (SMS, notification en tout genre). Si après ce bruit, s’en suit de brusques mouvements pour ne vraiment pas rater ce qui se passe sur votre bidule électronique, en effet, soit vous avez un problème psychomoteur soit vous souffrez de FOMO.

Si le premier geste, une fois votre fondement levé de votre lit, c’est de vérifier téléphone / mail / réseau sociaux avant même de donner des croquettes au chat, vous êtes vraiment atteint (le pauvre chat, vous ne le méritez pas).

### Ces téléphones qui nous gouvernent

Ça fait quelque temps que j’y pense. Quelques mois que mon téléphone intelligent m’énerve au plus haut point. Vas-y que je suis un obèse avaleur d’électricité malgré les réglages qui font que je vois mon écran uniquement la nuit tellement j’ai réduit l’éclairage. Strictement RIEN n’est synchronisé. Non, la batterie ne dépasse pas 3 jours.

Le fait que tout le monde crie « _Hourra !_ » quand tu entame ton 4ème jour sans chargement par un quelconque miracle, NON CE N’EST PAS NORMAL. On peut absolument tout faire avec un téléphone intelligent, de se repérer sur la planète Terre jusqu’à courtiser des donzelles en ligne tout en envoyant une super recette de gâteau au chocolat à sa grand-mère. Sans compter le fait qu’on peut prendre des super photos pendant une soirée (trop) arrosée, lesdites photos qui seront bien entendu envoyées sur 14 réseau sociaux en même temps et uploadées sur 300 serveurs partout dans le monde tout en prenant le soin de tagguer les personnes apparaissant dessus et en indiquant les coordonnées GPS de la prise de vue.

Alors oui, c’est super, c’est génial, mais déjà [qu’on nous la met bien profonde](http://www.pcinpact.com/news/85026-la-lpm-promulguee-conseil-constitutionnel-ne-sera-pas-saisi.htm), inutile de donner des armes aux ennemis.

Il y a quelques jours, j’ai entendu quelqu’un qui découvrait les historiques de géolocalisation Google. Pour faire court, ça vous affiche une map avec des traits dessus. Les traits sont vos déplacements et visiblement cette saloperie est bougrement précise. Heureusement que par soucis de batterie, dès que je voyais « Géolocalisation », je le désactivais sur mon super téléphone intelligent, ce qui fait que mon historique est vierge (et tant mieux). Bon de toute façon, je ne risquais pas grand chose, je n'avais pas de compte Google sur mon téléphone (merci Cyanogen Mod).

Pour faire court, ces téléphones sont des caméras sur pattes épiant vos moindres fait et gestes, mangeant chaque cliché ou vidéo que vous pourrez prendre, absorbant et mettant en base le moindre contact que vous indiquerez, base qui sera évidemment redondée aux quatre coins de la planète, dont la plupart des grandes puissances de ce monde auront accès.

Mais franchement, c’est pratique quand même un téléphone intelligent.

### Il suffit

Je vais donc me séparer de mon téléphone intelligent pour quelque temps. Juste le temps d’une expérience. Alors vous allez me dire
>Euh, ouais, des téléphones qui peuvent pas aller sur le web, c’est rare quand même, et c’est encore plus rare quand ils n’ont pas d’appareil photo !

Justement.

C’est un peu ma revanche sur la vie. J’adorais ce machin quand j’étais (beaucoup) plus jeune, et je ne l’ai jamais possédé. Vengeance est mienne, je viens de le commander (matérialisme quand tu nous tiens…).

Attention, voilà les caractéristiques du monstre, que dis-je de LA BÊTE:


*  Dimensions: 113 x 48 x 22 (mm)
*  Poids 132 grammes
*  Autonomie: 260h en veille et 4h30 en communication
*  Écran monochrome 84 x 48 pixels
*  Fréquences: Bi-bande 900/1800 Mhz
*  GPRS: GSM
*  EDGE: Non
*  3G (UMTS): Non
*  4G: Bah non, forcément..
*  Pas de wifi, pas de GPS pas d’UMA, ni appareil photo / vidéo, ni lecteur MP3, ni radio.
*  Pas de carte mémoire

Pour info 260h ça fait un peu plus de 10 jours. L’iPhone 5 indique 225h en veille. Merci toutes les apps qui synchronisent tout en temps réel pour descendre votre autonomie à environ 2 jours.

Pour faire court: SMS, APPELS ( et composition de sonnerie aussi, ça biche quand même !).

J’ai nommé, le **NOKIA 3310** ! \o/

Il fait téléphone, mais peut aussi servir de marteau ou d’arme contondante pour se défendre.

Avec ça, point de vidéo de chat, point d’email ou point de « _‘tain mais le métro ça chlingue le rat crevé_ » sur Twitter.

Je l’ai commandé, je vous tiendrai au courant quand il sera arrivé, et que la bête sera en fonctionnement.

> "Oui, si je donne ça à mon papa, il va s’en contenter hein ! L’est moisie ton expérience !"

J’ai un quart de siècle et suis assez actif sur un réseau social, j’ai plusieurs comptes email et suis très accro à ce mode de communication. Je communique beaucoup via divers moyens. Bref, j’utilise mon smartphone comme toutes personnes aimant les nouvelles technologies et travaillant dans le domaine informatique. On verra bien ce que ça donnera et si un smartphone m’est réellement utile (si je ressens un manque durant l’expérience donc).

Je partagerai au fil du temps mon expérience avec ce genre d’outil, mes frustrations, les éventuelles limites induites et toutes autres choses intéressantes ou rigolotes. Aussi, je vais changer de forfait. Adieu la data, je m’oriente sur le forfait à 0€ pour les abonnés Free ou équivalent chez mon opérateur actuel. Inutile d’engraisser un opérateur si je n’utilise pas le service (oui, le 3310 est désimlocké !)

Pour le deuxième épisode, [c'est par ici](/histoire/smartphone-experience-personnelle-ep2).
