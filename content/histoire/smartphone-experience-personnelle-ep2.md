---
authors: ["zilkos"]
title: "Smartphone: L'expérience personnelle - Episode 2"
date: 2013-12-27
lastmod: 2018-11-18
categories: ["Expériences"]
tags: ["Smartphone"]
---


Deuxième épisode de l’expérience, dans celui ci, je reçois le téléphone utilisé durant le prochain mois. Je l’ai commandé le 23, il est arrivé le 26 au matin chez moi. Voyons voir la bestiole. Retour sur la première semaine d’utilisation…

> Pour le premier épisode, [c'est par ici](/histoire/smartphone-experience-personnelle-ep1).

## Le grand déballage

### Le colis

Un Colissimo tout ce qu’il y a de plus banal, du papier bulles et au final le colis logeait dans ma boîte aux lettres, malgré le foisonnement de saloperies de prospectus.

A l’ouverture du papier bulles, une boite noire estampillée Nokia. Un autocollant avec le modèle du téléphone, son numéro IMEI et un code-barres. Packaging sympa, basique pour un téléphone mais efficace.

A l’intérieur de la boite, tout ceci:


* Un téléphone Nokia 3310 remis à neuf et désimlocké
* Une batterie neuve sous plastique
* Une coque neuve mise sur le téléphone
* Un clavier neuf mis sur le téléphone
* Un chargeur (secteur) neuf dans son emballage d’origine
* Un chargeur allume-cigare neuf dans son emballage d’origine (offert)
* Une housse de protection (offerte)
* Une facture à mon nom avec garantie de 6 mois en dehors de la boite du téléphone.

Un envoi soigné, et bien que le téléphone soit reconditionné, ils ont pris soin de remplacer ce qui devait l’être (la coque, la batterie, le chargeur). Donc je suis vraiment satisfait à l’ouverture de ce colis, le contrat a été respecté, l’envoi a été rapide et le matos est propre pour ne pas dire neuf.

### Première mise en service

Ayé. J’éteins mon téléphone intelligent après avoir exporté ma liste de contacts sur ma carte SIM, et oui, point de carte SD ou de mémoire quelconque, le répertoire de contacts est forcément stocké sur la carte SIM sur ce vieux modèle.

J’ouvre l’arrière du téléphone intelligent, sors la carte SIM. Je prends le neuf, l’ouvre, y insère la carte SIM puis la batterie et referme le tout. Une pression sur le bouton d’allumage en haut du téléphone et moins de 3 secondes ensuite il me demandait mon code PIN.

Code PIN OK, le téléphone fonctionne. Juste ce côté me plait. Si ton téléphone est éteint et qu’il te reste 20 secondes à vivre, tu as deux solutions:

1. Tu passe les 20 dernières seconde de ta vie à attendre que ton téléphone intelligent boot.
2. Tu passe les 17 dernières seconde de ta vie au téléphone avec qui tu veux (de préférence les secours) si tu possède un Nokia 3310 ou une bouse du genre. Ok en partant du principe que le contact décroche rapidement.

Inutile de copier quoique ça soit, le répertoire mange ce qu’il faut sur la carte SIM directement, les contacts sont déjà présents sur le répertoire. Il ne reste plus qu’à le configurer, bien que de base il est largement bien configuré pour un usage normal.

### Configurations

Paradoxalement, j’ai été surpris par le tas de configuration possible. De mémoire mon ancien _Ericsson T10_ en possédait largement moins. Sur un téléphone, je ne conçois pas que l’écran d’accueil d’affiche pas l’heure. Une fois réglée, l’heure apparait en haut à droite du petit écran. Ça c’est juste primordial, étant donné que je pars souvent sans montre au poignet.

Les sonneries, elles font franchement saigner des oreilles ! J’en ai choisi une discrète en attendant d’en composer une encore plus discrète (si toutefois une sonnerie monophonique puisse être discrète…). Le passage entre les différents modes (sonnerie, vibreur, silence complet) est rapide, il suffit d’une courte pression sur le bouton d’allumage. Ces modes sont paramétrables dans tous les sens, franchement agréable.

Et puis… le Snake quoi !

### Première (longue) soirée d'utilisation

J’ai eu la chance et le bonheur d’avoir une très longue soirée à cause d’aléas non maitrisés, et donc j’ai longuement conversé avec un ami par SMS (une sorte d’exutoire sur le moment !). J’avoue que taper en mode ABC, c’est foutrement agaçant d’y revenir, surtout quand on avait un T9 adaptatif avec différentes dispositions de clavier azerty / qwerty. Bref, le T9 est efficace tant qu’on ne sorte pas du dictionnaire Réforme 1990.

J’ai pas encore trop cerné le principe de rangement des SMS. Point de « discussion », là où sur un téléphone intelligent on a une entrée dans le journal SMS avec une loooongue discussion si on appuie sur la trogne du correspondant, avec le Nokia 3310 on se retrouve à une entrée par SMS. Ça devient vite le foutoir dans la liste si bien qu’il faut supprimer régulièrement si on veut garder un semblant de rangement. Je me trompe peut-être, vu que je n’ai conversé qu’avec une personne pour le moment. A voir à l’usage.

> Notez qu'on ne peut pas supprimer des SMS en masse, c'est un message par un et avec une confirmation à chaque fois...

Ce point fait réfléchir au caractère "inutile" de certaines conversations parfois...

### Les frustrations

Ce qui me "frustre" le plus, là où je ressens presque un manque de type FOMO, c’est pour les mails. Je suis fortement accro à ce mode de communication, mais surtout, en ne pouvant pas les consulter à volonté, j’ai surtout peur de ne pas être au courant d’un problème d’un utilisateur, qu’il soit d’Unixmail ou d’ailleurs (oui, j’ai d’autres occupations que ça quand même !). Ça, j’ai vraiment du mal. Ce matin je me suis rué sur mon laptop pour directement violer Claws pour qu’il me sorte mes mails.

Au final, je n’avais pas raté de mail.

Je pense que ça viendra avec le temps, il va falloir apprendre que tout n’est pas si instantané que ça dans la vie, si un utilisateur mail à 23h, il peut attendre 9h le lendemain pour que son problème soit réglé. J’aime pas hein… mais là, si je n’ai pas de laptop sous la main (chose rare, ceci dit), il attendra.

Concernant Twitter, aucun manque. Même mieux, ça m’auto-régule et ça m’évite de tweeter tout et n’importe quoi. Je ne suis pas un twitto spammer. Je n’ai même pas passé la barre des 8000 tweets. Je m’en sers beaucoup en lecture, pour de la veille principalement, puis pour troller le vendredi aussi.

Pas d’appareil photo. Ça, pour l’instant ça ne me dérange pas, mais le jour où je vais tomber sur un truc qu’il faut absolument envoyer par MMS à un ami, ça va grandement me hacher l’entrejambe. Je le sais déjà. Idem, je ne peux pas en recevoir… suffit de répondre qu’ils envoient leur photo par mail… que je consulterai sur mon laptop… plus tard.

## Conclusion

Une prise en main rapide et intuitive (même si j’ai duckduckgoté pour savoir comment verrouiller le clavier j'avoue). Au final, il s’adapte encore très bien à notre époque et réponds à plusieurs besoins, certes basiques. Cette première soirée d’utilisation était plaisante.

J’attends la suite et vous raconterai les futures frustrations que je rencontrerai. Là c’est la découverte, donc c’est sympa, mais est-ce qu’il va se révéler pas pratique avec le temps ?

On verra…

Ha, j’ai commencé à subir les premiers dénigrements sociaux ce matin, me faisant caractériser de passéiste et anti-technologique. J'aborderai ce point dans le prochain épisode !

Pour le troisième épisode, [c'est par ici](/histoire/smartphone-experience-personnelle-ep3).
