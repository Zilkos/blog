---
authors: ["zilkos"]
title: "Smartphone: L'expérience personnelle - Episode 3"
date: 2013-12-27
lastmod: 2018-11-18
categories: ["Expériences"]
tags: ["Smartphone"]
---

Troisième et dernier épisode de cette expérience après un mois d’utilisation d’un téléphone pas smart du tout !

> Pour le premier épisode, [c'est par ici](/histoire/smartphone-experience-personnelle-ep1).

> Pour le deuxième épisode, [c'est par ici](/histoire/smartphone-experience-personnelle-ep2).

## Retour à la vie normale

### Un mois déjà

Et oui, un mois (même un peu plus) que je me suis séparé de mon smartphone super intelligent, tactile et socialement acceptable, contre un Nokia 3310 tout pourri.

Mon Nokia 3310 m’accompagne désormais partout de 8h à 23h. Oui, je l’éteins la nuit, là où mon smartphone était allumé. Pourquoi ? Je ne sais pas, je sors comme excuse "j’ai pas de mails à synchroniser sur le 3310, vu qu’il peut pas" alors que rien ne m’empêchait d’éteindre mon smartphone durant le sommeil, typiquement c’est du FOMO. Aussi, un 3310 met moins de 2 secondes à démarrer pour être utilisable, contre beaucoup plus contre mon smartphone (normal, les fonctionnalités ne sont pas les mêmes).

Il m’a fallu du recul pour me rendre compte que même en ayant une utilisation minime d’un smartphone se résumant à SMS / MMS / Mail (et un peu de Twitter), j’étais atteint (légèrement) de FOMO. Je n’ose même pas imaginer à quel point peuvent être dépendantes certaines personnes, sans forcément s’en rendre compte. Ça peut paraitre bête pour ceux n’ayant que peu de recul, mais je vous garantis qu’on gagne en liberté sans cette saloperie qui vous dérange pour un oui ou pour un non.

Je vous passe le passage sur la protection des données privées car je n’ai aucune preuve, mais je me doute que le pillage de données privées est incessante sur smartphone, tout simplement rapport à la masse des données qu'il peut contenir. Encore récemment (avant-hier, en fait), le Guardian révélait que la NSA utiliserait de célèbres "jeux" comme Angry Birds pour arriver à leur fins. Je vous passe la localisation et les historiques Google et bien évidemment, on ne parle pas d’Apple hein, vous savez bien ce qu’ils font et vous l’acceptez si vous utilisez ces produits.

### Utilisation courante

J’ai réussi à fixer mes habitudes d’utilisation du téléphone. Ça vient naturellement quand j’en ai besoin. Je ne le consulte uniquement quand il sonne, et de temps en temps pour connaitre l’heure, si j’ai oublié ma montre. On est donc vraiment dans l’utile et non pas dans le futile, on est dans le nécessaire et non pas dans l’abus inutile d’utilisation. Autant vous dire que mon esprit reste plus « focus » sur les choses importantes.

J’ai réussi à l’utiliser dans le juste nécessaire, sans perte, sans superflu, chose complètement impossible avec un smartphone (bon ok, j’ai battu mon record au Snake sur les toilettes). Niveau batterie, c’est toujours la folie, à savoir une semaine complète de batterie, je ne le recharge uniquement le weekend. J’ai récemment découvert qu’il gérait à la perfection le double appel. C’est d’une rare clarté (une liste d’appels en cours, 2 boutons non obscurs et explicatifs).

J’économise du temps et de l’énergie pour pouvoir le consacrer à autre chose maintenant que les frustrations d’avoir hypothétiquement raté quelque chose sont complètement envolées. Twitter peut vivre sans moi, les mails peuvent attendre, même en ayant un service à rendre à des personnes 24H/24 (j’ai très souvent un PC avec moi qui me permet de voir si tout va bien). Quand je communique, c’est efficacement et pour une raison bien précise, et non pas pour des choses insignifiantes.

J’en avais déjà parlé dans un épisode précédent, mais l’appareil photo du smartphone, franchement ça manque. C’est souvent que je croise des choses ou situations que j’aimerais envoyer via MMS, mais je ne peux pas. Bon, certes, c’est pas handicapant, mais des fois ça peut être utile (pour garder une preuve de quelque chose, niveau juridique ou exploit personnel).

### Regards extérieurs

Une partie dont il faut que je vous parle absolument tellement c’est drôle (avec du recul du moins).

Sachez que le smartphone c’est la norme (de toute façon, vous étiez déjà au courant, non?). Je travaille dans l’informatique, comme vous le savez sans doute, ce qui implique toujours des:

> Bah t’façon t’as les derniers trucs high-tech non ?

ou autre

> T’as forcément un iPad chez toi !

Quand on vient dépanner un utilisateur (même si c’est pas tellement mon corps de métier) avec un 3310 posé nonchalamment sur le bureau, on est pas crédible. De nombreuses phrases fusent telles que: "Bah, c’est quoi cette vieille merde ?", "Tu peux communiquer avec ce machin ?" ou encore "Nan mais t’es sérieux d’avoir une bouse comme ça ? Tu peux même pas aller sur Internet avec !".

Oui, le smartphone c’est la norme pour être sociologiquement acceptable, pour éviter de se taper les quolibets des gens qui critiquent mais qui ne se posent pas la question de savoir pourquoi avoir ce type de téléphone, surtout qu’ils savent pertinemment qu’aucun opérateur n’en vendent encore.

Le plus drôle c’est quand je rétorque "Tu fais quoi de plus que moi, avec ton smartphone à 650€ ?" , car dans 95% des cas, ils font la même chose que moi à savoir des choses basiques, et aucun ne pense à me citer le GPS, chose dont l’absence me dérange, seul point en plus qu’ils pourraient marquer dans le débat.

Donc ça s’oriente sur un schéma souvent similaire qu’on peut résumer en quelques étapes, en ce qui concerne #LesGens (je n’aborde pas la partie surveillance):

* Je me rends compte que j’ai la même utilisation que toi, mais avec un téléphone plus cher.
* Je me rends compte qu’il me coute beaucoup plus cher que le tiens, pour rien de plus.
* Je critique ton téléphone car il est vieux, et fonctionne sans doute mal.
* Au final, je comprends ta position, mais je reste sur le fait que "posséder" c’est mieux.

Oui, "posséder" un smartphone est la norme. C’est pas tant l’utilisation ou autre chose, c’est le fait qu’aujourd’hui, vous devez avoir un smartphone, autrement, vous n’êtes pas crédible, ni inséré dans un groupe de gens, peu importe leur âge. Même si vous êtes incapable de configurer une boite mail dessus, c'est pas grave, tant que vous en avez un.

### Accumulation

Couplez le fait d’avoir un "téléphone de merde" avec le fait de ne pas avoir de compte Facebook et de refuser de communiquer/travailler avec des gens sur des systèmes centralisés de types Google Drive, et c’est bon, vous êtes un vrai terroriste pour la société actuelle. Sans même exposer une idée, une démonstration ou une explication de vos convictions, vous êtes catalogué, et comme toujours, les gens ne cherchent pas à comprendre.

Pour terminer, tout ce que je peux vous dire, c’est au moins d’essayer quelques temps (à minima 3 semaines selon mon expérience). Si vous arrivez à survivre, alors passez le pas et séparez vous de votre smartphone.

La liberté, ça passe aussi par le fait de ne pas se faire asservir par un objet et par les fausses relations liées à cet objet.

Si vous passez le pas, félicitation, vous êtes complètement sorti du moule à la con que vous impose la société.

>>>>>> [Consultez l'épisode bonus de cette expérience](/histoire/smartphone-experience-personnelle-ep-bonus)
