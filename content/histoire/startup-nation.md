---
authors: ["zilkos"]
title: 'Startup Nation'
date: 2018-10-29
lastmod: 2018-06-29T19:30:00+06:00
categories: ["Pensées"]
tags: ["#MaVie"]
toc: true
---

Et oui, encore un billet sur l'entreprise. Plus globalement, je vais vous expliquer mes récents changements professionnels, parce que ça fait longtemps que je n'ai pas fait d'article.

### Quelle vie

Un peu de chamboulement, une démission, un déménagement, pas d'accès internet. Bref, du changement, même si ces changements ne sont pas tous souhaitables ni même décidés. À vrai dire certains sont plutôt subis, c'est la vie (de merde). Enfin, pas de connexion internet ce n'est pas très vrai. Sauf si on considère qu'une carte SIM 4G dans un boîtier chinois non sécurisé qui génère un réseau wifi poussif est une connexion internet. Bref, j'ai un minimum de connexion quand il fait beau, surtout pour assurer les choses basiques et vitales, du genre gérer le déménagement et sa suite, donc de l'administratif chiant et inintéressant. Comble du manque de bol, je n'ai pas de carte wifi sur mon PC de bureau, donc je reste à temps plein sur le laptop, ça me convient plutôt bien au final quand le hotspot chinois est conciliant du moins.

C'est bien, ça fait réfléchir sur les usages et ça force à utiliser l'ordinateur que quand c'est nécessaire, dans un but précis. Ça aussi, ça fait partie des choses à voir par la suite, à savoir rationaliser l'utilisation de tous ces trucs, des services et d'autres choses. L'idée m'est venue en dévorant le blog de [Cyrille BORNE](https://cyrille-borne.com/) qui apparemment va vers un minimalisme plutôt appréciable, autant en terme de matériel que de service. Juste me passer d'un lave-vaisselle, faut pas pousser non plus (surtout que j'en ai chié à le déménager donc bon).

### Démission

[Dans le dernier article](/histoire/digital-whore/), je vous indiquais que j'avais posé ma démission le 31 mail dernier. Fatalement et selon ma convention, j'ai été libéré de mes fonctions au 31 août.

Pourquoi ?

Comme je l'ai expliqué dans le dernier article, j'ai de plus en plus de mal avec la hiérarchie éloignée, stupide et qui ne fait pas confiance en ceux qui ont les mains dedans, donc qui sont le plus à même de remonter des axes d'amélioration, de rationalisation, d'industrialisation. Les remonter, en discuter et qu'on me réponde _"Non, c'est une idée de merde"_ je suis d'accord avec ça si c'est argumenté. On n'a pas forcément que des bonnes idées (probablement à force d'avoir trop la tête dedans justement). Par contre qu'on ne réponde pas, qu'on réponde _"On s'en fout, fait ce qu'on te demande et ça suffira"_ ou autres choses condescendantes ne passe pas.

A l'école, pendant mes années d'étude on nous répétait à qui voulait l'entendre que _"Vous êtes ingénieurs, vous êtes donc force de proposition, proposez, essayez d'améliorer les choses"_. Certes, sauf que j'ai travaillé dans quelques entreprises différentes, dans des secteurs différents, et j'ai la vague impression que rien ne changera jamais sur ce point. Travailler avec des menottes et une caméra braquée sur vous, ça suffit. Je suis plutôt souple, sympa, je prends la critique si elle est fondée et j'essaye de m'améliorer du mieux que je peux. Me faire exploiter à faire des tâches stupides, vides de sens, et dont 60% d'entre elles sont de l'administratif _"parce que comme ça on a les fesses propres, en cas de problème on lève les mains et on dit c'est pas nous"_, juste non.

Ajoutez à ça le bullshit à tous les niveaux, de gens qui parlent, parlent, parlent tout le temps pour ne rien dire au mieux, au pire pour dire des inepties. Des gens surpayés qui ne font rien à part des Powerpoints et des réunions vides de sens dont tout le monde se fout. Grand bien leur fasse d'accepter leur inutilité flagrante, d'accepter leur vie uniquement conditionnée par l'argent qu'ils gagnent, peu importe comment. Je ne veux pas de ça.

J'avais un salaire vraiment honnête pour la région, même plutôt très confortable. Mon seul revenu a suffi pour que l'agence immobilière accepte notre dossier (madame étant en sortie d'étude). Juste gagner de l'argent à faire des trucs chiants ? Je n'ai pas spécialement d'attrait pour l'argent, le minimum me suffit pour vivre, n'étant pas d'un naturel dépensier. C'est donc avec une immense joie que j'ai quitté mon travail. À en croire mes supérieurs et mes collègues (que je revois depuis), je faisais du super boulot, personne n'avait rien à redire et peu comprenait mon choix de partir.

Je passe bien évidemment sur les aspects de bonheur au travail, de "temps de cohésion" qui n'est simplement que du formatage pour que tu fermes bien ta gueule et que tu fasses ton boulot sans sourciller (en vrai, soyez plus discrets franchement). Partir du principe de base que les gens **peuvent** s'entendre sans les forcer à s'entendre, c'est trop demander ? Tellement de paranoïa pour faire en sorte que tout se passe bien est risible, voire même flippant tellement ça ressemble à de la propagande. Un peu comme s'il y avait une chape de non-dits dans tous les couloirs, presque palpable. Le premier jour de mon arrivée je me souviens encore de la seule phrase que j'ai sorti, seul dans le couloir: _"Y'a un truc qui cloche ici..."_ Un peu comme un bâtiment vide de personnalisation, dans des couleurs neutres. Presque comme les décors dans le film _Her_, tous lissés, calmes, presque cliniques, angoissants à terme.

### Le problème, c'est moi

J'ai bien conscience que le problème vient de moi (comme dans beaucoup d'aspects de ma vie d'ailleurs). Je ne peux pas accepter que mon travail soit con, vide de sens, sans intérêt, mis à part engraisser des fortuné sur trois niveaux au-dessus de moi. Je n'arrive pas à accepter cette hypocrisie ambiante à tous les étages, la fourberie de savoir qui va faillir pour le lyncher en place publique.
J'ai essayé, pendant huit ans. Et à chaque fois c'est la même chose. Certains y arrivent et en sont conscients quand je leur en parle. Ils acceptent que leur boulot ne soit pas foufou mais bon, ça permet de manger donc on sert les fesses huit heures par jour puis ça passe. J'ai également conscience que c'est un **privilège** de pouvoir faire ce boulot. Même si en soit ce n'est pas une question d'argent, vu que je n'ai pas déboursé un centime pour mes études d'informatique (ni mes parents hein), c'est donc accessible à qui veut bien l'entreprendre. Certains ne peuvent pas et ont vraiment des boulots pourris, chiants, j'en conviens. Donc je me plains mais bon, on trouve toujours pire.

Le but étant que votre boulot vous convienne, si vous avez la chance d'en avoir un. S'il ne vous plaît pas, mais que vous avez la flemme d'en changer, ne venez pas vous plaindre.

Donc si le problème c'est moi et cette inadéquation entre ma personnalité et l'environnement professionnel dans lequel j'évolue, il faut que quelque chose change. Soit moi, soit l'environnement professionnel. Comme changer d'environnement professionnel, ça commence légèrement à m'agacer et que ça prend du temps, j'ai pris le problème à l'envers.

### Si ça n'existe pas, crée-le

J'ai donc créé mon travail. Dans la mouvance macroniste du moment, _"J'ai monté ma boite"_. Non vraiment j'en ai rien à foutre de la politique, de la mouvance du moment ou autre. J'ai vu ça d'abord comme une expérience, d'une part parce que c'est un domaine que je ne connais pas trop (et tout ce qui s'y rapporte comme la comptabilité, les factures et tout ça), d'autre part parce que c'est un peu ma seule solution pour prendre l'air quelques temps.

Si les structures posent problème, je m'en affranchis. Je n'ai pas de hiérarchie, pas de collègues que je vois tous les jours, pas de locaux aseptisés.

Attention, ce n'est pas forcément tout rose. Déjà en terme de loi, on ne sait jamais ce qui va nous tomber dessus et donc possiblement flinguer la possibilité de vivre décemment d'une activité. Il y a quelques temps, l'auto-entreprise était pratique en guise de deuxième activité. Maintenant, cela peut être une activité à temps plein, plutôt rémunératrice si vous vous y prenez correctement. Autre désavantage, les fiches de paye. Vous n'en avez pas (de base), c'est bien pour cette raison que j'ai déménagé avant de ne plus en avoir. Pour en avoir, mais en restant "indépendant", vous pouvez opter pour le portage salarial, moyennant une perte financière (le coût du service au final, rien de plus normal), vous avez un CDI, des fiches de paye et un comptable qui gère votre affaire en contrepartie d'un pourcentage de votre chiffre.

En fait, je travaille pour l'état, seul intermédiaire non compressible. Je me suis fait à l'idée. C'est toujours mieux que de bosser pour une ESN, qui me vend à un client, qui lui-même me vend à un autre client, en payant l'état à tous les niveaux. C'est certes un point négatif, mais en contrepartie, je bénéfice d'avantages non négligeables, condition unique de ma liberté, à savoir que je travaille pour qui je veux, où je veux et au tarif que j'ai décidé, dans le domaine que j'ai décidé. J'y vois plus d'avantages que d’inconvénients, je sais que c'est risqué, c'est bien pour ça que c'est une expérience. Je ne sais pas du tout si ça va durer, mais je voulais le tenter. J'ai eu plusieurs indices, pour ne pas dire incitations à travailler en dehors de mon cadre de salarié. J'ai donc enquêter et contacter plein de gens pour voir si d'autres étaient intéressés par ce service et il se trouve que oui. Je me suis donc lancé, sans trop savoir, sans filet hormis quelques économies de côté le temps que ça démarre.

### Concrètement tu fais quoi ?

De la formation. J'ai toujours été attiré par la pédagogie, par transmettre les choses que je sais ou que j'apprends même encore aujourd'hui. Je suis donc formateur indépendant, ex ingénieur système. Avoir un savoir c'est bien, le partager c'est encore mieux, et être payé pour faire ça, c'est vraiment chouette. Je n'ai pas l'impression de travailler. Travailler dans le sens "subir le système", le fameux travail où tu n'as pas le choix de le faire, t'es obligé, si tu veux manger. Je pars donc du principe que je n'ai pas un travail, mais "une activité rémunératrice". Le mot travail est beaucoup trop connoté négativement dans mon esprit. Quand j'étais à la chaîne dans une fonderie, c'était du travail. Quand je dénudais des fils électriques 8h par jour en faction, c'était du travail. Quand je servais des demis ou des cocktails à des gens sans respect jusqu'à 4h du matin, c'était du travail. Quand j'ai repris mes études, c'était déjà moins présent, mais au fur et à mesure, je vois bien qu'il y a un problème entre la liberté que je recherche et le dogmatisme institutionnel présent.

Travailler moins aussi. Je m'en cache pas, le but était aussi d'avoir plus de temps pour moi et mes autres passions. Je ne jouais que rarement de la musique, le blog est depuis trop longtemps à l'abandon, j'ai deux milliards de logiciels et de trucs à tester. Je n'ai pas besoin de faire 35h par semaine tout le mois pour dégager un salaire. Quelques jours suffisent pour générer un SMIC. Parfois il y en a plus et parfois il n'y a rien. Le côté variable de l'activité n'est pas du tout rassurant c'est certain, je suppose que c'est aussi le prix de la liberté. Je suis plutôt content de ressortir le saxophone, la guitare, le piano et bientôt une batterie électronique.

D'ailleurs, ma structure me permet une flexibilité que je vais essayer de mettre à profit pour enseigner autre chose que l'informatique : la musique. Probablement la batterie, vu que c'est un instrument que je pratique depuis que j'ai huit ans, après pas mal d'année en conservatoire dans ma jeunesse. Ainsi je pourrai réunir mes deux passions dans une activité rémunératrice où je ne subis rien, hormis la lourdeur administrative. Et encore, je ne me plains pas, en microentreprise c'est bien plus allégé qu'en SARL ou qu'un autre type de société.

### C'est compliqué ?

Premièrement, d'un point de vue personnel, c'est très bizarre. C'est-à-dire qu'après avoir démissionné, c'est le mode _"Oh putain mais qu'est-ce que j'ai fait, j'ai plus de salaire, j'existe plus pour personne, merde, merde, merde"._ Cette période se calme après un peu de temps. Même si le projet a été préparé pendant quelques mois, les doutes et les peurs sont là, la peur que ça ne fonctionne pas est permanente et on se rappelle que c'est notre choix, une chose voulue, donc ça va mieux. Libre à moi de me sortir les doigts pour que ça fonctionne.

Techniquement, créer une microentreprise (oui, on dit microentreprise depuis janvier 2018, avant c'était auto-entrepreneur) c'est cinq minutes sur un site web. Bon là aussi, ça parait facile, mais il y a pas mal de choses à prendre en compte, en terme de protection de patrimoine si vous avez des dettes un jour (donc des gens à payer), en terme d'impôt (taxe libératoire de l'impôt sur le revenu) mais des gens ont déjà calculé ça pour vous. Fouiner dans des blogs, des chaînes Youtube pour avoir de l'information. J'ai eu de la chance en terme de timing. En janvier 2018, le statut a beaucoup été modifié, donc plein de gens ont fait du contenu rapport à ces modifications, donc j'ai pu profiter d'une information fraîche sans perdre trop de temps. Honnêtement je n'ai pas grand chose à craindre. Je n'ai pas d'investissement, personne à payer, rien à acheter, donc le seul risque c'est uniquement que ça ne fonctionne pas et que je sois obligé d'arrêter.

Et puis après c'est juste lancer la machine. Trouver des clients, que j'avais déjà avant de créer ma structure, en trouver d'autres et proposer des choses, puis faire son job. Former des jeunes. Le jeune.

L'entourage voit ça comme une aventure, où il faut un courage à toutes épreuves, que ça doit être compliqué et tout ça. Non, vraiment ce n'est pas compliqué, suffit juste de vouloir le faire comme on souhaite le faire et de se donner les moyens d'arriver là où on le souhaite.

### Comment atteindre ces enfants ?

{{% center %}}
![Comment atteindre ces enfaaaaaants ?](/static/img/histoire/cartmanez.jpg)
{{% /center %}}

Je suis formateur dans les domaines suivants :

 * Administration système (Linux, virtu, docker, etc)
 * Développement (système, applicatif et web)
 * Modules connexes (cloud, ergonomie, etc)

Le tout pour des étudiants de bac +2 à bac +5. Actuellement, je suis surtout intervenu pour des bacs +2 et +4, profil système, développeur ou réseau. On ne va pas se mentir, le niveau n'est pas là où je l'attendais. À vrai dire, il est plutôt bas. Pas forcément en informatique pure, mais surtout sur le reste...

Le niveau d'expression, orale ou écrite, la curiosité, l'expérimentation. Tout ça n'est pas là et c'est plutôt rageant dans le métier qu'ils vont faire. Quand je demande qui est là par choix et que j'ai les deux tiers de la classe qui lève la main, déjà, je pense qu'il y a un problème. Mais quand les deux tiers au final n'en savent pas plus que le tiers restant, qu'ils n'ont pas le goût de la recherche, de la bidouille et qu'ils attendent passivement qu'on transmette les informations et la maîtrise de ces informations, ça m’attriste.

Peu d'entre eux font de la veille, peu d'entre eux s'intéressent à la chose, aux nouveautés, ils restent dans la superficialité de la chose. Je trouve çela inquiétant de former des gens qui n'ont pas plus envie que ça. Autant au lycée ou avant, tu subis et tu n'as pas le choix de ce qu'on t'enseigne, autant là, tu es là pour ça. De plus t'es payé pour le faire, car la majorité des gens en face de moi sont des apprentis, parfois dans des très grosses sociétés.

J'essaye de leur donner ce goût de bidouiller les choses, de détourner les choses pour en faire de nouvelles, de construire des choses, des services, même pour leurs potes. Non, ils préfèrent utiliser une page Facebook dédiée la promotion, un Exchange ou un partage sur Google. Je me souviens quand j'étais étudiant dans cette même école, on avait un mec qui gérait tout seul un serveur privé Lineage (un jeu), moi qui m’évertuais à faire tenir Unixmail qui proposait un service mail, Jabber, dépôt de fichiers et d'autres choses, on avait des mecs qui faisaient des applis pour tout et rien, on bidouillait à outrance pour contourner le pare-feu de l'école et on faisait virer les profs qui ne nous apprenait rien.

Là, ils s’assoient sur une chaise, lancent un navigateur propriétaire sur un système propriétaire, ouvrent une page Facebook et en avant. Le pire dans tout ça, c'est qu'ils sont intéressés, qu'ils écoutent, qu'ils posent des questions (d'accord, pas tous). J'essaye de faire les cours les plus honnêtes, les moins chiants, avec de l'humour, mêlant points théoriques et pratiques, parfois détournés, n'hésitant pas à faire quelques digressions pour distiller des informations en plus, mais superficielles, pour les inciter à chercher, à comprendre, à gratter plus loin que le support de cours. J'essaye de les mettre mal à l'aise parfois, pour qu'ils sortent de leur coquille et qu'ils se défendent, qu'ils argumentent, qu'ils réfléchissent. Ils ont la chance de travailler dans un domaine d'activité immense où tout est intéressant, tout est sans fin, avec des milliards de choses à apprendre, à tester, à casser, à construire. Bien évidemment, ce n'est pas possible sur tous les modules et certains sont plus monotones que d'autres (aussi bien pour eux que pour moi).

Au final, malgré le niveau peut-être un peu bas, malgré quelques difficultés de leur part à s'affranchir du cadre scolaire (mais c'est bien normal), je suis très content de faire ceci. Je participe à quelque chose de plus grand que moi, je suis utile et j'éprouve de la fierté quand un élève me contacte pour me remercier ou tout simplement me demander quelques informations en plus sur tel sujet. Je suis content s'ils sont contents de la journée qu'ils viennent de passer à mes côtés. Tout simplement parce que je ne vois pas mon activité dans un but de gagner une montagne d'argent, mais uniquement dans un but de sens.

Ce que je fais a du sens, au moins pour moi. Même si je ne touche que 50% de la classe, c'est déjà une somme de gens en plus qui vont réfléchir à des choses, faire des choses. Je ne me vois pas comme le sauveur de l'humanité, mais j'essaye d'agir au mieux à mon échelle, avec honnêteté. Je ne viens pas pour l'argent, je viens pour eux. Pour essayer de leur donner ce que je n'ai pas toujours eu en terme d'intervenant, à savoir du dynamisme, un peu de piquant, un moment sympa où on apprend des choses dans la bonne humeur, où on essaye d'aller plus loin, en collaboration plutôt qu'en confrontation. Inutile de faire la police quand ça se passe comme ça, si tout le monde est content, il n'y a pas de problème.

Le mot est lâché, il faut que j'en parle. "Collaboration". Ça c'est compliqué visiblement, à chaque exercice ou travail pratique, il y a confrontation, mise en compétition. Comme si chaque chose de la vie, de l'apprentissage était forcément un duel, une course, une bataille. J'essaye de développer ce sentiment que réfléchir sur un problème à plusieurs apportera forcément plus de résultats que d'y réfléchir seul dans son coin. Je les force donc à travailler ensemble, à s'aider, à collaborer. Quand tout se met en place et qu'ils arrivent au final à tous travailler ensemble, à se répartir les tâches en sous-groupe et que tout ceci donne un résultat immensément supérieur à ce que j'attendais, je suis content et eux aussi. Ils comprennent vite qu'à plusieurs chacun vient avec ses connaissances qu'il met à profit du groupe pour une élévation globale et individuelle. Le fait que ceci passe est déjà un grand pas. Au-delà de la technique et des connaissances théoriques, il seront amenés à travailler avec des humains, et ça me semble nécessaire de savoir travailler "en bonne intelligence" avec les autres.

### Et le libre dans tout ça ?

Comme dans la vraie vie, tout le monde s'en fiche, donc je n'aborde que très peu ce sujet sauf quand c'est nécessaire. Si on parle d'un logiciel libre (et fort heureusement dans l'administration système il y en a plein), j'explique ce que c'est et les bienfaits en quelques mots. À ma grande surprise il y a souvent des questions et tant mieux. Le but n'est pas de pirater mes cours pour faire de la politique, de la philosophie ou du militantisme pour les logiciels libres quand il n'y en a pas besoin.

Tous mes supports de cours (PDF ou présentation) sont sous licence CC, à chaque slide c'est indiqué en pied de page, rédigé uniquement avec des logiciels libres, tout comme ma comptabilité, mes factures et tout le reste (merci [Dolibarr](https://www.dolibarr.fr/)). Comme je l'ai dit dans un article, je ne milite plus, j'explique uniquement si on me le demande. Il y a des étudiants sous Mac ? Bien, c'est que cela doit leur convenir, je respecte leur choix autant qu'ils respectent le mien de venir faire des cours sous Linux.

Par contre, en ce qui concerne ma liberté, le sous-titre de ce blog _"Un pas de plus vers la liberté !"_ n'a jamais été aussi vrai.

Je ne sais pas si ça va durer, si je vais pouvoir en vivre longtemps, mais en tout cas j'ai vraiment trouvé quelque chose qui me plaît, une activité honnête, pleine de sens, épanouissante, libre et qui me laisse du temps pour apprendre et découvrir encore plein de choses.

Et avouons-le, en travaillant moins, on travaille mieux, c'est d'autant plus important que mon activité est utile aux autres, et pas qu'à moi.
