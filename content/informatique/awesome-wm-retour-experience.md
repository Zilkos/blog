---
authors: ["zilkos"]
title: Awesome WM, retour d'expérience
date: 2014-10-10
lastmod: 2018-11-18
categories: ["Linux"]
tags: ["Desktop", "AwesomeWM"]
toc: true
---

Depuis quelque temps, j’étais vraiment devenu frustré à cause de la plupart des gestionnaires de fenêtres. Trop de souris, les placements des fenêtres par défaut sont mal pensés, les déplacements sont laborieux et nécessitent forcément de lever les mains du clavier. Pour contrer tout ça, j’ai donc essayé sur les conseils d’une communauté, **Awesome WM**.

Voyons donc ce qu’est Awesome WM, qu’est-ce qui le différencie des autres gestionnaires de fenêtres, les avantages, les défauts et quelques clefs pour faciliter son adoption.

## Historique

La version 1 d’Awesome WM est apparue le 19 septembre 2007, de la main de **Julien Danjou**. Oui, un Français au talent énorme, pas forcément très connu de tous mais pourtant, il a participé de près ou de loin à nombreux projets, dont certains utilisés tous les jours ou presque... Arrêtons-nous un instant sur ses projets.

### Julien Danjou

Depuis 2002, il est développeur Debian et maintient un peu plus d’une cinquantaine de paquets. Il travaille aussi sur [OpenStack](http://www.openstack.org) et ses divers composants, il a bien évidemment assuré le packaging d’OpenStack sous Debian. Il est co-mainteneur du projet [Common List IRC client library](http://common-lisp.net/project/cl-irc/), une lib client IRC en Common Lisp. Il est l’auteur d’**Awesome WM**, qui est écrit en Lua. il est aussi commiter en ce qui concerne [GNU/Emacs](http://www.gnu.org/software/emacs/), il a écrit notamment quelques packages comme le [Emacs Rainbow-Mode](https://julien.danjou.info/projects/emacs-packages) (du Lisp encore, un mode mineur d’Emacs). Il a participé à l’évolution de [XCB](http://xcb.freedesktop.org/) qui est un remplacement de Xlib (dans Awesome WM, XCB est utilisé pour communiquer avec le serveur X). En 2010, il est devenu développeur [Gnus](http://www.gnus.org), un lecteur de message et de news tournant sous Emacs, ainsi que développeur [Org-mode](http://orgmode.org/fr) (un mode d’Emacs qui est dédié à l’organisation avec des agendas, todolist, etc…). Il a aussi élaborer l’outil [rebuildd](https://julien.danjou.info/projects/rebuildd) qui permet de reconstruire entièrement l’archive Debian. Et pour finir, en vrac, il a aussi travaillé sur [sysrqd](https://julien.danjou.info/projects/sysrqd), [telak](https://julien.danjou.info/projects/telak), [VARMon](https://julien.danjou.info/projects/varmon), [PyMuninCli](https://julien.danjou.info/projects/pymunincli), [libparportled](https://julien.danjou.info/projects/libparportled), le module Apache [mod_defensible](https://julien.danjou.info/projects/mod_defensible) et moult autres choses plus passionnantes les unes que les autres.

Autant vous dire que c’est une pointure. Il est rare que je m’arrête sur des personnes, mais je pense que là, ça vaut vraiment le coup de se pencher sur le bonhomme tellement ses travaux sont intéressants.

## Awesome WM

Awesome WM est actuellement en version **3.5.5** (_du moins au moment où je rédige cet article_) et ce, depuis le 11 avril 2014. Il est écrit en C et en Lua, la configuration est entièrement en Lua ainsi que tous les widgets existants. Awesome WM est sous licence GNU GPL. Au début son code était initialement basé sur celui de **dwm**, un gestionnaire de fenêtre dans la même veine.

Les principales orientations d’Awesome sont:
    - être très léger
    - être entièrement pilotable au clavier
    - être extrêmement personnalisable
    - proposer plein de dispositions de fenêtres  


Awesome WM dispose de 11 dispositions de fenêtre différentes, allant de la maximisation d’une fenêtre au placement automatique des fenêtres en pavé, ce qu’on nomme le _tiling_. Vous pouvez aussi placer librement les fenêtres, comme dans tout gestionnaire de fenêtres.

### Principe du tiling

Le principe est relativement simple. Je vais ouvrir une console, par défaut, elle s’étendra sur toute la place disponible, donc tout l’écran. J’en ouvre une deuxième, elle prendra la moitié de l’écran (en gardant la première console affichée sur la première moitié). Si j’en ouvre encore une autre, la première console occupera la moitié de l’écran, la deuxième et la troisième se partagerons la deuxième moitié, et ainsi de suite, sous forme de mosaïque.

Ce qui fait qu’avec le tiling, vous n’aurez pas de fenêtre qui se superposent, sauf si bien sûr, il y a pléthore de fenêtres affichées hein, une console de 10×10, c’est pas super pratique.

## Ce dont Awesome WM dispose

Outre un puissant système de tiling, Awesome WM est agréable sur bien des points. Voyons voir tout ça:

### Un gestionnaire discret mais efficace

Par défaut, Awesome est utilisable sans se taper une longue période de configuration. La configuration de base vous permet de faire tout ou presque. Elle inclut un fond d’écran, une barre des tâches qui vous affichera le nom de l’application ainsi que son logo, un pseudo menu avec vos applications (le but n’étant pas forcément de l’utiliser, sauf pour les logiciels relativement rare d’utilisation), des raccourcis clavier pour gérer le placement de vos fenêtre, le lancement d’application ainsi la navigation entre les différents bureaux virtuels.

### Le clavier, point central de la gestion

Il est vrai que la touche Windows (aussi nommée touche _Super_ ou _Mod4_) n’est que peu utilisée, surtout sous GNU/Linux. Sous Awesome WM, cette touche est dorénavant centrale. Bien évidemment, la configuration permet de modifier cette affectation.

Par défaut, la plupart des raccourcis clavier sont positionnés sur la base de la touche Mod4. Un petit scénario pour vous montrer:

En premier lieu, je consulte mes mails sous Mutt, je lance donc un terminal via la commande `Mod4 + Entrée`. Je désire aussi aller troller sur IRC avec des potes, pour ceci je vais lancer un autre terminal via `Mod4 + Entrée` puis lancer Irssi.

J’ai un mail à taper, j’aurai donc besoin de mettre la fenêtre de Mutt de manière horizontale et de l’agrandir un peu. Pas de soucis, je fais défiler les possibilités de dispositions des fenêtres via Mod4 + espace. Une fois la disposition voulue (donc les consoles l’une au dessus de l’autre) je vais redimensionner la console du haut pour qu’elle prenne environ 80% de l’écran (la deuxième console, sera de manière horizontale en bas de mon écran, en prenant 20% de haut). Facile, je mets le focus sur la fenêtre de Mutt avec le raccourci `Mod4 + k` qui permet de faire tourner le focus entre les fenêtres à la mode d’un `Alt + Tab`.

Une fois mon focus sur la fenêtre voulue, j’étends la fenêtre verticalement grâce au raccourcis `Mod4+l` pour augmenter la taille de la fenêtre vers le bas. Si je veux la diminuer, ça sera `Mod4+h`. Dans les deux cas, la deuxième fenêtre adapte sa taille automatiquement, ce qui fait qu’elles ne se chevauchent pas.

Si je veux lancer Nemo, mon gestionnaire de fichier, un coup de `Mod4+r` et hop, j’ai un champs « *Run:* » dans la barre des tâches qui apparait, qui supporte la complétion dans le nom de vos applications, mais aussi l’historique via les touches haut et bas. Juste magique.

Tout ceci, sans bouger les mains du clavier. Magique, j’vous dis !

### Des bureaux virtuels sous forme de tags

Sous la plupart des environnements de bureau, on retrouve le système de bureaux virtuels. Histoire de troller un peu, le futur Windows 10 inclura ce système, sous Linux ça fait déjà un bon nombre d’années qu’on peut en bénéficier.

Pour ceux étant uniquement sous Windows ou ne connaissant pas le système de bureau virtuel, on va s’arrêter quelques instants dessus pour une petite explication. Quand vous démarrer votre ordinateur, votre session se lance et vous êtes sur ce que vous appelez communément « votre bureau ». Il est composé d’une barre des tâches qui contient vos programmes en cours d’exécution, divers raccourcis et un fond d’écran. Cool, jusque-là, rien de bien folichon.

Sous GNU/Linux et plus précisément sous certains environnements de bureau comme _GNOME_ ou _KDE_, nous avons aussi un bureau. Sauf que nous en avons autant que nous le voulons.

En effet, on peut lancer un navigateur dans un bureau, changer de bureau et lancer tout ce qui concerne la messagerie instantanée. Ce qui fait que dans chaque bureau, nous voyons uniquement les applications lancées dans ledit bureau. Pratique pour ~~cacher des choses~~ organiser ses applications.

Et bien sous Awesome, les bureaux sont considérés comme des tags. Dans la barre des tâches en haut à gauche, il y a par défaut 9 bureaux virtuels, on peut changer de bureau via le raccourci `Mod4 + flèche droite` ou flèche gauche OU `Mod4 + &` pour le bureau 1, `Mod4 + é` pour le bureau 2, etc.

On peut « envoyer » une application dans un bureau virtuel, par exemple, j’ai le focus sur Chromium, si je fais `Mod4 + 3`, j’envoie Chromium sur le bureau 3.

#### Bureau == tag

Chaque bureau peut porter un nom, disposer d'un tag. En ce qui me concerne, j’ai plusieurs noms. En totale exclusivité, voici les noms de mes bureaux avec leurs usages (oui, c’est très commun et complètement inintéressant j’en conviens):

  - *main* = Pour tout et rien
  - *im* = Pour tout ce qui est messagerie instantanée (IRC / Jabber principalement)
  - *tw* = Pour twitter (une appli, pas le site)
  - *www* = Pour le web
  - *dev* = Pour le développement (IDE et/ou console)
  - *misc* = Pour le reste mais très souvent Cmus

Awesome couple la puissance de son tiling avec la souplesse de ces bureaux virtuels. Vous pouvez définir pour chaque bureau virtuel une disposition pour les fenêtres. Et ça, c’est vraiment très pratique, très très pratique !

Pour chaque tag, je peux décider de quel affichage je souhaite. Par exemple pour le bureau `www`, qui généralement contient uniquement un navigateur, je peux garder l’application de ce bureau en plein écran en permanence. Ainsi, en faisant `Mod4+’` je me retrouve sur un Chromium en plein écran. Et ainsi de suite pour chaque bureau actifs. Cette technique permet d’anticiper le placement et la gestion des fenêtres par bureau. Confortable et économe en temps !

Sachez qu’Awesome est une vraie merveille en ce qui concerne le multi-écran. Là ou la plupart font juste une réplication de l’affichage ou un affichage étendu, Awesome gère ça de manière très intéressante.

Par exemple, je branche un écran sur ma sortie VGA. J’ai donc deux bureaux d’affichés, sur le même tag. Et bien sur les deux écrans, les tags sont différents. C’est comme si j’avais 9 bureaux par écran, soit 18 au total. Les écrans possèdent des tags indépendants, et ça c’est vraiment super chouette quand vous présentez des choses.

Si j’envoie une application sur le 3ème tag de mon premier écran, l’application sera visible uniquement sur cet écran, mais pas sur l’autre.

Bien sûr, c’est le comportement par défaut, libre à vous de modifier, ce qui nous ammène à la partie suivante !

### Des extensions

Awesome dispose de plusieurs systèmes d’extensions, des widgets. Ils permettent d’enrichir votre gestionnaire de fenêtres de manière significative. On peut trouver des widgets pour le réseau, des calendriers, des informations de température des composants, des informations sur quel morceau Cmus est en train de jouer, si vous avez des mails, etc.

Je ne suis pas forcément accro à ce genre de chose, j’en dispose uniquement de quelques uns: L’heure en français ainsi que la date, un widget de batterie qui m’indique la charge en cours/restantes, un widget de consommation RAM et CPU (_en % pour le CPU et en % et MB pour la RAM_) et un widget m’indiquant le volume sonore de la sortie audio. J’ai aussi quelques autres widgets qui m’indiquent diverses notifications, ainsi qu’un widget pour la gestion du tethering, mais vu que j’en suis l’auteur, je ne les compte pas.

Non, ce n'est pas compliqué d'en écrire, personnellement, je n'avais jamais pratiqué le LUA. J'ai principalement appris en lisant le code des autres widgets.

Ces extensions sont sous forme de fichiers Lua. On peut en écrire ou on peut récupérer ceux écrits par la communauté et les intégrer à notre configuration (attention aux versions !).

Passons maintenant aux points moins rigolos…

### La configuration

Comme vous le savez, sous GNU/Linux, tout ce qui est hautement personnalisable est généralement long à configurer. Ici, on n’y échappe pas.

Sauf que de base, **ça fonctionne**. Et ça, c’est agréable pour tout le monde. Ce qui fait que vous pouvez avoir un environnement vraiment bien, avec seulement peu de configuration. Personnellement j’ai rajouté une trentaine de lignes à la configuration par défaut, en incluant l’activation des widgets cités plus haut, c'est donc **vraiment peu** !

Ce qui fait que tout le monde y trouve son compte. La personne n’ayant pas forcément trop envie de bidouiller aura un système simple, fonctionnel, et la personne connaissant le Lua et aimant tout casser/réparer pourra aller extrêmement loin dans la configuration, voir dans la création de widgets. C’est vraiment ça qui est appréciable avec Awesome, c’est cette largeur permise à tout le monde. Mais avouons-le, si vous n'aimez pas bidouiller, Awesome WM n'est pas forcément pour vous.

### Attention aux erreurs !

Pour avoir un gestionnaire de fenêtre correct, c’est donc relativement simple, par contre, si vous faites une erreur, c’est très punitif. Punitif dans le sens où votre session utilisateur démarrera, mais sans gestionnaire de fenêtre ! Elle vous dira quelque chose du genre : "*Votre session a duré moins de 10 secondes, patati patata…*" . Si vous cliquez sur *OK* , vous serez ramené à l’ouverture de session et si vous ne modifiez rien, vous aurez cette même erreur en continue, jusqu’à ce que votre problème de configuration soit réglé.

Heureusement, on a quand même un bon système de journalisation des erreurs qui permet de voir d’où l’erreur vient à la ligne de configuration près. Nous verrons plus loin comment faire face à ce problème, encore faut-il savoir se débrouiller pour accéder à un *tty* et maitriser des outils uniquement en ligne de commande.

### La documentation

La documentation d’Awesome est disponible sur le site [awesome.naquadah.org](http://awesome.naquadah.org/). Elle est maintenue dans les grandes lignes, mais pas forcément à jour sur tous les points.

Vous avez aussi de nombreux tutoriels, sites, forums et blogs qui abordent le sujet en présentant leurs configurations, leurs astuces et bien d'autres ressources. Sauf qu’Awesome en version 2.x et en version 3.x c’est vraiment pas la même chose, donc faites *très* attention aux ressources que vous récupérez !

En effet, lors du passage à la version 3, votre Awesome se casse, littéralement. Le souci étant que la plupart des personnes écrivant sur ce sujet écrivent "sur l’instant" (comme cet article) et donc ne donne pas leur version d’Awesome. Ce qui fait qu’on peut se retrouver avec une configuration qui au final, n’est pas faite pour votre version d’Awesome. C’est risqué, et bien évidement, c’est pas aisé que de « traduire » la conf de la version 2 à la version 3. Notez qu’il existe quelques subtilités de configurations et d’écriture entre les version 3.4 et 3.5 notamment et surtout au niveau des widgets.

Le temps passant, on trouve de plus en plus de ressources pour Awesome 3, et tant mieux !

##  Prérequis pour l’adopter

Donc, qu’on se le dise, Awesome WM, c’est franchement très sympathique, même si un peu rebutant au premier abord pour les non bidouilleurs. Voyons donc ce qu’il nous faut, à nous, faibles humains pour dompter la bête.

### De l’anglais

Inutile de troller là dessus, en informatique, la plupart et les meilleures documentations sont en anglais. On n’y reviendra pas, c’est nécessaire de maitriser un minimum l’anglais. Pas forcément de le parler, mais comprendre une documentation, même technique.

### Des connaissances minimales en cas d’erreur

Si ça vous pète au groin, vous serez amené à mettre les mains dedans « à l’ancienne ». Vous devez savoir comment vous retrouver sur un tty. Vous devez savoir comment lire un fichier journal contenant des erreurs, et bien sûr maitriser un outil qui permettra d’éditer votre configuration hors d’un environnement graphique (donc typiquement un éditeur CLI du type vi, vim, emacs, nano, ed, etc).

Personnellement, je suis maintenant sur un Linux Mint avec Awesome WM. Si erreur il y a, elle sera dans le fichier `.xsessions-errors`. Il est nécessaire de savoir faire un:

```bash
cat .xsession-errors | more | grep "awesome"
```

pour savoir d’où l’erreur vient, et d’éditer votre fichier de configuration sous Vim, Emacs, Nano ou autre.

### De la volonté et l’envie d’apprendre

On l’a dit, les logiciels hautement configurables nécessitent que l’on y passe du temps. La meilleure configuration sera la vôtre, et pas celle du voisin qui sera uniquement un copié/collé nul. Je vous rappelle que si vous prenez une configuration existante sans savoir comment elle fonctionne, c’est carrément plus ardu de régler les problèmes en cas de soucis. Si vous faites votre configuration, vous savez quels composants sont affectés, vous savez comment c’est construit, donc vous êtes plus à même de réparer vous-même si ça pète lors d’une mise à jour.

Les pré-requis nécessaires pour "aller plus loin", dans le sens "vraiment construire quelque chose qui comble vos besoins", c’est d’abord s’informer un minimum sur le langage Lua. En effet, les widgets et le fichier de configuration sont en Lua, vous n’y échapperez pas (mais rassurez-vous, c'est relativement simple).

Un autre prérequis serait de connaitre le système. Pour faire un widget, vous devrez accéder à des éléments du système GNU/Linux qu’on n’utilise pas tous les jours. Si un `/proc/net/dev` ne vous dit rien, apprenez, c'est pas compliqué, ça augmente votre culture informatique et c'est fun !

## Conclusion

Et oui, déjà la fin. Contrairement à d’habitude, je ne vous explique pas comment ça marche, ou comment le configurer. Par volonté et par logique. Déjà parce que c’est un retour d’expérience, et pas un tutoriel. Mais aussi parce que je suis entrain de dompter la bête et le Lua depuis quelque temps. J’ai écrit des widgets qui fonctionnent certes, mais de là à expliquer simplement comment ça fonctionne, ça risque de faire un article à rallonge et peu didactique. Si toutefois vous voulez un coup de main pour vous y mettre, n’hésitez pas à me contacter.

Au final, Awesome WM est franc avec vous. Sur le _about_ on trouve cette phrase:

> It is primarly targeted at power users, developers and any people dealing with every day computing tasks and who want to have fine-grained control on theirs graphical environment.

Il ne laisse pas vraiment sa chance au grand débutant et c’est normal, car il n’est pas fait pour. Le débutant débrouillard, sans être ingénieur, peut s’en sortir honnêtement avec un gestionnaire simple, mais élégant. Il va vite se retrouver frustré de ne pas exploiter complètement les possibilités et va donc se pencher sur le Lua et se pencher sérieusement sur l’architecture d’Awesome WM. Les personnes ne faisant pas ce mini parcours ne sont tout simplement pas la cible d’Awesome WM. Pas d’élitisme, le fait est clairement établi dès le départ: c’est pas simple, mais vous pouvez y arriver et apprendre plein de choses.

Par contre, une fois cet effort fait, vous êtes face à un océan de possibilités toutes plus séduisantes les unes que les autres !

Vous êtes prévenus ! ;-)
