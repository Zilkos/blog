---
authors: ["zilkos"]
title: Claws-mail, la sérieuse alternative à Thunderbird
date: 2013-07-16
lastmod: 2018-11-18
categories: ["Linux", "Logiciel"]
tags: ["Mail"]
---

Aujourd’hui, on va parler de client mail et plus particulièrement de Claws-Mail qui au final, s’avère être une très bonne alternative à Thunderbird, bien que moins connu.

Fervent adepte de Mutt qui comble entièrement mes besoins sur tous les plans, j’étais à la recherche d’un client mail graphique de qualité sur un autre poste. Bien évidemment, j’avais Thunderbird d’installé, aussi bien sur Windows que sur GNU/Linux ou FreeBSD, avec Enigmail qui me permet de chiffrer mes mails.

Cependant, avec 5 boites aux lettres autant alimentées qu’un ours avant sa période d’hibernation, il devient lent, assez lourd et surtout il consomme quand même pas mal de RAM (actuellement 190 Mo). Or, le portable qui l’accueille ne dispose que d’un seul Go de RAM. Couplé avec tout le reste (services et applications), ça swap de temps en temps…

Je suis donc parti en quête d’un client mail graphique, léger et avec des fonctionnalités similaires à Thunderbird, vitales pour moi, à savoir au minimum:

  - POP3 / IMAP (oui d’accord, c’est un peu la base quand même…)
  - SMTP (ça aussi)
  - Gestion des alias (nommés parfois « identités »)
  - Chiffrement de mail via GPG (MIME)
  - Filtres et tris automatiques
  - Gestion de plusieurs carnets d’adresses
  - Messages pré-enregistrés et templates divers
  - Envoi différé (Ctrl + Maj + Entrée sous Thunderbird)

C’est en cherchant sur [Freshports](http://www.freshports.org/) avec le mot clef _mail_ que j’ai trouvé Claws Mail. Même Freshports le dit:

> Lightweight and very featureful GTK+ based e-mail and news client

Parfais, me dis-je (même si c’est en GTK+).

### Installation et première utilisation

Selon votre OS, dans les dépôts ou les ports, adaptez la commande en conséquence. Installation enfantine, vous trouverez facilement certains plugins de quelques Ko chacun ([voir ici](http://www.freshports.org/search.php?stype=name&method=match&query=claws-mail&num=10&orderby=category&orderbyupdown=asc&search=Search) pour les BSDistes, ou [ici](http://www.claws-mail.org/plugins.php) pour les autres en sachant que les plugins sont également dans les dépôts / ports).

Au premier lancement, le client demande à configurer un premier compte courriel. Une fois fait, le logiciel est en fonctionnement standard.

Des boutons tout ce qu’il y a de plus simple, comme sous Thunderbird, sauf que de base... il ne prend qu'une dizaine de Mo de RAM.

Au fil du temps on découvre des fonctionnalités très intéressantes, comme les templates lors de réponses ou de transferts de mail (un genre de squelette que l’on applique, pour les non-initiés).

Ses points forts:

  - Un puissant système de tags et d’actions qui en découlent (via le logiciel ou des commandes issues du Shell (si, si, /dev/null est de la partie pour laposte.net !))
  - Traitement manuel ou automatique à la réception (pre-processing) ou après la réception (post-processing)
  - Gestion de plusieurs identités / signatures / carnets d’adresses
  - Support de vCard, JPilot et LDAP
  - Respecte par nature les standards en vigueur (on y reviendra)
  - Pas de rédaction HTML (enfin presque)
  - Beaucoup de plugins pour bien des choses
  - Et plus encore

### Comment ça "_Pas de rédaction HTML !?_"

Ah, je vous imagine déjà bondir sur votre chaise ~~longue~~ de bureau. Ce logiciel a pour moi un défaut et demi, qui n’en n’est pas vraiment un (et demi).

Le défaut (qui n’en est pas un), c’est l’impossibilité d’écrire un mail en HTML. C’est pour moi un avantage indéniable qui force les utilisateurs aux bonnes pratiques de la rédaction de courriel. L’utilisation du texte brut n’a que des avantages.

Le demi-défaut est l’impossibilité de lire un mail au format HTML. En effet, les mails HTML seront forcés en texte brut à la lecture. Rassurez-vous, un plugin permet la lecture de mail en HTML, un petit bouton qui permet de switcher d’affichage dans la vue courante pour profiter pleinement du Comic Sans MS en taille 25 couleur rose de vos correspondants.

Une petite fonction que j’aime beaucoup, c’est la collecte d’adresse email. Indiquez un dossier contenant des mails et il remplit le carnet d’adresses de votre choix avec les adresses présentes selon les champs que vous voulez (`From, Reply To, Sender, To, CC, Errors-to`) avec les sous dossiers ou pas, tout en soignant la chose (il évite les doublons, etc…). Il dispose aussi d’une commande permettant d’effacer les messages dupliqués présents dans un ou plusieurs dossiers.

En bref, l’essayer c’est l’adopter. C’est l’adopter lui, mais aussi adopter des bonnes pratiques pour vos courriels. Tout le monde s’en porte mieux, vous, vos contacts, vos mails, le réseau et votre RAM !

Et vous voulez savoir comment chiffrer vos mails, allez donc lire l’excellent tuto de Maxime AUvy intitulé [[Tuto] Signer/chiffrer ses mails avec GPG](http://open-freax.fr/tuto-mails-gpg/).
