---
authors: ["zilkos"]
title: 'C''est la dernière fois que j''utilise LaTeX... ou pas.'
date: '2015-09-29'
toc: true
lastmod: 2018-11-18
categories: ["Linux"]
tags: ["LaTeX", "Pandoc", "txt2tags"]
---

Cher visiteur, tu n'es pas sans savoir que je viens de terminer mes études. "_Enfin_" dirais-je ! La bonne nouvelle, c'est que les articles devront réapparaître un peu plus fréquemment par la suite. J'ai terminé mes études, et comme la plupart de mes camarades, je fut obligé de rendre un immense dossier concernant un projet, ainsi que faire un support de présentation pour une soutenance orale. Pour tout ceci, j'ai utilisé LaTeX, et je vais t'expliquer pourquoi je ne le réutiliserai pas.

> C'est indiqué sur le site, mais je le répète spécialement pour cet article, il s'agit d'un avis sur LaTeX qui est valable **uniquement** dans mon cas et ma situation bien précise. Il émane **uniquement** de moi et de personne d'autre. Merci de ne pas faire de mes dires une généralité.

LaTeX est un système de composition que j'utilise depuis environ 5 ans. Je l'ai découvert par un ami (la bise à Maxime en passant), à la suite d'un ras-le-bol général de logiciels "usine à gaz" que sont Libre Office ou la suite Microsoft Office, bref au début de "ma vie en texte brut". Donc, sous Linux, sans utiliser LibreOffice, il ne reste que peu de choix. C'est donc parti pour un article un peu blasant sur LaTeX...

### Utilisation lambda

J'avais à cette époque, il y a 5 ans, une utilisation basique de LaTeX. Tellement basique que mon préambule ne dépassait pas les 10 lignes. C'était plutôt chouette et pratique, c'était la découverte et les documents étaient vraiment de très bonne qualité. Je ne rencontrais pas de souci de mise en page particulier car tout est bien pensé pour un usage courant, si on ne sort pas des sentiers battus. Je pouvais éditer mes documents avec l'éditeur que je voulais et tous embarquaient des configurations spécifiques pour LaTeX, un bonheur, vous connaissez mon attachement au texte brut.

Dans le coup j'ai pris le risque de faire mon premier mémoire d'étude avec (BTS, à l'époque), ce fut rude, et la présence de mon ami était hautement nécessaire régulièrement. Car oui, en LaTeX, il ne faut pas être seul ou alors il ne faut pas être pressé. A chaque fois que je voulais faire quelque chose d'un peu exotique, une longue phase de recherche s'imposait. C'était bien, c'est formateur comme pratique et au fil du temps on capitalise sur des connaissances pour gagner encore plus de temps par la suite (même si, avouons-le, la plupart du temps c'était de la recopie sans forcément trop comprendre ce qu'il se passait).

### Utilisation trop lambda

Comme certains le savent, je suis peu amené à utiliser LaTeX dans le cadre professionnel. Ce cadre est entièrement sous Microsoft, et même les interactions vers l'extérieur (prestataires, fournisseurs, etc) sont fortement liés au monde Microsoft (et donc aux formats fermés ou peu interopérables, à la suite Office que je déteste et tout ça). La seule fois où je peux utiliser du LaTeX, c'est sur un document qui réunit les conditions suivantes:

  * Il ne peut être rédigé que par moi-même (méconnaissance complète de l'existence de LaTeX dans la boite)
  * Évidemment, il ne doit pas être collaboratif (à l'inverse de presque tous les documents)
  * Il ne doit pas être en accord avec la charte graphique (impossible de la reproduire en LaTeX) donc la portée n'est pas "officielle"
  * Il doit être suffisamment long pour nécessité l'utilisation de LaTeX (c'est _overkill_ pour de simples notes)

En clair, ça fait peu de document, tellement peu que j'utilise vraiment très rarement LateX, à l'exception de quelques lettres et certaines procédures. Sauf que LaTeX n'est pas très compatible avec l'expression "_je l'utilise une fois par an_"...

### Manque d'habitude

Je l'utilise tellement rarement que rédiger mon dernier mémoire d'étude il y a quelques mois fut une véritable souffrance. J'étais sans cesse obligé de regarder les sources de mon précédent mémoire pour des mises en page spécifiques, des tableaux un peu corsés, certains symboles rarement utilisés, et même parfois des choses simples comme un `flushright` sur un pavé de texte ou du `multicol` sur une page. Une véritable amnésie... par manque d'habitude, par manque d'utilisation.

### Par manque de clarté...

Si je suis amnésique à ce niveau là, c'est tout simplement que LaTeX est lourd, à mon sens beaucoup trop brouillon et casse-gueule. On sent bien à l'utilisation qu'on traîne des années d'histoire derrière nous...

#### ... ou par complexité ?

Le Libre, c'est le choix. Là, c'est juste le bordel. Énormément de packages font la même chose, mais si tu utilises X, ça casse Y qui possède la fonctionnalité que tu veux, mais tu dois redéfinir des trucs de Z, parce qu'autrement ça casse tout. Clairement, c'est chiant. Il faudrait un immense ménage là dedans, garder une base saine de packages les plus utilisés ou les plus courants dans un dépôt central et officiel qui ne rentrent pas en conflits avec eux-même et éventuellement en proposer via des dépôts tiers, à utiliser si on sait ce que l'on fait. De manière similaire à nos dépôts logiciels sur des distribution dites "stables".

Au final, pour mon mémoire, j'ai un préambule de 52 lignes. mon `\begin{document}` ne commençant qu'à la ligne 58 de mon fichier. Honnêtement, après quelques temps sans l'utiliser, je ne sais même plus l'utilité de tel ou tel package. Est-ce pour une fonctionnalité utile ? Est-ce un package pour empêcher tel ou tel bug ou pour contourner tel comportement ? A force, je ne sais plus. D'autant que c'est commenté comme un cake, du style `% nécessaire`. _Mea culpa_.

Une compilation qui foire, et c'est la fin si l'erreur n'est pas syntaxique (une accolade qui manque, un environnement mal déclaré, etc). Les messages d'erreur de compilation sont juste incompréhensibles, et quand ils indiquent une ligne, c'est rarement la bonne. Obligé de reprendre toute une portion de code foireuse pour essayer de déterminer au final d'où peut venir le souci. Frustrant. Frustrant certes, mais ce n'est pas la faute de l'outil. C'est comme un langage de programmation (d'ailleurs c'en est un). En programmant tous les jours, nous sommes habitués, nous avons des réflexes, des automatismes, nous passons à côté des pièges les plus courants. Là, je ne l'utilise plus assez, donc je rentre tête la première dans chaque pancarte que je peux rencontrer, comme un débutant à chaque fois.

### LaTeX c'est bien

Je ne dit pas que ce système est mauvais. Il est bien, autrement je ne l'aurais pas utilisé tant de temps. Je dis juste qu'il est incompatible avec une utilisation lambda et qu'il est fortement perfectible. Il traîne depuis trop de temps des choses mauvaises et peu claires qui à mon sens limitent fortement son adoption, même auprès de personnes travaillant dans l'informatique en général, et en particulier dans la programmation. J'ai dis que ça limite, je n'ai pas dit que LaTeX était incompréhensible, il faut en gros un après midi pour l'appréhender, temps d'apprentissage vraiment court quand on voit la qualité des documents que l'on peut sortir avec.

Il est bien pour les personnes utilisant ce système tous les jours. Si c'est pour l'utiliser une fois de temps en temps et chercher à faire des choses relativement complexes, c'est beaucoup trop chronophage et contre productif, même si le résultat est terriblement _sexy_.

Je n'incrimine pas LaTeX. C'est entièrement de ma faute, principalement à cause de ma fréquence d'utilisation très réduite, c'est logiquement plus adapté à mes contraintes et à mon contexte d'utilisation. Je ne crache pas volontairement sur l'outil, j'essaye de revenir sur ces années d'utilisation en ayant un œil aussi objectif que possible. Par contre ce qui n'est pas de mon cru, c'est cette gestion chaotique des packages.

### Je le conserve ?

Pour tout vous dire, les documents sont tellement bien présentés et typographiquement justes que j'ai envie de le garder, mais pas de l'utiliser. Je ne veux pas non plus utiliser les usines à gaz citées précédemment, j'aime ma vie en texte brut.

Je vais donc tester prochainement `txt2tags`, qui me fait de l'œil depuis longtemps. Le principe est d'écrire des documents de manière __simple__ (avec une syntaxe similaire au markdown, de ce que j'ai vu) puis d'exporter en LaTeX, quitte à _pimper_ un peu la sortie pour que ce soit vraiment chouette (typiquement utiliser ma police préférée sous LaTeX et d'autres choses). Il permet un export vers 18 formats de sortie, oui, **18**. À voir ce que ça donne...

Je ne sais pas trop dans quoi je me lance, on verra bien, mais les sources txt2tags sont aussi simples que du Markdown, je ne sais juste pas comment c'est géré dans l'arrière-boutique au niveau LaTeX et si il y a grand besoin de tailler dans le `.tex` de sortie. En bref, dans le brouillard, je vais essayer d'éclaircir tout ça, puis essayer de trouver un workflow pour mes documents qui devra respecter les points suivants:

  * Édition en texte brut
  * Simplicité de rédaction
  * Qualité d'un document en LaTeX


Challenge accepted.
