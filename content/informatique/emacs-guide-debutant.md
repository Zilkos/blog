---
authors: ["zilkos"]
title: 'Emacs: Le guide du parfait débutant'
date: 2015-03-01
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Emacs", "CLI"]
toc: true
---

Emacs. Un mythe, mais aussi un géant des écosystèmes GNU/Linux et *BSD. De nombreux débutants veulent mettre les mains dedans, mais il est relativement complexe à prendre en main. Ce guide est justement là pour vous aider à entrer dans le monde d'Emacs en apprenant comment il fonctionne. Orienté débutant, il comporte les principales commandes de base.

### Le très rapide point historique

Emacs signifie _Editing MACroS_. Il a été développé par **Richard Stallman** en 1976, c’est lui qui le maintient encore aujourd’hui (avec l'aide d'autres développeurs bien entendu). Très utilisé par beaucoup programmeurs, certains le préfère à un IDE tellement il est puissant, surtout en le couplant à de nombreux plugins. Non, on ne reviendra pas sur le troll historique ayant fait coulé tant d’encre virtuelle concernant Emacs VS Vi(m). Direction l'installation !

### Installation et premiers pas

Un simple `sudo apt-get install emacs` suffit pour l'installer pour tout ce qui est basé Debian. Pour le lancer, vous avez deux solution:

1. En graphique, tapez `emacs` dans un terminal.
2. E console, tapez `emacs -nw` dans un terminal.

Pour voir les options disponibles, tapez `emacs --help | more` dans un terminal. On y trouve l'explication concernant l'argument `-nw` permettant de lancer emacs dans un terminal:

> --no-window-system, -nw     do not communicate with X, ignoring $DISPLAY

J’ai volontairement couplé (_pipé_ pour être précis) la commande avec la commande `more` car l’aide est un poil trop grande pour l’écran, en faisant ainsi, on dispose d’un défilement (touche `espace` pour avancer et `q` pour quitter).

### Explication des touches de raccourcis pour les commandes

Emacs fonctionne principalement via des raccourcis, il y en a énormément et ils sont basés à partir d’un simple schéma. Un exemple est toujours plus parlant, on peut retrouver des commandes du type `C-f` ou bien `M-f` ou encore
`C+M-f` et même `C-x u`.

Ça a l’air compliqué comme ça, mais rassurez vous, c’est enfantin à comprendre, voyons voir ces `C` et `M` qui fusent de partout:

* `C` indique la touche `Control`
* `M` indique la touche `Meta`, qui est généralement la touche `Alt`
* `-` ou `+`indique qu’il faut maintenir `C` ou `M` (ça dépend de la commande)
* `M-x commande` indique le nom de la commande à taper après `M-x`

Ainsi, `C-f` équivaut à un courant `Control + F` (je laisse enfoncé `Control` et j’appuie sur `F`. `M-f` équivaut à un `Alt + F`, je laisse enfoncé `Alt` et j’appuie sur la touche `F`.

`C+M-f` est donc l’équivalent de `Control + Alt + F`, je laisse enfoncé `Control` puis j’enfonce et laisse enfoncé `Alt` puis j’appuie sur `F`. Les personnes sous Windows font couramment un `Ctrl + Alt + Suppr` (pour ouvrir le gestionnaire des tâches), et bien le principe est le même.

Enfin, la dernière variante: `C-x u`. Notez que entre le `x` et le `u` il n’y a pas de tiret ou de symbole plus, le tiret indiquant normalement qu’il faut laisser la touche enfoncée. Pour celui là nous faisons donc un `Control + x`, je lâche tout et j’appuie ensuite sur `u`.

Première commande à savoir:

* `C-x C-f` : Ouvre un fichier

Pour lancer cette commande, on maintient la touche `Control` puis on appuie sur `x`, on relâche tout et on maintient `Control` puis on appuie sur `f`. Une fois cette commande lancée, on a accès dans la barre du bas à un prompt où l’on tape le chemin du fichier. Bien sûr, Emacs gère la complétion parmi vos dossier et fichiers, appuyez deux fois sur `Tabulation` et vous verrez une liste cohérente.

### Pratiquons !

On va travailler dans le dossier utilisateur (typiquement `/home/vous/`) pour pas s’embêter avec les chemins. On va créer un fichier avec Emacs pour travailler dedans. Allons-y, nommons notre fichier `test`. Pour créer un fichier, il suffit de faire suivre le nom du fichier (et son chemin si besoin) à la commande de lancement d’Emacs. Comme ceci:

```bash
emacs -nw test
```

Si toutefois votre écran est coupé en deux à l'ouverture d'Emacs (ce qui est fort probable), faites la commande `C-x 1` (donc un `Ctrl + x` basique, on relâche tout et on appuie sur `1` ou Shift + & pour ceux qui n’ont pas de pavé numérique). Maintenant, on va y mettre du contenu dans ce fichier de test. Copiez ce texte:

> Praesent laoreet suscipit faucibus. Cras suscipit sagittis purus vel rutrum. Integer tincidunt felis lacus. Mauris nunc nibh, (mollis quis ultricies et, rhoncus id quam). Nulla semper viverra fermentum. Suspendisse rhoncus varius enim ac bibendum. Suspendisse iaculis, felis in varius ullamcorper, leo magna mattis elit, vitae fringilla odio elit dapibus nibh.
Praesent laoreet suscipit faucibus. Cras suscipit sagittis purus vel rutrum. Integer tincidunt felis lacus. Mauris nunc nibh, (mollis quis ultricies et, rhoncus id quam). Nulla semper viverra fermentum. Suspendisse rhoncus varius enim ac bibendum. Suspendisse iaculis, felis in varius ullamcorper, leo magna mattis elit, vitae fringilla odio elit dapibus nibh.

Oui, du lorem ipsum basique.

Collez donc le texte dans Emacs via le raccourcis `Shift + Insert`. Pour coller sous Emacs, on utilise normalement `C-y` mais il y a un soucis entre Emacs et les autres logiciels si vous êtes sous Ubuntu ou Mint, donc on utilise un raccourcis clavier bien connu qui fonctionne partout: `Shift + Insert`. Je ne reviendrai pas sur le soucis du kill-ring, pour l'instant, ça n'importe peu.

> Pourquoi on utilise pas Control V pour coller, comme partout ?

Bonne question, mais si vous avez suivi, vous devriez avoir la réponse. J’ai mentionné plus haut que normalement sous Emacs, la commande coller c’est `C-y`. On utilise `Shift + Insert` dans ce cas car la commande `C-v` sous Emacs n’est pas coller, mais nous fait _avancer d’un écran_ (on y reviendra). Non, `Ctrl + v` n'est pas une convention obligatoire pour tous les éditeurs !

Bien, notre fichier contient du texte, sauvegardons le avec la commande `C-x C-s`. Donc, on laisse appuyé `Control` et on appuie sur `X`, on relâche tout et on laisse appuyé `Control` puis on appuie sur `s`. C’est l’équivalent de deux commandes à la suite (typiquement couper puis sauvegarder selon les raccourcis globaux des principaux OS). Emacs nous indique clairement que le fichier a été sauvegardé en mettant dans la barre inférieure `Wrote /home/vous/test`.

Parfait ! Vous savez maintenant ouvrir ou créer un fichier, écrire dedans, coller depuis l’extérieur, et sauvegarder !

#### Du mouvement !

On va maintenant apprendre à se déplacer dans le fichier. Pour commencer, on va aller directement se positionner à la fin du fichier.

Pour faire cette action, vous avez deux solutions:

1. Vous êtes faibles, vous utilisez les flèches et mettez 15 ans pour atteindre la fin du fichier.
2. Vous avez la classe et en une commande vous êtes à la fin.

Bien sûr, on choisira la solution deux pour raison de rapidité. Voyez donc les deux commandes suivantes:

* `M->` pour aller à la fin du fichier
* `M-<` pour aller au début du fichier

Donc oui, la première commande se fait avec `Shift` si le verrouillage des majuscule n’est pas activé. En une seule commande, peu importe où l’on se trouve dans le fichier, elle vous emmène soit au début, soit à la fin. Parce que vous êtes mignon et qu’on est là pour apprendre, voici d’autres commandes de déplacement:


| Commande | Description                                    |
| -------- | -----------                                    |
| `C-f`    | avance d’un caractère                          |
| `C-b`    | recule d’un caractère                          |
| `M-f`    | avance d’un mot                                |
| `M-b`    | recule d’un mot                                |
| `C-n`    | avance d’une ligne                             |
| `C-p`    | recule d’une ligne                             |
| `C-a`    | va au début de la ligne                        |
| `C-e`    | va à la finC+M-f de la ligne                   |
| `M-a`    | va au début de la phrase                       |
| `M-e`    | va à la fin de la phrase                       |
| `M-{`    | va au début du paragraphe                      |
| `M-}`    | va à la fin du paragraphe                      |
| `C+M-b`  | va à l’ouverture de parenthèse correspondante  |
| `C+M-f`  | va à la fermeture de parenthèse correspondante |

Pour mémoire, une phrase se termine par un point. La ligne et la phrase ne sont pas la même chose, faites attention. Pour Emacs, une ligne correspond à la largeur de l’écran. Pour vous souvenir de toutes ces commandes, soyez logique: B = BACK qui signifie "_arrière, retour, derrière_" en anglais, alors que F = FORWARD qui signifie "_vers l’avant, en avant_".

Aussi, **pratiquez !** Juste les lire ne suffit pas à les avoir dans les doigts. Nous avons mis en place un fichier d'exemple, servez-vous en et testez toutes ces commandes. Pratiquez le plus possible, c'est comme ça que ça va rentrer.

#### Éditons !

Maintenant qu’on sait comment se déplacer d'un pas chaloupé mais non dénué de nonchalance dans nos fichiers, voyons voir comment les éditer.

Placez vous à la fin du fichier. Si vous appuyez sur la touche `Backspace` (_grosse flèche vers la gauche généralement au dessus de la touche Entrée sur les clavier Azerty_), le caractère précédent est effacé, comme partout. Maintenant, maintenez la touche `Alt` tout en appuyant sur `Backspace` (donc `M-Backspace`), on voit que ça efface le mot entier précédent, ou le début du mot jusqu’au curseur, si le curseur est au milieu du mot.

Dans l’autre sens, `C-d` efface le caractère suivant et `M-d` le mot suivant (donc après le curseur).

Super, maintenant on va sélectionner du texte. Là aussi, vous avez deux solutions: à la souris ou au clavier. Bien sûr on le faire au clavier ! Le système de sélection fonctionne via des marques. Vous avez voir, c’est très simple.

Placez vous n’importe où dans le texte, faites la commande `C-espace` qui permet de **poser une marque**. Maintenant, vos déplacements seront considéré comme de la sélection ! Sélectionnez ce que vous voulez et faisons un copier coller ou autres choses, comme:

| Commande | Description                                                  |
|----------|--------------------|
| `M-w`      | Copier                                                       |
| `C-y`      | ou Shift-Insert Coller (pour les raisons évoquées plus haut) |
| `C-w`      | Couper                                                       |
| `M-h`      | Sélectionne le paragraphe                                    |
| `C-x h`    | Sélectionne tout le contenu du fichier                       |
| `C-k`      | Supprime à partir du curseur jusqu’à la fin de la ligne      |
| `M-k`      | Supprime à partir du curseur jusqu’à la fin de la phrase     |


#### Corrigeons !

Oui, Emacs embarque un correcteur orthographique, nommé **ispell**. Testons-le !

Ré-ouvrez votre fichier test (`emacs -nw test` depuis `/home/vous`) et sélectionnez tout le contenu avec la commande `C-x h` puis supprimez le tout avec la commande Couper, `C-w`. Rapide non ?

Non ? Simple question d'habitude.

##### Une histoire de dictionnaire

Écrivez quelques mots puis faites la commande M-x qui permet de taper le nom d'une commande. On voit que la barre du bas affiche un M-x puis un curseur qui clignote. Le principe est simple, il suffit de taper le nom d’une commande. Nous allons voir les commandes disponible avec _ispell_. Tapez `ispell` puis appuyez deux fois sur `Tabulation` pour voir les commandes disponibles commençant par ispell. Dans cette liste on voit un `ispell-change-dictionary`, qui permet de changer le dictionnaire pour la correction. Complétez votre commande et appuyez sur `Entrée` pour choisir le dictionnaire voulu...

Misère ! Uniquement _American_ ou _Default_ Et le français, où est-il ?

Nous allons plutôt installer `aspell` alors, car il est utilisé par certains logiciels comme dépendance et il peut s’utiliser aussi comme un logiciel normal. Il se trouve qu’Emacs hérite des dictionnaires d’aspell.

Quittez Emacs avec `C-x C-c`, si vous avez une commande en cours via la commande `M-x`, saisissez la commande `C-g` pour l’interrompre et revenir dans le fichier, puis quitter. Si vous ne sauvegardez pas, Emacs vous le propose, Si vous répondez non, il vous demandera une confirmation, tapez `yes` pour confirmer.

Pour installer aspell et le dictionnaire français correspondant, un simple `sudo apt-get install aspell aspell-fr` suffit si vous êtes sur un OS dérivé de Debian, rien à paramétrer dans notre cas. Si toutefois vous souhaitez d’autres langues, faites suivre le code de langue après aspell. Ainsi, pour le dictionnaire italien, il faudra installer `aspell-it`.

Relancez Emacs et faites un `M-x ispell-change-dictionary`, Emacs vous demande fièrement la langue à choisir, tapez donc `fran` puis tabulation pour compléter en français. Une fois l’opération finie, Emacs vous gratifie d’un gentil `Local Ispell dictionary set to francais`, ce qui était le but recherché.

Ce problème étant réglé, voyons comment fonctionne la correction orthographique, adieu les clics droits et autres trucs qui prennent un temps fou, là encore, c’est tout au clavier.

Ouvrez votre fichier test si c’est pas déjà fait (`C-x C-f` pour ceux qui dorment au fond de la salle).

#### Les principales fonctions de correction

##### Corriger l'intégralité du fichier

Pour parcourir le fichier en entier à la recherche de fautes, lancez `M-x ispell`. Bam, il passe tout le fichier à la moulinette ! A chaque fois qu’il s’arrête il vous propose plusieurs choix, selon ce qu'il propose, vous pouvez faire plusieurs actions:

* **`Espace`** pour ne rien changer et passer à l’occurrence suivante
* Tapez le caractère entre parenthèses dans la liste de propositions en haut de l’écran pour corriger
* **`i`** pour ajouter le mot surligné par l’éditeur au dictionnaire courant pour qu'il ne le considère plus comme faux.

Ainsi de suite jusqu’à la fin du fichier. Si vous avez ajouté des mots dans le dictionnaire, une fois le fichier corrigé, il vous demande si vous voulez enregistrer les modifications dans le dictionnaire, acceptez donc, ça vous évitera de le refaire.

##### A la volée

Ce mode _à la volée_ nous fait gagner un temps précieux. On va d’abord l’activer via la commande `M-x flyspell-mode`. Maintenant, tapez quelque chose comme: _Bonjour, bonjour, bonjoour, bonjour_ (oui, avec deux « o » sur le troisième bonjour). On voit que le troisième bonjour est coloré, non seulement il y a une faute, mais en plus, il n’existe pas dans le dictionnaire.

Revenons sur le mot.

> C’est embêtant d’utiliser les flèches pour revenir sur le mot précédent ! Je croyais que c’était rapide Emacs !?

Utilisez donc `M-b` ! Une fois sur le mot mal orthographié, faites un `M-$` pour afficher les propositions de correction, ou l’enregistrer dans votre dictionnaire avec la touche `i`.

### Conclusion

Ayé, vous avez les principales fonctions basiques pour vous en sortir sous Emacs !

Si toutefois vous avez des remarques, des questions ou des incompréhensions, surtout n’hésitez pas, vu la taille du tutoriel, il y a sans doute à redire. Pour rappel, vous pouvez allez sur le dépôt d'article pour enrichir ou corriger cet article, via le bouton Gitlab en bas de page.

Et pour finir, parce que je suis mignon et parce que vous avez eu le courage d’aller jusqu’à la fin (ou pas d’ailleurs), voici un récapitulatif des commandes vues dans ce tutoriel. N'hésitez pas à l'imprimer, il peut servir de feuille de triche pour éviter de vous perdre le temps de les avoir dans la tête !

| Commande | Description |
|----------|-------------|
|`C-x C-f`| ouvre un fichier|
|`C-x C-s`| sauvegarde le fichier|
|`C-x C-c`| quitte Emacs|
|`C-_` ou `C-x u`| annule la dernière commande (défaire)|
|`M->`| pour aller à la fin du fichier|
|`M-<`| pour aller au début du fichier|
|`C-f`| avance d’un caractère|
|`C-b`| recule d’un caractère|
|`M-f`| avance d’un mot|
|`M-b`| recule d’un mot|
|`C-n`| avance d’une ligne|
|`C-p`| recule d’une ligne|
|`C-e`| va à la fin de la ligne|
|`C-a`| va au début de la ligne|
|`M-e`| va à la fin de la phrase|
|`M-a`| va à la fin de la phrase|
|`M-{`| va au début du paragraphe|
|`M-}`| va à la fin du paragraphe|
|`C+M-f`| va à la fermeture de parenthèse correspondante|
|`C+M-b`| va à l’ouverture de parenthèse correspondante|
|`C-espace`| Poser une marque|
|`M-w`| Copier|
|`C-y` ou `Shift-Insert`| Coller|
|`|C-w`| Couper|
|`M-h`| Selectionne le paragraphe|
|`C-x h`| Sélectionne le fichier entier|
|`C-k`| Supprime à partir du curseur jusqu’à la fin de la ligne|
|`M-k`| Supprime à partir du curseur jusqu’à la fin de la phrase|
|`M-x ispell-change-dictionary`| Sélectionner le dictionnaire pour la correction|
|`M-x ispell`| Scanner le fichier à la recherche de fautes|
|`M-x flyspell-mode`| Activer le mode de correction "à la volée"|
|`M-$`| Affiche les propositions de correction pour le mot surligné|
|`C-g`| Interrompt une commande M-x en cours|

Si toutefois vous êtes vraiment perdu, `M-x help` vous aidera !
