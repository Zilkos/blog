---
authors: ["zilkos"]
title: Fonctionnement de la cryptographie à clef publique
date: 2013-01-09
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Cryptographie"]
---
Dans un article précédent, j’avais dit que j’expliquerai quelques principes de fonctionnement de chiffrement et autres joyeusetés cryptographiques. Attention, cet article n’est ni scientifique, ni mathématique. Il s’adresse à des personnes non initiées et essaye de faire en sorte que vous compreniez simplement comment ça fonctionne.


### Mots, histoire et théorie

#### Un peu de vocabulaire

Aujourd’hui, apprenons donc comment fonctionne la cryptographie à clef publique. Tout d’abord, veuillez prendre connaissance du [précédent article](http://unixmail.fr/blog/vocabulaire-chiffrement) traitant du vocabulaire de  cryptographie histoire d’avoir quelques bonnes habitudes de langage, et surtout, les définitions des mots couramment utilisés de cet article. Ça vous évitera aussi probablement de vous faire méchamment molester par un puriste si vous dites ~~crypter~~ le mot qu’il ne faut pas dire...

#### Un peu d'histoire

Avant les années 1970, c’était surtout les militaires de tout poil qui utilisaient la cryptographie. Mais vers le début des _seventies_, les banques avaient la nécessité d’utiliser des messages chiffrés pour la sécurité de leurs transactions. A l’époque le chiffrement DES était sûr (par rapport aux connaissances et moyen de l’époque bien sûr). C’était plutôt bien, ils n’avaient qu’à échanger les clefs de chiffrement.

C’est en 1976 qu’une nouvelle manière de procéder apparaît  grâce à Whitfield Diffie et Martin Hellman, créateurs du concept de cryptographie à clef publique, aussi nommé **cryptographie asymétrique**.
Quelques années plus tard, en 1978, le premier exemple d’un système de chiffrement à clef publique est montré par Monsieur Rivest, Shamir et Adleman. Et oui, ça donne RSA, le fameux algorithme de chiffrement asymétrique encore utilisé actuellement. Quelques temps après ils fondèrent la société [RSA Security](http://www.emc.com/domains/rsa/index.htm), encore active de nos jours.

#### Un peu de théorie

Alors la cryptographie à clef publique, ça fonctionne comment ? Plutôt bien à vrai dire. Le principe est assez simple dans la théorie :

Admettons qu’un ami souhaite m’envoyer un message important et confidentiel. Je lui propose d’utiliser un coffre ainsi qu’un cadenas de ma fabrication avec une serrure un peu spéciale pour que je sois le seul à pouvoir l’ouvrir, au cas où s’il serait intercepté par un vil gueux durant son acheminement.

> Attention: Un message n’est pas forcément un mail. Considérez le plutôt comme une "transmission d’information" peu importe son support ou sa diffusion.

En premier lieu, je fabrique les clefs, il me faut :

* Une clef dite publique:

On l’appellera cadenas. Elle va permettre à Paul de fermer la boite qui contient son message.

> Etape 1: Je donne à Paul un cadenas pour qu’il ferme une boite qui contiendra son message. Il génère son message, le met dans la boite, et ferme cette dernière avec le cadenas que je lui ai donné. Donc pour l’instant, personne ne peut ouvrir ce cadenas, donc personne ne peut ouvrir la boite, donc personne ne peut prendre connaissance dudit message. Paul envoie sa boite cadenassée.

* Une clef dite privée:

Celle-ci permet d’ouvrir le cadenas. C’est uniquement moi qui l’utilise, pour pouvoir lire le message dans la boite. Et oui, si quelqu’un d’autre possède cette clef, il peut lui aussi lire les messages que normalement seul moi peux lire.

> Etape 2: Je reçois la fameuse boite de Paul avec le cadenas dessus. Je l’ouvre grâce à ma clef privée.

Ainsi, Paul peut enfermer son message dans une boite dont je suis le seul à posséder la clef, donc le seul à pouvoir lire le message.
Simplement, j’envoie plein de cadenas ouverts à mes correspondants pour que je puisse être le seul à pouvoir lire les messages qu’ils m’envoient une fois le cadenas fermé

Donc, si le message est sous forme de mail et qu’il est dupliqué sur des MTA durant son acheminement, l’admin du MTA, aux mains pleines de gras de chips et avide de petits secrets, ne pourra pas le lire, car il n'a pas la clef permettant d'ouvrir le cadenas.

#### Et si Paul se fait braquer son cadenas ?

Mais ! Dans quel monde cruel vivons-nous ! A dire vrai, c’est le seul désavantage de ce système. Dans le cas du chiffrement de mail via des clef GPG, il y a un système de confiance des clefs. C’est à dire que les personnes qui sont certaines que la clef émane bien de la personne en question et donc signent la clef en lui attribuant un niveau de confiance. M’enfin une usurpation  reste quand même rare. Pour les mails, voyez donc le paragraphe suivant qui apporte plus de précisions.

##### Le cas de GnuPG

Pour les mails, c’est GnuPG qui s’occupe de du chiffrement, le principe est très similaire, et tout aussi simple (attention, je reprends le comparatif avec les cadenas).

La clé privée sert à signer un message. Lorsque je signe un message avec ma clé (privée), que je suis le seul à posséder et qui est protégée par la passphrase (un mot de passe de préférence très compliqué), GnuPG en crée un résumé qui, grâce à ma clé publique, garantit au destinataire que je suis bien le signataire du message, que je suis bien celui que je prétends être. Attention, signer n’est pas chiffrer hein ! Je peux transmettre un mail en clair, en prouvant que c’est moi qui l’ai écris / envoyé, sans qu’il soit chiffré !

La clé publique sert à chiffrer le contenu d’un message, à l’image d’un cadenas ouvert comme dans l’exemple plus haut. Je chiffre un message avec la clé publique de mon correspondant (je ferme son cadenas sur mon message). Lui seul pourra le déchiffrer (ouvrir le cadenas) avec sa clé privée et sa passphrase.

#### Pour finir

Notez que ces clefs sont utilisées pour chiffrer du texte, des fichiers, un peu ce qu’on veut en fait ! Plusieurs serveurs de clefs existent (beaucoup même), mais ne vous inquiétez pas si votre correspondant ne possède pas de clef sur le serveur que vous utilisez, tous ces serveurs sont interconnectés et par conséquent ils communiquent entre eux.

Quelques liens utiles:

* [GnuPG (en)](https://www.gnupg.org)
* [MIT PGP Public Key Server (en)](http://pgp.mit.edu:11371/)
* [\[Wikipédia\] GnuPG (fr)](http://fr.wikipedia.org/wiki/GNU_Privacy_Guard)
