---
authors: ["zilkos"]
title: Irssi et le correcteur orthographique
date: 2014-12-16
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Irssi", "CLI", "XMPP", "Jabber"]
---

Aujourd’hui nous allons voir comment ajouter à **Irssi** un correcteur orthographique basé sur **Ispell** ! C’est un article court qui va à l’essentiel, toutes les manipulations n’excèdent pas 2 minutes !


### Irssi, le client IRC

Si vous connaissez IRC, vous connaissez Irssi. C’est le logiciel à connaître. Utilisé par beaucoup de monde, il est extensible à souhait, en console, terriblement bien fait, stable et extrêmement efficace.

Oui, mais voilà, il lui manque quand même une chose.

C’est souvent que pendant une longue discussion, on butte parfois sur l’orthographe d’un mot. C’est là qu’on intervient.

### Spell checking pour Irssi

C’est basé sur un script de la communauté Irssi, et quelques magouilles de ma part !

En premier lieu, nous allons installer Ispell, sans qui ce script n’a plus aucune utilité. Comme tout ce qui est bien, le script est écrit en Perl, nous installons donc le nécessaire à son fonctionnement (_Lingua::Ispell_) avec la commande suivante:

``` bash
sudo aptitude install ispell liblingua-ispell-perl
```

Pour vous qui n’êtes pas sous Debian, vous trouverez aisément comment installer ces paquets sur une autre distribution.

Tant qu’on y est, installons donc un dictionnaire français digne de ce nom pour ispell:

``` bash
sudo aptitude install ifrench
```

Parfait !

Maintenant, on se positionne dans le répertoire contenant les scripts irssi avec la commande suivante:

``` bash
cd ~/.irssi/scripts/
```
On récupère le script qui va bien avec un coup de wget:

``` bash
wget http://scripts.irssi.org/scripts/spell.pl
```

Basiquement, c’est fini. Sauf qu’en l’état, c’est pas super utilisable.

### Automatisons la chose !

Lancez Irssi, et chargez le script avec la commande suivante (dans Irssi hein) : `/script load spell.pl`

Histoire d’interroger le dictionnaire quand bon nous semble, on va lier la fonction de spellcheck à un raccourci clavier via la commande suivante: `/bind meta-s /_spellcheck`. `Meta-S` pour `Alt-s`. On lui indique de nous fournir les 3 premiers résultats pour une correction (donc les plus probables) via la commande: `/set spell_max_guesses 3`

Un petit `/save` et tout est ok !

En fait non, c’est pas encore ok. Si vous redémarrez Irssi et que vous faites un `Alt-S` dans une fenêtre de conversation, il va vous crier dessus comme quoi « spellcheck » n’est pas une fonction reconnue. Un `/script load spell.pl` règle le souci. Il faut donc charger le script à chaque démarrage d’Irssi.

Et comme vous êtes des experts Irssi, vous me dites ?

>Bah, suffit de mettre le script dans l’autorun !

Exact !

Allons dans .irssi/scripts: `cd ~/.irssi/scripts`, un petit `ls` vous montre un dossier `autorun`. Si il n’existe pas créez le (`mkdir autorun`). Une fois le dossier créé, mettez le script dedans via la commande: `mv spell.pl autorun/`.

Oui, les scripts présents dans le dossier `autorun` se lancent au démarrage d’Irssi, c’est l’équivalent d’un `/script load monscript` au démarrage ! Ainsi, votre raccourcis clavier `Alt-S` ira correctement interroger Ispell sans aucune obligation de charger quoique ce soit au démarrage.

Enjoy !
