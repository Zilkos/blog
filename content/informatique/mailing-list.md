---
authors: ["zilkos"]
title: Les mailing-lists
date: 2016-03-26
lastmod: 2018-11-18
categories: ["Unixmail"]
tags: ["Mailing-list"]
---

Les _mailing lists_ sont un moyen de communication accessible à tous. Dans cet article, on va se pencher sur leur fonctionnement, vous y trouverez toutes les informations utiles pour vous inscrire et participer. Simple et rapide !


## _"J'ai pas le temps !"_

Pour s'inscrire rapidement à la liste discussion, envoyez un mail à `discussion-subscribe@unixmail.fr`. Pour la liste support, envoyez un mail à `support-subscribe@unixmail.fr`. Dans les deux cas, suivez les indications d'UnixBot pour mener à bien votre inscription ! Une fois inscrit, envoyez votre message à `discussion@unixmail.fr` ou `support@unixmail.fr` selon votre propos pour commencer à échanger !

### Les listes Unixmail

##### Discussion

La première liste est nommée `discussion@unixmail.fr`. Elle sert principalement à émettre des remarques, commentaires. Outre les articles elle permet aussi de discuter de plein d'autres sujets, à votre convenance. Si vous souhaitez réagir sur un article ou lancer un débat enflammé comme quoi Ubuntu ça pue, c'est l'endroit parfait (mais en argumentant un peu quand même !).

##### Support

La deuxième liste est une liste de support, banalement nommée `support@unixmail.fr` Elle permet de poser des questions techniques en rapport à des articles ou non, à demander conseil sur telle ou telle manière de faire, techno, distribution, logiciel, paramétrages, etc... En bref, c'est une liste dédiée aux problèmes et demande de points techniques. Elle peut aussi servir pour signaler des coquilles sur les articles pour ceux ne maitrisant pas Git.


### Comment ça marche ?

Comme beaucoup de listes de distribution, c'est basé sur le mail, et uniquement ça.

  - **Inscription**

Pour vous inscrire, il suffit d'envoyer un mail à `discussion-subscribe@unixmail.fr` pour la liste discussion et `support-subscribe@unixmail.fr`. Bien évidemment, inscrivez vous avec l'adresse que vous souhaitez utiliser pour communiquer sur ces listes. Peu importe le contenu ou le sujet, l'action d'inscription est gérée par l'adresse, pas par autre chose.

Une fois ce premier mail envoyé, UnixBot, le gardien les listes vous répondra et vous demandera d'envoyer un mail de confirmation, peu importe le corps ou l'objet. Cette confirmation permet de vérifier deux choses:

  * Que vous pouvez bien envoyer du mail sur la liste
  * Que c'est bien vous qui vous inscrivez

C'est tout.

Je vous invite fortement à faire une règle de redirection pour ranger ces mails directement dans des dossiers.


  - **Désinscription**

Envoyez simplement un mail à `discussion-unsubscribe@unixmail.fr` pour être supprimé de la liste, le fonctionnement est le même pour la liste de support: `support-unsubscribe@unixmail.fr`. Mon fidèle serviteur UnixBot fera le nécessaire pour vous désinscrire et vous le notifiera chaleureusement.

En gros, c'est tout.

Pour ceux ne sachant vraiment pas ce qu'il en est, le principe est le suivant. Si une personne (autorisée) envoie un mail à `discussion@unixmail.fr`, UnixBot va le transmettre à tous les inscrits de la liste. Le mail sera formé d'une telle manière que quiconque y répondra, répondra en fait à la liste, UnixBot distribuant la réponse à tous. C'est ainsi qu'on forme des conversations entre tous.

Donc le principe est comme un forum mais en version mail. Soit on répond à un sujet déjà existant (un premier mail envoyé à la liste), soit on en crée un (en écrivant) à la liste. Quoi de plus simple ?

### Une question de respect

Afin de garder une ambiance sereine et de s'y retrouver, merci de respecter autant que possible la [Nétiquette](https://fr.wikipedia.org/wiki/Nétiquette). Si vous avez besoin de contacter le propriétaire des listes, envoyez un mail avec votre réclamation à `discussion-owner@unixmail.fr` ou, pour la liste de support `support-owner@unixmail.fr`

Je vous rassure tout de suite, les pénibles seront rapidement écartés pour le confort de tous.

N'hésitez pas à participer, tout le monde est le bienvenu !
