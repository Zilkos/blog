---
authors: ["zilkos"]
title: 'Mutt: Configuration minimale et première utilisation'
date: 2015-03-11
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Mutt", "CLI", "Mail"]
toc: true
---

On poursuit dans la série des articles consacrés au client mail **Mutt**. Dans cet épisode, nous établirons une configuration minimale pour récupérer et envoyer nos mails et nous commencerons à manipuler ce logiciel.

C'est parti !

> Les autres épisodes sont [ici](tags/mutt) !

Attention, c'est relativement long, J'ai tout testé globalement sur une machine virtuelle, comptez un peu plus d'une demie heure pour lire et effectuer toutes les manipulations énoncées dans cet article, sans doute moins si vous connaissez déjà Mutt. Vous trouverez en fin d'article un tableau récapitulatif des raccourcis clavier. Aussi, cet article est basé sur mes connaissances personnelles et mon expérience sur Mutt. Si des gourous de Mutt passent par ici et constatent des coquilles, n'hésitez surtout pas à me contacter pour faire en sorte que cette suite d'articles soit la plus précise possible. Au pire, le dépôt d'article est accessible !

J'essaye de faire en sorte que cette suite d'articles soit une porte d'entrée pour le débutant sous Mutt. Ma volonté est de construire quelque chose en français que je n'ai pas réussi à trouver sur le web, ni en français, ni en anglais. Je ne prétends pas "créer" quelque chose d'exceptionnel, mais j'essaye d'être le plus pédagogue possible pour que personne ne soit perdu. Enfin, cette suite d'articles ne se substitue absolument pas à la lecture de la documentation, n'hésitez pas à y jeter un œil. Le but est d'avoir une configuration minimale confortable et qui fonctionne.

## Quelques considérations

> Durant cet article `$USER` fait référence à l'utilisateur courant. Dans mon cas, `/home/$USER/` équivaut à `/home/zilkos`. Veillez à bien remplacer `$USER` par votre nom d'utilisateur.

Avant de commencer, on va voir comment Mutt s'organise. Certes, vous allez faire le plus gros du travail, mais Mutt pose quelques "conditions" de base.

Par défaut Mutt regardera à deux endroits:

* **`/var/spool/mail/$USER`**

Cet emplacement est nommé le _mailspool_, quand vous relèverez vos mails, ils tomberont dans ce fichier (oui, c'est un fichier, et pas un dossier). C'est un peu le fichier courant, dans celui où vous travaillerez le plus (forcément, vu que les mails arrivent ici).

* **`/home/$USER/Mail`**

Mutt est intelligent (ou du moins très bien fait), et se doute bien que personne ne passe sa vie dans sa boite de réception. `/home/$USER/Mail` est donc là pour organiser vos archives.

Pour faire court, voici le workflow basique par défaut dans Mutt:

1. Les mails entrants sont stockés dans `/var/spool/$USER/mail`.
2. Une fois lu (ou pas), tu peux les stocker dans `/home/$USER/Mail`

> Notez que `/home/$USER/Mail` est l'emplacement par défaut. Il est possible de stocker les archives ailleurs.

## Une configuration basique

> Durant tout le tutoriel, j'utiliserai l'adresse mail `test@unixmail.fr`. Inutile de la spammer, c'est une fausse adresse.

On va se faire une configuration basique afin de commencer à toucher au fameux fichier `.muttrc` et afin d'apprendre à utiliser Mutt.

### A l'attaque de muttrc !

`/home/$USER/.muttrc`, c'est **le** fichier de configuration principal de Mutt. C'est ici que tout va se jouer et autant vous le dire tout de suite, on va y passer énormément de temps. À titre indicatif, mon `.muttrc` fait 404 lignes avec les commentaires:

```bash
~ wc -l .muttrc
404 .muttrc
```

Pour l'instant, on va en faire un tout simple, mais il est important qu'il soit organisé, aussi bien pour le faire évoluer, que pour l'éditer plus simplement dans le futur et trouver la bonne information au bon endroit.

##### Explication du fichier .muttrc

Le fichier `.muttrc` est lu par Mutt au lancement du logiciel. Il le lit ligne par ligne en ignorant les commentaires. Pour faire un commentaire, il suffit de faire commencer la ligne par un `#`. Une ligne est un paramètre et peut prendre diverses formes selon le paramètre.

Ci après un petit exemple résumant comment est construit le fichier:

```
####### CONFIGURATION MUTT
# Créé le: xx/xx/xxxx
# Dernière modification le: xx/xx/xxxx

#### GENERALITES
set VARIABLE = VALEUR
set VARIABLE = "CONTENU" #Commentaire explicatif
```

Vous pouvez mettre un commentaire en fin de ligne. Bien évidemment, tout ce qui suit le `#` jusqu'à la fin de la ligne sera ignoré.

Il y a d'autres formes de paramétrage, notamment via des mots clefs propres à Mutt pour le configurer, nous verrons cela au fur et à mesure.

Bien, maintenant qu'on sait comment la configuration est architecturée, construisons une première base nous permettant d'envoyer et recevoir des mails.

## Un muttrc basique

Avant toute chose, créez le fichier via un `touch /home/$USER/.muttrc` ou via votre éditeur favoris et commentez le début du fichier pour y intégrer divers commentaires (par exemple, l'URL de cet article, si toutefois vous avez besoin d'y revenir, la date, etc...).

### Identité

Pour envoyer et recevoir des mails, il faut définir plusieurs informations. Commençons par notre identité. Mutt génère un champ `From:` à partir du nom complet de l'expéditeur et de son adresse mail. Il est souvent de la forme `From: Prénom Nom <adresse>`.  Nous allons donc indiquer à Mutt trois choses:

1. Notre nom complet
2. Notre adresse mail
3. Qu'on veut générer un champs `From` se basant sur les points 1 et 2

Hop, mettons tout ça dans notre `.muttrc`:

```bash
# Adresse électronique de l'expéditeur
set from = "test@unixmail.fr"

# Nom complet de l'expéditeur
set realname = "Test Unixmail"

# Génération du champs from
set use_from = yes
```

Comme vous le voyez, c'est assez simple. Une option, une valeur.

Sachez que l'option `use_from` est par défaut à `yes`, comme nous le dit la page de manuel:

```
use_from
        Type: boolean
        Default: yes

        When set, Mutt will generate the `From:' header field  when
        sending  messages.   If  unset,  no `From:'  header  field
        will be generated unless the user explicitly sets one using
        the "my_hdr" command.
```

Attention si vous utilisez le paramétrage `my_hdr`, attention...

La directive `my_hdr` pour fixer soi-même le header `From` est dangereuse. En effet, si vous recevez un mail sur un machine, puis que vous déplacez le message sur une autre machine, si `reverse_name` est indiqué dans votre configuration, lorsque vous répondez au mail il indiquera dans le header `From` l'adresse à laquelle vous avez reçu le message (donc sur l'ancienne machine). Dans le cas contraire, il utilisera le `From` basé sur l'adresse actuelle, ce qui peut provoquer des soucis, en plus de l'incompréhension de votre interlocuteur.

Comme nous sommes mignon avec nos correspondants, on fixe donc la variable `reverse_name`, ainsi si on migre nos mails sur une autre machine, le header `From` restera cohérent. Ajoutez donc la ligne suivante:

```bash
# Lors d'une réponse, utiliser l'adresse originale à laquelle a été envoyé le mail
set reverse_name
```

### Signature
Il sera bon d'inclure une signature à nos mails. Il suffit pour cela de lui indiquer un fichier à utiliser en tant que signature:

```bash
set signature=~/.signature
```

Créez et remplissez le fichier avec signature, comme un fichier texte basique.

### Répertoires de travail

Fixons maintenant les répertoires de travail. On va fixer le fichier de spool, donc là où Mutt ira mettre les nouveaux messages fraîchement récupérés. On va aussi fixer notre dossier d'archives ainsi qu'un dossier temporaire qui nous sera utile pour la suite.

```bash
# Définition des dossiers de travail de Mutt
set spoolfile = /var/spool/mail/$USER
set folder    = ~/mail
set tmpdir    = ~/tmp
```

Dans ce bloc, vous pouvez laisser `$USER` car cette variable sera remplacée par le nom de l'utilisateur courant lançant Mutt (c'est une variable système comme `$EDITOR` que vous pouvez afficher avec la commande `echo $USER` par exemple).

On a défini notre fichier de spool à l'emplacement par défaut. On lui a indiqué que nos archives doivent être dans `/home/$USER/mail` et notre fichier temporaire dans `/home/$USER/tmp`. Bien évidemment, vous pouvez changer comme bon vous semble ces emplacements.

> `set spoolfile` sera redéfini plus loin dans cet article si vous utilisez IMAP !

Créons les dossiers:

```
mkdir /home/$USER/tmp /home/$USER/mail
```


### Éditeur

Mutt n'intègre pas d'éditeur, et se base par défaut sur la variable système `$EDITOR`. Fixons l'éditeur que nous voulons utiliser pour taper nos mails via la ligne suivante:

```bash
# Editeur utilisé pour rédiger les mails
set editor = 'vim'
```

Je suis vraiment sympa... Vraiment sympa parce que cette ligne en l'état, c'est la misère pour écrire un mail avec vim. Et oui, un mail, c'est un nombre maximal de caractères sur une ligne !

Pour les utilisateurs de vim, indiquez plutôt ceci:

```bash
set editor = "vim -c 'set tw=72' -c 'set wrap'"
```

Avec ceci, vim coupera intelligemment vos lignes là où il faut.

### POP3(S)

Si vous êtes un utilisateur fan de POP3(S), optez pour ces paramétrages:

```bash
set pop_user = "test@unixmail.fr"
set pop_pass = "votre mot de passe POP"
set pop_delete = yes / no
set pop_host = "pops://pop.unixmail.fr:995"
set pop_last = yes / no
```

Alors, quelques explications s'imposent:

* **`pop_user`**

Votre identifiant sur le serveur pop, dans ce cas c'est l'adresse entière qui est utilisée (ça dépend des fournisseurs).


* **`pop_pass`**

Sans grande surprise, le mot de passe associé au compte utilisateur.


* **`pop_delete`**

Si fixé sur `yes`, les messages récupérés seront supprimés du serveur. Si fixé sur `no`, les messages sont récupérés sur le serveur, mais seront encore présents sur le serveur (et donc récupéré encore une fois à la prochaine relève, pour régler ce comportement, voir `pop_last`).


* **`pop_host`**

Indique le serveur et port à utiliser, je l'ai volontairement "surchargé" pour vous montrer ce qu'il est possible de faire.

Le pattern complet incluant toutes les possibilités est le suivant:

```
[pop[s]://][username[:password]@]popserver[:port]
#exemple: pops://toto@example.net:MotDePasse@monserveur.fr:995
```

Notez que **`set pop_user`** et **`set pop_pass`** deviennent inutiles si vous les indiquez dans **`set pop_host`**.

* **`pop_last`**

Chaque mail dispose d'un identifiant **unique**. Le fonctionnement de **`pop_last`** quand il est fixé à **`yes`** est de comparer les identifiants des mails dans le spool (donc les mails déjà récupérés) avec ceux présents sur le serveur. Pour chaque mail, si l'identifiant est le même, cela veut dire que le mail est déjà présent dans le spool, donc il ne le récupère pas (autrement, ça fait doublon). Si l'identifiant n'existe pas dans le spool, ça veut dire que le mail n'a jamais été récupéré, donc il le récupère depuis le serveur.

Passer **`pop_last`** à **`no`**, et Mutt récupérera tous les mails présent sur le serveur, même s'ils ont déjà été récupéré auparavant

Tout ceci fonctionne uniquement si vous laissez les mails sur le serveur après récupération, comportement défini par **`pop_delete`**.

Je vous conseille fortement de ne pas laisser les mails sur le serveur, car la [RFC 1460](https://tools.ietf.org/html/rfc1460) qui décrivait le comportement de la commande LAST (le mécanisme utilisé ici) date de Juin 1993, puis supprimée dans la [RFC 1725](https://tools.ietf.org/html/rfc1725), et la [RFC 1939](https://www.ietf.org/rfc/rfc1939.txt) qui définit le protocole POP3 n'en fait pas mention.

### IMAP(S)

Si vous êtes un utilisateur fan d'IMAP(S), c'est complètement différent et certains paramétrages présentés dans le début de l'article sont à réécrire.

```bash
# On s'identifie dès le lancement de Mutt
set spoolfile="imaps://test:MotDePasse@imap.unixmail.fr/INBOX"
# On fixe la boite de réception
set folder="imaps://imap.unixmail.fr/INBOX"
# Les messages envoyés sont sauvegardé dans le dossier IMAP "Sent"
set record="Sent"
# Idem pour les brouillons
set postponed="Drafts"
```

D'autres paramètres pour les utilisateurs d'IMAP qui peuvent être utiles:

* **`set imap_authenticators`**

Permet de choisir la méthode d'authentification sur le serveur, si la première n'est pas disponible, il passe à la deuxième. Exemple:

```bash
set imap_authenticators="gssapi:cram-md5:login"
```


* **`imap_delim_chars`**

Ce paramètre contient une liste des caractères de séparation pour les dossiers IMAP. Par défaut, la liste contient `/.`.


* **`imap_force_ssl`**

Si fixé sur `yes`, Mutt utilisera tout le temps SSL pour se connecter au serveur IMAP. Chaudement recommandé.

### SSL / TLS

Quelques paramétrages disponibles pour les connexions SSL / TLS qui peuvent vous servir selon vos serveurs de messagerie:

* **`ssl_usesystemcerts`**

Si fixé à `yes` (valeur par défaut), Mutt utilisera le magasin de certificats du système.


* **`ssl_use_sslv2`**

Par défaut à `yes`, Mutt utilisera SSLv2.


* **`ssl_use_sslv3`**

Par défaut à `yes`, Mutt utilisera SSLv3.


* **`ssl_use_tlsv1`**

Par défaut à `yes`, Mutt utilisera TLSv1 dans le processus d'authentification SSL.

### SMTP

C'est relativement similaire au reste:

* **`set smtp_pass="votreMotDePasseSMTP`**

Ça se passe de commentaire.

* **`set smtp_url = [pop[s]://][username[:password]@]popserver[:port]`**

La construction de la chaîne est la même que pour le POP !


## Testons !

En guise de résumé, je vais vous "coller" une configuration POP que nous allons tester pour apprendre à se servir de Mutt. Toutes les lignes de configurations ont été vues plus haut, aucune surprise !

Le `.muttrc` utilisé:

```bash
#### CONFIGURATION MUTT
# Adresse électronique de l'expéditeur
set from="test@unixmail.fr"

# Nom complet de l'expéditeur
set realname = "Test Unixmail"

# Génération du champs from
set use_from = yes

# Signature
set signature=~/.signature

# Définition des dossiers de travail de Mutt
set spoolfile = /var/spool/mail/$USER
set folder    = ~/mail
set tmpdir    = ~/tmp

# Vim comme éditeur
set editor = "vim -c 'set tw=72' -c 'set wrap'"

### CONFIGURATION POP3
set pop_user = "test@unixmail.fr"
set pop_pass = "votre mot de passe POP"
set pop_delete = yes # On ne garde pas les mails sur le serveur
set pop_host = "pops://pop.unixmail.fr:995" #inutile de tester, c'est faux :)
# set pop_last = yes / no INUTILE vu que pop_delete à yes

### CONFIGURATION SMTP
set smtp_pass = "MotDePasseSMTP"
set smtp_url = "smtps://test@unixmail.fr@smtp.unixmail.fr:465" # Oui, SSL :)

```

Avant de lancer Mutt, je prévois un souci: comme nous n'avons jamais reçu de mail, le fichier `/var/spool/mail/$USER` n'existe pas, et comme il est fixé dans notre configuration via la ligne `set spoolfile = /var/spool/mail/$USER`, il y a de fortes chances que Mutt vous gratifie d'un gentil:

> `/var/spool/mail/$USER: Aucun fichier ou dossier de ce type (errno = 2)`

en bas de page.

Rassurez-vous, c'est normal, Mutt ne peut écrire dans un fichier inexistant, nous allons le créer.

En fait, nous allons le créer en nous envoyant un mail à nous-même depuis le système avec la commande suivante:

`echo Texte | mail $USER`

Et hop, maintenant, vous avez un mail ! Le premier sous Mutt ! Émouvant hein ?

> MENTEUR ! Comment tu le sais qu'on a un mail ?

Pour prouver, et expliquer un peu les choses, et surtout pour savoir comment fonctionne ce fichier de spool, affichez le contenu de votre fichier de spool avec la commande suivante:

`cat /var/spool/mail/$USER`

Vous aurez probablement un retour du genre:

```
From test@xxxx Wed Mar 11 13:36:08 2015
Return-path: <test@xxxx.fr>
Envelope-to: test@xxxx.fr
Delivery-date: Wed, 11 Mar 2015 13:36:08 +0100
Received: from test by xxxx.fr with local (Exim 4.72)
        (envelope-from <test@xxxx.fr>)
        id 1YVfrs-0007Ix-Jx
        for test@xxxx.fr; Wed, 11 Mar 2015 13:36:08 +0100
Date: Wed, 11 Mar 2015 13:36:08 +0100
Message-Id: <E1YVfrs-0007Ix-Jx@xxxx.fr>
To: test@xxxx.fr
From: Test <test@xxxx.fr>

Text

```

Alors ? Convaincu ? Envoyez vous un autre mail avec la commande donnée plus haut, et affichez encore une fois le contenu de votre spool...

Et oui, vous avez un fichier, avec deux mails à la suite ! Le spool est donc au format **mbox** un format de fichier dédié au mail qui "rassemble" tous les mails dans un seul fichier. Chaque mail est séparé par une ligne vide. Il est très facile de scripter quelque chose pour analyser un tel fichier !

Bon, fini de jouer, passons à la manipulation de Mutt !

## Mutt, utilisation basique

Lancez Mutt avec un joli `mutt` en guise de commande dans un terminal en plein écran (touche `F11` sur la plupart des émulateurs de terminal). Jusqu'à la fin de l'article, ouvrez grands vos oreilles, je vais utiliser des termes que je réutiliserai jusqu'à la fin !

### "Mais ! Il fait tout noir !"

Oui, forcément, vous n'avez pas de mail, ou très peu et les couleurs de bases sont... pas très agréables, j'en conviens.

À gauche de l'écran, vous avez une grosse colonne vide, on y reviendra plus tard. En haut de l'écran on retrouve les actions courantes, au milieu le contenu et en bas des informations. Notez que c'est organisé de la même façon pour tous les écran:

```
q:Quitter  d:Effacer  u:Récup  s:Sauver  m:Message  r:Répondre  g:Groupe  ?:Aide
   1 N F Mar 11 To test@xxxx (   1)
   2 N F Mar 11 To test@xxxx (   1)
```

La barre du haut vous présente quelques raccourcis courants et utiles, je n'ai pas forcément besoin de vous les présenter, vu que tout est correctement traduit en français. Il y a quand même quelques subtilités concernant certains raccourcis:

* `d`: Efface un message. Comportement modifié par le paramétrage **`set pop_delete = yes`** (supprime du serveur également).
* `s`: Sauve un message dans le sens "Déplace le message dans un fichier d'archive".
* `g`: Permet de répondre à tous les destinataires.
* `?`: Affiche la liste des commandes, utilisez `q` pour quitter la liste.

Sur la deuxième et troisième ligne, nous avons la liste des mails dans le spool. Une ligne pour un mail avec diverses informations...

* `1`: C'est le numéro du mail, tout simplement
* `N`: C'est un **indicateur d'état** pour ce mail
* `F`: C'est un deuxième indicateur reflétant le destinataire du message.
* `Mar 11`: C'est la date, du moins un bout de date
* `To test@xxxx`: C'est l'expéditeur. Ce n'est pas très clair pour le moment, nous y reviendrons.
* `(   7)`: C'est la taille du mail qui peut être en nombre de ligne ou en kilo-octet
* Une colonne vide, mais une colonne quand même qui contient le sujet (que nous avons omis lors de l'utilisation de la commande pour nous envoyer un mail)

> Cet écran présentant une liste des mails est nommé l'**INDEX**

Afin de déblayer un peu tout ça et d'avoir une vraie vue correcte comme si c'était quelqu'un qui vous envoyait un mail, envoyez-vous un mail avec une autre adresse. Avec un vrai sujet et un peu de contenu.

Une fois fait, retournez sous Mutt. Oh misère, comment fait-on pour récupérer les mails ?

### Mutt comme des grands

Pour récupérer les mails, c'est `G` et j'ai bien dit `G`, pas `g` ! Si vos paramétrages sont bons (utilisateur, mot de passe), la barre du bas vous gratifie d'un:

`Nouveau(x) message(s) dans cette boîte aux lettres.`

Joie ! Maintenant vous comprenez mieux les différents affichages, en cernant correctement l'expéditeur du message ainsi que le sujet.

Il est vrai que par défaut, c'est peu lisible. Rassurez-vous, nous modifierons les éléments à afficher sur un prochain article.

Bien, nous savons lancer Mutt, et rapatrier les nouveaux messages. Maintenant, on va lire le message que vous vous êtes envoyé.

Avec les flèches directionnelles haut et bas, positionnez-vous sur le message que vous souhaitez lire et appuyez sur `Entrée`.

> L'écran de lecture d'un mail est nommé: **PAGER** (in english, please)

Dans mon cas, j'ai ceci d'affiché:

```
i:Quitter  -:PgPréc  <Space>:PgSuiv v:Voir attach.  d:Effacer  r:Répondre  j:Suivant ?:Aide
Date: Wed, 11 Mar 2015 14:04:04 +0100
From: Zilkos <zilkos@unixmail.fr>
To: ****@unixmail.fr
Subject: Sujet !
User-Agent: Mutt/1.5.21 (2010-09-15)

Contenu

--
Zilkos - http://unixmail.fr - @Zilkos
Clé GPG: 0xc265aad7501a545f
Fingerprint : 9F35 0B91 1390 316C 5EA6  464E C265 AAD7 501A 545F
Merci de préférer les formats ouverts pour vos pièces jointes.
```

En bref, un mail, et comme tout à l'heure, on a une barre en haut nous présentant les raccourcis les plus courants, la plupart son explicites:

* `i` ou `q`: Quitte le pager pour retourner à l'index.
* `v`: Ouvre un écran présentant la liste des pièces jointes du mail
* `j`: Retourne à l'index et met en surbrillance le prochain message
* `g`: Non affiché, il permet comme depuis l'index de "Répondre à tous"
* `d`: Supprime un message. Comportement modifié par **`set delete=yes`** qui les efface aussi de l'index quand il est resynchronisé.

Sur la barre du bas, nous avons aussi plusieurs informations, notamment le numéro du mail que nous sommes en train de lire (`X/X`), l'expéditeur, le sujet et tout à droite, le pourcentage d'affichage du mail ou `(all)` si la totalité du mail loge sur un écran.

### Communiquons !

Appuyez sur `q` pour retourner sur l'index. Nous allons maintenant écrire un mail, appuyez donc sur la touche `m` pour créer un nouveau message.

En bas de l'écran, le prompt est affiché en vous demandant à qui vous souhaitez écrire. Indiquez donc l'adresse voulue. Une fois l'adresse tapée, validez avec entrée et ce coup-ci, Mutt vous demande de remplir le sujet, faites de même que pour le destinataire.

C'est à ce moment-là que votre éditeur arrive (vim, dans notre exemple).

Là, vous devez me dire _"Ha, mais c'est pour ça qu'il faut un dossier `tmp` !_ et je vous réponds: Oui, le mail que vous êtes en train d'écrire est pour l'instant stocké dans le dossier `tmp` ! On verra par la suite que ce dossier `tmp` servira aussi pour d'autres choses.

Tapez votre mail, puis sauvegarder le fichier et quitter l'éditeur, donc sous vim `:wq!` fait le travail.

Une fois l'éditeur fermé, vous êtes devant un nouvel écran, vous présentant encore une fois une barre avec des raccourcis souvent utilisés, un pavé qui résume l'expéditeur (histoire de vérifier la génération du champs `From` !), le destinataire, le chiffrement utilisé s'il y en a.

Quelques raccourcis utiles pour cet écran:

* `y`: Envoie le message.
* `q`: Annule le message en vous proposant de l'ajourner pour éventuellement le reprendre plus tard (c'est donc un brouillon).
* `c`: Permet de mettre une adresse mail en copie carbone (champs Cc).
* `b`: Permet de mettre une adresse mail en copie carbone cachée (champs Bcc).
* `s`: Permet d'éditer le sujet.
* `a`: Permet d'ajouter une pièce jointe. C'est simple, commencez à taper votre chemin, puis utilisez `Tab` pour compléter.
* `d`: Permet d'éditer la description de la pièce-jointe.

Si vous ajournez le message, lors du prochain appui sur `m` pour créer un nouveau message, il vous demandera si vous souhaitez rouvrir un message ajourné.

Notez que la barre du bas vous donne aussi des informations concernant le mail que vous venez de rédiger (la taille, le nombre de pièce-jointe,etc...).

> Il est où le mail envoyé ?

Mutt étant docile, il vient de créer un dossier nommé `Sent` dans votre dossier personnel (`/home/$USER`). C'est moche, on va changer ça. Fermez Mutt avec `q` et retournez éditer `.muttrc`. On va y apporter quelques modifications pour les mails envoyés, les brouillons et le format de boite aux lettres.

On y rajoute les choses suivantes:

* **`set mbox_type = mbox`**

Permet de définir le type de la boite mail, au choix `mbox`, `MMDF`, `MH`, ou `Maildir`.


* **`set record = "~/mail/sent"`**

Dossier dans lequel seront ajoutés les mails envoyés. Pensez à supprimer `/home/$USER/sent` si Mutt en a créé un (faites attention s'il n'est pas vide, vous perdrez vos mails envoyés !).


* **`set postponed = "~/mail/postponed"`**

Dossier dans lequel seront ajoutés les brouillons (les messages ajournés).


* **`set read_inc = 1`**

C'est un indicateur de progression de lecture quand vous ouvrez un dossier de mail qui en contient énormément.


* **`set pager_stop = yes`**

Permet de ne pas passer au message suivant lorsqu'on atteint la fin du mail courant dans le pager.


* **`set smart_wrap = yes`**

Quand une ligne dépasse la largeur de votre écran, Mutt renvoie automatiquement à la ligne à la fin d'un mot (`no`: renvoie à la ligne une fois le bord de d'écran atteint, donc possiblement au milieu d'un mot).


* **`set markers = yes`**

Lié au paramétrage précédent, il affiche un `+` devant une ligne tronquée.



### Sauvegardons !

Relancez Mutt, lisez un mail quelconque et appuyez sur la touche `s` pour le sauvegarder. Le comportement de Mutt, c'est d'être plutôt sympa en vous disant:

`Sauver vers une BAL ('?' pour avoir la liste) : =zilkos`

Quelques explications sur cette ligne:

* Appuie sur `?` pour voir la liste des boites et sélectionner celle que tu veux.
* Si t'en a pas, je t'en crée une nommée `zilkos` si tu appuie sur `Entrée`.
* Tu peux enlever `=zilkos` et écrire toi-même le nom de la boite à créer, quand tu appuieras sur `Entrée`, Mutt sera charmant et te demanderas de confirmer la création de cette boite.

## Fin !

Hop, nous savons comment lire / écrire / envoyer / sauvegarder des mails.

Au menu du prochain épisode:

1. Pourvoir lire les pièces jointes automatiquement
2. Pouvoir gérer dans la barre de gauche une liste de boites et se déplacer entre les boites.
3. Peaufiner la configuration de Mutt en ce qui concerne les citations, les couleurs, les transferts de mail.

En quelques mots, le rendre utilisable plus simplement, le rendre plus agréable à regarder.

Et sans doute d'autres choses que j'oublie.


> J'ai conscience qu'en l'état, votre Mutt est est un peu brut et pas utilisable de manière simple. Mais comme cet article est relativement long, je suis obligé de repousser le reste des fonctionnalités à l'épisode suivant. Un peu de patience !

## Tableau des raccourcis

Dans tous les cas si vous êtes perdu, `Ctrl + g` annule la commande en cours (oui, comme sous Emacs) !

| INDEX |    |
|---|---|
| **Raccourci**  | **Action**   |
| `c` | Changer de boite aux lettres |
| `C` | Copier le message courant dans une autre boite aux lettres |
| `d` | Effacer le mail |
| `F` | Marquer le message comme _important_ |
| `G` | Effectue une relève de mail |
| `N` | Marquer le message comme _nouveau_ |
| `q` | Sauvegarder les modifications et quitter |
| `s` | Sauvegarde le message dans un autre dossier |
| `u` | Récupérer un mail effacé |
| `v` | Afficher les pièces jointes du mail |
| `g` | Répondre à tous |
| `Entrée` | Affiche le message dans le pager |
| `Tab` | Aller au prochain **nouveau** message |
| `Ctrl + L` | Redessine l'écran |


| INDEX: Différentes manières d'envoi |    |
|----|----|
| **Raccourci**  | **Action**   |
| `m` | Composer un nouveau message |
| `r` | Répondre à l'expéditeur |
| `g` | Répondre à tous les destinataires |
| `L` | Répondre à la liste de diffusion |
| `f` | Faire suivre un message (forward) |
| `b` | Réexpédier un message (bounce) |



| INDEX: Indicateurs d’états |    |
|---|---|
| **Indicateur**  | **Signification**   |
| `D` | Le message est supprimé (ou marqué pour suppression) |
| `d` | Le message a des pièces-jointes marquées pour suppression |
| `K` | Contient une clé PGP publique |
| `N` | Nouveau message (new) |
| `O` | Ancien message (old) |
| `P` | Le message est chiffré avec PGP |
| `r` | Vous avez répondu à ce message |
| `S` | Message signé par PGP et signature vérifée |
| `s` | Message signé |
| `!` | Le message est considéré comme important |



| PAGER |   |
|---|---|
| **Raccourci**  | **Action**   |
| `Entrée` | Descend d'une ligne |
| `Espace` | Page suivante |
| `-` | Page précédente |
| `S` | Sauter le texte cité |
| `T` | Afficher / Masquer le texte cité |
| `^` | Aller au début du message |
| `$` | Aller à la fin du message |


Si vous constatez qu'il manque des choses, que des passages sont pas clairs ou si vous souhaitez contribuer à cet article, le d^pôt d'article est fait pour ça ! Voyez donc l’icône Gitlab en bas de page.
