---
authors: ["zilkos"]
title: 'Mutt: Un peu de confort !'
date: '2018-03-11'
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Mutt", "CLI", "Mail"]
toc: true
---

> Les autres épisodes sont [ici](tags/mutt) !

Bienvenue dans ce troisième épisode de la série d'article consacrée au célèbre client mail en console: **Mutt** !

Dans les épisodes précédents nous avons installé le nécessaire et nous avons fait un tout petit peu de configuration pour avoir nos mails. Tout ceci est terriblement archaïque et pas très confortable pour l'instant, mais c'est fonctionnel. Dans cet article on va aller plus loin dans la configuration pour obtenir un niveau de souplesse confortable. On va également apprendre à automatiser certaines choses selon nos besoins.

### Rappel de la configuration

Nous avons une base correcte, un début de configuration qui se contente de l'extrême minimum pour recevoir et envoyer du mail.

Pour rappel, notre configuration de base est celle-ci, non commentée pour plus de concision:

```sh
### Généralités
set from = "test@unixmail.fr"
set realname = "Mon vrai nom"
set use_from = yes
set signature = ~/.signature
set spoolfile = /var/spool/mail/$USER
set folder = ~/mail
set tmpdir = ~/tmp
set editor = "vim -c 'set tw=72' -c 'set wrap'"

### Configuration POP3
set pop_user = "test@unixmail.fr"
set pop_pass = "votre mot de passe POP"
set pop_delete = yes
set pop_host = "pops://pop.unixmail.fr:995"

### Configuration SMTP
set smtp_pass = "MotDePasseSMTP"
set smtp_url = "smtps://test@unixmail.fr@smtp.unixmail.fr:465"

### Configuration IMAP
set spoolfile="imaps://imap.unixmail.fr/INBOX"
set imap_user = "test@unixmail.fr"
set imap_pass = "MotDePasseIMAP"
set folder="imaps://imap.unixmail.fr/INBOX"
set mail_check = 120
set imap_keepalive = 100
set record = +Sent
set postponed = +Brouillons
set trash = +Trash
set imap_check_subscribed
set hostname = imap.unixmail.fr

### Touch G pour récupérer les mails
bind index "G" imap-fetch-mail
```

Si vous ne comprenez pas ces lignes, je vous invite fortement à relire les articles précédents qui les expliquent une par une.

### Mise en place de la _sidebar_

Actuellement avec notre configuration, nous sommes soit sur l'index, soit sur le pager. Donc soit sur la liste des mails, soit sur la lecture d'un mail. Le souci c'est qu'on a plusieurs dossiers dans lesquels nous rangeons nos mails mais nous n'avons pas de visu sur cette liste de dossiers, ni de moyen pour s'y déplacer rapidement, hormis les raccourcis classiques comme la touche `c` qui permet de changer de boite aux lettres par défaut.

Pour apporter cette modification, nous allons couper notre écran en deux parties. Garder l'index et le pager sur la droite de l'écran, et une petite barre sur la gauche qui nous permettra de visualiser nos dossiers de mails. Pour les déplacements dans ces dossiers, nous allons mettre en place des raccourcis clavier pour nous simplifier la vie. Rassurez-vous, il n'y a rien de compliqué ! Ce n'est que des petites lignes de configuration pour la plupart très explicites !

En premier lieu, on ajoute une ligne qui permet de faire apparaître cette fameuse _sidebar_:

```sh
set sidebar_visible = yes
```

Nous allons de suite essayer de faire en sorte qu'elle soit à la bonne taille. Pour régler la largeur de la _sidebar_ c'est là aussi très explicite:

```sh
set sidebar_width = 20
```

Attention tout de même, il y a une subtilité. La valeur dépend toujours de l'unité, et dans notre cas, cette valeur n'est pas exprimée en pixel, mais en colonne.

_"Des colonnes ? Mais il est fou !"_

Non vraiment, c'est des colonnes ! En fait, une colonne correspond à la largeur d'un caractère ASCII. Ainsi, la valeur 20 dans cette ligne de configuration équivaut à "une largeur de 20 caractères ASCII" (ou 10 si vous utilisez des caractères chinois).

Petite astuce en passant, pour connaître la largeur de votre terminal en nombre de colonnes, c'est la commande `tput cols` !

Jouez donc avec la valeur pour obtenir une largeur de colonne suffisante, quitte à modifier cette valeur dans l'avenir.

C'est bien mignon de jouer avec tout ça, mais pour l'instant, la _sidebar_ est complètement vide. Remédions à ça !

#### Remplir la _sidebar_

Simple comme une ligne de configuration, c'est comme ça que ça se passe:

```sh
mailboxes vous perso mailing-list factures
```

Il suffit d'indiquer vos dossier de mails à la suite du mot `mailboxes` pour qu'ils soient pris en compte dans la _sidebar_.

Par exemple pour avoir dans ma _sidebar_ ma boite de réception, mes dossiers de mails concernant ma banque, les factures et ce qui ne regarde que moi:

```sh
mailboxes zilkos banque factures perso
```

> Dans le cadre d'une configuration IMAP, la ligne `mailboxes` gère aussi la synchronisation des dossiers avec le serveur si la directive `set imap_check_subscribed` est présente.

#### Une histoire de dossier... ou de fichier

Je rappelle que vous pouvez créer vos dossiers en sauvegardant un mail. Par défaut Mutt vous demandera où sauvegarder le mail. Deux solutions s'offrent à vous pour créer un dossier (ou fichier, ça dépend du format de stockage de mail mbox ou maildir) et quand Mutt vous indique `Sauver vers une BAL:`:

 - Indiquez "banque" et il créera le dossier dans `/home/vous/banque`
 - Indiquez "=banque" et il le créera à la suite de la valeur de `set folder`

 La différence est uniquement dans ce que vous allez lui indiquer. Dans le deuxième cas il vous affichera ceci:

 `Créer /home/vous/mail/banque ? ([oui]/non): `

 Ce `=` fonctionne un peu comme un `~` dans le cadre de la gestion des fichiers sous GNU/Linux, il fait référence au dossier utilisateur en quelques sorte et il est donc fixé dans la configuration par la ligne `set folder`. En débutant sur Mutt je n'étais pas au courant et je me suis arraché quelques cheveux sur cette subtilité pour placer mes dossier où je le souhaitais.

 Pour preuve, j'ai sauvegardé 3 mails dans 3 dossiers différents et Mutt a créé ce qu'il fallait sous `~/mail` qui est la valeur de la directive `set folder`. Voyez plutôt l'arborescence de mon dossier avec la commande `tree`:

 ```sh
zilkos@tutomutt:~$ tree mail/
mail/
├── banque
│   ├── cur
│   │   └── 1520446383.R11453778731646881153.tutomutt:2,S
│   ├── new
│   └── tmp
├── factures
│   ├── cur
│   │   └── 1520446486.R11028679740265174737.tutomutt:2,S
│   ├── new
│   └── tmp
└── perso
    ├── cur
    │   └── 1520446493.R1701576256308225629.tutomutt:2,S
    ├── new
    └── tmp
12 directories, 3 files
```

Pourquoi autant de dossiers ? C'est tout simplement le comportement normal du type `Maildir`. Le type de boite est fixé par la ligne suivante:

```sh
set mbox_type=Maildir
```

Indiquer `mbox` au lieu de `Maildir` et votre arborescence deviendra celle-ci:

```sh
zilkos@tutomutt:~$ tree mail/
mail/
├── banque
├── factures
└── perso

0 directories, 3 files
```

Le format mbox stocke tous les mails les uns à la suite des autres. Quand vous demandez à Mutt de "sauvegarder dans le dossier banque" il va juste écrire le mail en question à la suite des précédents dans ce fichier.

Personnellement j'aime bien mbox car c'est facilement scriptable pour sortir des statistiques ou traiter des mails à la chaîne, il suffit de _parser_ un fichier !

Allez, nous avons assez de précisions sur comment organiser nos dossiers, il serait quand même bien appréciable de pouvoir s'y déplacer !


#### Se déplacer

La _sidebar_ est pleine, mettons maintenant en place des raccourcis clavier pour s'y déplacer comme bon nous semble.

La _sidebar_ vient avec des "fonctions", et dans Mutt on peut appeler des fonction en les liant à une combinaison de touches. Cela se fait de la manière suivante:

`bind où raccourci fonction`

En gros, je `bind` (lie) mon `raccourci` à une `fonction` qui sera effective `où`.

Ainsi, nous avons 3 raccourcis, les miens sont les suivants:

```sh
bind index,pager \CP sidebar-prev
bind index,pager \CN sidebar-next
bind index,pager \CO sidebar-open
```

Donc pour faire simple, quand je suis dans le pager ou l'index, la combinaison de touches `Ctrl + p` me fera remonter la liste des boites de la _sidebar_, `Ctrl + n` fera descendre la liste et `Ctrl + o` ouvrira la boite actuellement mise en surbrillance. Tout ceci grâce aux trois fonctions de la _sidebar_: `sidebar-prev`, `sidebar-next` et `sidebar-open`.

Si je souhaite que l'ouverture d'une boite soit possible uniquement dans l'index, via `Ctrl + *`, ça sera cette ligne:

```sh
bind index \C* sidebar-open
```

`\C` est bien évidemment pour la touche contrôle. Pour la touche `Alt`, ça sera `\A`. De manière globale, Mutt pourra vous aider à trouver la syntaxe des différentes touches. Pour se faire lancer Mutt et tapez ceci:

```sh
:exec what-key
```

Pour chaque combinaison de touche que vous allez effectuer, Mutt vous donnera des informations. Un exemple avec `Alt + flèche du haut`:

```
Caractère = <A-Up>, Octal = 1064, Decimal = 564
```

Pour sortir de `what-key`, pressez `Ctrl + g`.


#### Plusieurs identités...

Quotidiennement, j'utilise deux adresses email, mais l'une est un alias de l'autre. Ça me permet d'avoir deux identités dans une seul compte mail, ce qui est plutôt pratique pour tout lire au même endroit mais séparer un peu les propos selon leur portée. Nous allons configurer Mutt pour lui indiquer quelles adresses nous utilisons et paramétrer quelques automatismes basés sur ces deux adresses.

Commençons par fixer la directive `alternates`, qui peut se faire de plusieurs manières.

Vous avez qu'une identité supplémentaire, et c'est super simple:

```sh
alternates = "deux@unixmail.fr"
```

Si vous avez plusieurs identités, en plus de la principale, vous pouvez opter de la sorte:

```sh
alternates ^trois@unixmail.fr$ ^quatre@unixmail.fr$ ^cinq@unixmail.fr$
```

Pourquoi une forme d'expression régulière ? Parce que pour Mutt, une _alternate_ du style `numerotrois@unixmail.fr` est valide pour l'adresse `trois@unixmail.fr` !

Pas vraiment compliqué ! Sauf que vous si vous avez tout suivi, vous devriez vous rendre compte qu'un problème se pose...

Si, si, cherchez bien.

Bon d'accord je vous aide, regardez donc en haut de votre configuration Mutt:

```sh
set from = "test@unixmail.fr"
set realname = "Mon vrai nom"
set use_from = yes
```

_"Bah dans le coup c'est le foutoir !"_

#### ... donc plusieurs adresses

Certes, c'est un peu le bazar.

Partons du principe que nous avons notre adresse principale en `un@unixmail.fr`, et l'adresse `deux@unixmail.fr` en `alternate`. Respectivement nous voulons les noms `Numéro Un` et `Numéro Deux`.

Nous voulons que l'adresse s'adapte automatiquement au **contexte**. Donc si je reçois sur l'adresse `deux`, je veux répondre avec l'adresse `deux`, même si dans ma configuration j'ai `un` en identité principale (donc avec les `from` et autre réglages). Bien évidemment, je veux également changer le `realname`. Personnellement j'ai une adresse à pseudo (Zilkos) et une adresse en nom réel. Si mon adresse à pseudo affiche mon nom réel (donc `un@unixmail.fr` avec le nom `Numéro Deux`) mon correspondant ne va comprendre grand chose et je vais perdre l’étanchéité offerte par mes multiples identités.

Le but étant donc de raccorder le bon nom avec la bonne adresse.

On va faire ça en deux étapes.

Premièrement, on va indiquer une ligne de configuration qui va grandement nous simplifier la vie pour la suite:

```sh
set reverse_name = yes
```

Elle fait quoi cette ligne ? Simple, si on m'écrit à destination d'une des adresses présentes dans `alternates`, le fait d'avoir positionné `reverse_name` à `yes` va faire que Mutt va **automatiquement** utiliser cette adresse au lieu d'utiliser celle du `from` par défaut.

Le besoin était de changer l'adresse automatiquement, c'est donc gagné pour celui-ci !

Le deuxième besoin est de changer le nom affiché, on va profiter du comportement de `reverse_name` pour faire un _hook_ par rapport à l'adresse avec laquelle on écrit. Donc on imbrique un automatisme à la suite d'une autre, ça peut être risqué en cas de mauvaise configuration, mais le premier automatisme est simple vu que ce n'est pas un _hook_ mais uniquement un comportement prévu par le programme.

Comme nous sommes certains que l'adresse est la bonne grâce à `reverse_name` qui la modifie quand nécessaire, on ne s'en préoccupe pas. On va juste "regarder" quelle adresse c'est et positionner ce que l'on veut comme configuration pour cette adresse.

Un _hook_ est de la forme suivante:

```sh
send-hook "~f deux@unixmail.fr" 'set realname="Numéro Deux"'
```

Découpons le:

  - `send-hook` est le nom de la commande qui permet de définir un _hook_ (un contournement, littéralement "un crochet") sur les **réponses** et les **nouveaux** messages. Nous verrons qu'il y a plusieurs commandes pour les _hooks_, selon les besoins.

  - `"~f deux@unixmail.fr` est condition que la commande va évaluer.

  - `'set realname="Numéro Deux"'` est l'instruction exécutée si la condition est vraie.

Petite subtilité, `~f` est ce que l'on appelle un **motif**. Une sorte de raccourci indiquant des choses. On y reviendra.

Donc ce hook, plutôt simple, va effectuer ceci:

  1. Dans le cadre d'un nouveau message ou d'une réponse à un message
  2. Si l'adresse utilisée est `deux@unixmail.fr`
  3. Positionne mon nom réel à `Numéro deux`

Efficace, rapide, simple.

Sauf que, si vous rédigez un nouveau message avec votre identité par défaut, vous aurez l'adresse `un@unixmail.fr` et le `realname` à `Numéro Deux` étant donné que le _hook_ a réécrit le paramétrage `set realname`. Pas pratique !

Comme le `send-hook` s'applique sur les réponses **et** les nouveaux messages, il suffit d'en construire un autre pour la première adresse. Nous avons donc au total, deux _hooks_ gérant nos noms selon nos adresses, le tout de manière automatique:

```sh
send-hook "~f un@unixmail.fr" 'set realname="Numéro Un"'
send-hook "~f deux@unixmail.fr" 'set realname="Numéro Deux"'
```

Ainsi, l'adresse sera toujours la bonne et le nom sera également toujours le bon, car le hook est appelé à chaque nouveau message ou nouvelle réponse !

##### Les motifs

Ce petit `~f` veut dire _Message provenant (dans le sens _from_) de `$USER`_. `$USER` est une expression rationnelle, donc dans votre hook, si vous indiquez `"~f un@*"` le hook fonctionnera pour toutes les adresses commençant par `un@`.

Je vais pas tous les passer en revue car ce serait trop long, mais je vous mets les principaux:

| Motif | Traduction |
|-------|------------|
| `~A` | Tous les messages |
|`~b EXPR` | Messages contenant EXPR dans leur corps |
|`~B EXPR` | Messages contenant EXPR dans l'intégralité du message |
|`~c USER` | Messages avec une copie carbone (cc:) pour $USER |
|`~g`| Messages signés (PGP) |
|`~G`| Messages chiffrés (PGP) |
|`~h EXPR`| Messages contenant EXPR dans leurs en-têtes |
|`~l` | Message adressé à une liste de diffusion connue |

Il y en a plus que ça, voyez donc [la doc](http://www.mutt.org/doc/manual/#patterns-modifier) pour la liste complète !

### Organisons les champs (headers)

À l'affichage d'un mail, il y a des champs (des _headers_) par défaut qui sont affichés en haut. Nous allons organiser les différents _headers_ que l'on veut voir (si ils sont présents). Ces _headers_ sont les lignes "De:", "A:" ou "Sujet:" par exemple.

Pour se faire, il suffit d'indiquer ceux que nous voulons, mais pour pas que la configuration par défaut ne vienne pas mettre le bazar dans nos headers, on va d'abord tous les effacer, les ignorer avec la ligne de configuration suivante:

```sh
ignore *
```

Si nous ouvrons un mail avec juste cette ligne de configuration, tous les _headers_ sont absents, nous avons juste le corps du mail.

À la suite de cette ligne, il suffit tout simplement d'indiquer quels sont les _headers_ nous souhaitons afficher:

```sh
unignore From:
unignore To:
unignore Subject:
unignore Date:
unignore Organization:
unignore Newsgroups:
unignore CC:
unignore BCC:
unignore X-Mailer:
unignore User-Agent:
unignore Message-ID:
unignore X-Junked-Because:
```

A partir de maintenant, et si il sont renseignés, tous ces _headers_ s'afficheront. Par contre nous ne les voulons pas dans n'importe quel ordre, donc nous allons spécifier l'ordre d'affichage des headers avec la ligne suivante:

```sh
hdr_order From: Subject: To: CC: BCC: Date: Organization: User-Agent: X-Mailer: Message-ID:
```

Vous n'êtes pas obligé de tous les mentionner bien évidemment.

### Organisons l'index

Notre index présentant la liste des messages n'est vraiment pas très clair. Ni en terme de date, ni en terme d'espace entre les éléments présentés.

La configuration que j'utilise pour mon index est la suivante, pour avoir un peu de simplicité:

```sh
set charset = "utf-8"
set locale  = "fr_FR.UTF-8"
set index_format="%4C %Z %D %-25.25L %s"
set date_format="%d/%m/%y - %H:%M"
```

Les deux premières lignes fixent le _charset_ utf-8, courant pour pas dire universel sous Linux. Les deux autres lignes sont nettement moins explicites, expliquons-les:

```sh
set index_format="%4C %Z %D %-25.25L %s"
```

Celle-ci pique un peu les yeux, mais vous allez voir que ce n'est pas bien compliqué.

Commençons par `%4C`. En premier lieu le `C` signifie _le numéro du message courant_ dans l'index. Le `4` signifie que `C` réserve l'affiche du numéro sur 4 caractères. C'est un peu gros certes, mais au moins il y a suffisamment de place, vu qu'au maximum ça fait 9999 messages dans l'index, chose peu probable. Notez que ce n'est une question d'affichage, votre Mutt continuera de fonctionner après le 1000ème message.

Ensuite un `%Z` qui représente le statut du message dans votre index, le fameux _flag_. Un petit tableau qui vous les présente tous, avec la signification associée:

| Indicateur | Signification |
|------------|---------------|
| N | Nouveau message |
| O | Vieux message |
| r | Message auquel vous avez répondu |
| D | Message marqué pour suppression |
| d | Message avec pièce jointe marqué pour suppression |
| T | Message où vous êtes indiqué dans le champs À avec d'autres destinataires |
| F | Message venant de vous |
| C | Message où vous êtes en copie |
| + | Message où vous êtes l'unique destinataire |
| L | Message issu d'une mailing-list à laquelle vous êtes inscrit |
| * | Message marqué (tagged) |
| s | Message contenant une signature PGP |

Donc vous pouvez aisément deviner notre `%D` qui suit, c'est un indicateur de suppression.

A la suite nous avons une construction particulière: `%-25.25L`

`L` indique que dans l'index, `To <list>` sera indiqué si une liste de diffusion est présente dans le champ `To:` ou `Cc <list>` sera indiqué si une liste de diffusion est présente dans le champ `Cc:`, autrement, c'est le nom de l'expéditeur qui est retourné. Le nom de l'expéditeur correspond à `%F`. Donc en gros, il écrit `%L` mais si ça ne _match_ pas avec une liste, il retombe sur `%F`. Ça permet donc de gérer deux comportements en une seule commande.

`-25.25` indique qu'il va écrire la valeur de `%L` sur 25 caractères et que si ce qu'il écrit est inférieur à 25 caractères, il va rajouter des blancs à la fin jusqu'à atteindre 25 caractères. Typiquement, si ce qu'écrit `%L` fait 20 caractères, il va rajouter 5 caractères blancs en fin de ligne. Ceci permet d'avoir un alignement sympathique du sujet dans la liste des messages:

```
uneadresse@email.invalid Sujet
utilisateur Toto         Sujet
lasuperliste@example.fr  Sujet
```

Donc peu importe la taille de l'expéditeur, le sujet sera toujours aligné. Libre à vous de changer la valeur 25 si c'est un peu court selon les noms ou adresses de vos correspondant. Si ça dépasse, ça sera tronqué.

La dernière instruction est `%s` qui affiche simplement le sujet du mail.

La ligne `set date_format="%d/%m/%y - %H:%M"` permet tout simplement de formater la date pour avoir une date du type `06/03/18 - 15:12`.

Par défaut quand vous vous baladez dans la liste de vos mails, Mutt met en surbrillance la ligne du mail sur lequel vous êtes. Si vous préférez avoir un curseur en début de ligne, indiquez la directive suivante:

```sh
set arrow_cursor
```

La ligne en cours ne sera plus en surbrillance et une jolie flèche sera présente au début de la ligne en cours.

### Configurations en vrac

Quelques directives sympathiques qui peuvent vous aider, inutile de les expliquer longuement tellement elles sont explicites:

```sh
# Ne pas passer au message suivant lorsqu'on atteint la fin
# de la lecture du mail
set pager_stop = yes

# Lecture confortable
set smart_wrap = yes

# Nombre de ligne de contexte à conserver lors d'un saut de page
set pager_context = 8

# Symbole de citation
set quote_regexp = "^([ \t]*[|>/])+"
set markers = yes

# Inclus le message dans la réponse
set include

# Préfixe le message récupéré via set include dans la réponse
# %d pour la date
# %n pour l'expéditeur
set attribution = "Le %d, %n a écrit :"

# Pour éditer les headers dans la réponse
set edit_headers

# En répondant (touche r), récupère automatiquement
# le destinataire et le sujet.
# Fait apparaître directement l'éditeur de message.
set fast_reply
```

### HTML et mailcap

On termine cet article par la gestion des mails en HTML. Personnellement je ne suis pas pour, mais hélas les mails reçus sont très souvent en HTML. De base Mutt gère à peu près l'HTML.

On l'aider un peu à y voir plus clair. Le but sera de l'aider avec un navigateur en mode texte qui va "lire" le contenu, le formater et l'envoyer à Mutt.

Personnellement je trouve que `w3m` fait parfaitement le boulot. Installez-le comme n'importe quel autre logiciel, il est forcément présent dans les dépôts de votre distribution.

En premier lieu, on crée un fichier `.mailcap` dans notre dossier utilisateur:

```sh
$ cd ~ && touch .mailcap
```

Dans ce fichier on va y mettre la ligne qui permettra d'appeler w3m pour qu'il fasse son boulot et qu'il renvoie la sortie à Mutt. C'est comme ceci:

```sh
text/html; w3m -I %{charset} -T text/html; copiousoutput;
```

L'option `-I` est là pour la gestion du charset et `-T` pour définir le type de contenu, ce sont des arguments uniquement liés au logiciel w3m. `copiousoutput` quant à lui indique à Mutt qui va y avoir un gros tas de contenu texte et qu'il va devoir l'ouvrir dans un pager (donc un écran de lecture de mail, interne à Mutt dans notre cas).

En plus de ce fichier mailcap il nous faut deux lignes de paramètre dans notre `.muttrc`:

```sh
auto_view text/html
alternative_order text/plain text/html
```

La première ligne permet d'appeler directement le fichier mailcap, donc Mutt ne tentera pas d'afficher le contenu par lui même. Dans le cas présent on lui indique de déléguer à w3m uniquement pour le contenu de type text/html. Si cette ligne n'était pas fixé, le moteur Mutt tenterait de décoder le message en HTML tout seul et la sortie ne serait pas vraiment claire... Pour invoquer l'entrée du mailcap correspondant au type de contenu, il faut aller dans les pièces jointes du mail (touche `v`), se mettre sur la ligne ayant le bon type de contenu et appuyer sur la touche `m` pour invoquer l'entrée du mailcap correspondant au type de contenu.

Donc sans la ligne `auto_view text/html`, c'est beaucoup d'actions en plus pour pas grand chose.

Certains clients mails peuvent envoyer plusieurs versions de leur contenu, une en HTML et une en texte brut, cela dépend des clients. La deuxième ligne de configuration permet de dire "Si il y a du contenu text/plain, donc en texte brut, merci de me l'afficher en priorité, plutôt que le HTML moisi".

#### Fonctionnement d'un fichier mailcap

Quelques explications sur comment fonctionne un fichier mailcap. Pour faire simple, c'est juste une table de correspondance. A gauche le type de contenu, à droite la manière de la consulter.

Ainsi pour du simple texte, je peux utiliser la commande more pour le voir:

```sh
text/plain; more
```

Attention more écrit sur la sortie standard, donc en arrière-plan de Mutt !

Je peux également passer par vim:

```sh
text/plain; vim %s
```

`%s` fait en sorte de l'enregistrer dans un fichier.

Par exemple quand je reçois un mail en texte brut avec cette entrée dans le mailcap, je l'ouvre comme tous les autres mails et si j’appuie sur la touche `v` pour consulter les pièces jointes je vois une entrée en text/plain. J'ai deux solution:

  - J'appuie sur Entrée et Mutt me l'affiche dans un pager
  - J'appuie sur `m` pour le lire avec la méthode mailcap et Mutt met le fichier dans la cible de `set tmpdir` et me l'ouvre avec vim

Si toutefois je veux automatiquement que le mailcap soit invoqué sur ce type de contenu, je surcharge la ligne de configuration déclarée plus haut comme ceci:

```sh
auto_view text/html text/plain
```

Et ainsi Mutt appellera directement le mailcap.

Bien évidemment, nous pouvons passer un script en deuxième partie de mailcap pour gérer un contenu d'un type particulier:

```sh
image/jpeg; ~/.mutt/script/monsuperscript % jpg
```

ou utiliser des applications graphiques bien entendu, ci-après un exemple pour ouvrir les PDF avec evince:

```sh
application/pdf; evince %s
```

Facile et grandement personnalisable !


### Fin

C'est tout pour cet épisode 3 ! Il est certes plus volumineux que les autres mais il était grandement nécessaire pour avoir un peu plus de souplesse sous Mutt et surtout pour vous donner les premières armes qui vous permettrons de mettre en place quelques automatismes sympathiques. C'est ça qui est bien avec Mutt, c'est que de base il fonctionne et il vous permet d'aller beaucoup plus loin en le modelant à votre guise pour qu'il comble le moindre de vos besoins !

Dans l'épisode 4, on abordera la gestion du chiffrement des mails et on se penchera sur la mise en place d'un carnet d'adresse ainsi que sa gestion, de manière simple et rapide.

Cet article est long et probablement rempli de coquilles. Si vous trouvez des choses bizarres, des points mal expliqués, des manques d'exemples ou tout autre défaut, n'hésitez pas à faire un tour sur les [mailing-list](/communaute) ou tout simplement à proposer vos corrections sur le [dépôt](https://gitlab.com/Zilkos/blog) !
