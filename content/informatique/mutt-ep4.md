---
authors: ["zilkos"]
title: 'Mutt: Chiffrons !'
date: '2018-03-27'
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Mutt", "CLI", "Mail"]
toc: true
---

> Les autres épisodes sont [ici](tags/mutt) !

Bienvenue dans ce quatrième épisode de la série d'article consacrée au célèbre client mail en console: **Mutt** !

Dans l'épisode précédent nous avons rendu notre configuration nettement plus flexible qu'au début. Maintenant que nous avons un peu de confort et de simplicité d'utilisation, nous allons nous pencher sur le chiffrement des nos mails sous Mutt avec GPG !

### Je n'ai pas de clef, je ne sais pas faire ou c'est compliqué

Seule la première excuse est valable ! Pour le reste, je vous renvoie vers le blog de l'ami Maxime Auvy qui a justement fait un article pour ce cas de figure.

Au menu, des explications sur ce qu'est GPG, comment l'installer, générer des clefs, les gérer, bref tout ce qu'il vous faut pour débuter dans le monde du chiffrement de mail, en douceur avec des explications claires.

> [[Tuto] Signer/chiffrer ses mails avec GPG](https://open-freax.fr/tuto-mails-gpg/)

Merci à lui en passant !

Cet article se concentre uniquement sur comment faire causer GPG et Mutt. Et rassurez-vous, on a déjà pas mal de boulot mais au final ça cause plutôt bien ! Comme toutes choses que l'on souhaite mener à bien, commençons donc par un peu d'organisation.

### Organisation des fichiers

Oui, on ne mélange pas les torchons et les serviettes (enfin, personnellement je le fais, mais ce n'est pas le sujet).

La configuration de GPG pour Mutt est relativement verbeuse, en plus d'être sensible. Donc plutôt que de venir polluer votre `.muttrc`, on va créer un fichier de configuration dédié aux paramétrages de chiffrement de mail et on demandera à Mutt de venir lire ce fichier au démarrage.

En terme d'organisation vous êtes plutôt libre. De manière personnelle je suis organisé comme ceci:

  * Un `$HOME/.muttrc` pour la configuration générale de mutt
  * Un `$HOME/.mutt/` avec mes configurations additionnelles

Ainsi, mes configurations de chiffrement, mes thèmes de couleurs pour mutt, mes scripts divers et variés sont dans le dossier `$HOME/.mutt/`. Et depuis mon `.muttrc` j'appelle mes configurations présentes dans `$HOME/.mutt/`.

Pour "sourcer" un fichier de configuration externe au `.muttrc`, il suffit de l’appeler avec cette ligne de configuration:

``` sh
source chemin/vers/le/fichier
```

Oui, comme la commande sous GNU/Linux. Elle permet d'inclure à l'endroit où la commande est appelée le fichier qu'elle désigne.

Pour rappel, Mutt lit votre fichier de configuration ligne par ligne de haut en bas. Quand il croise une directive `source` il s'arrête, va lire le fichier de configuration cible et reprend là où il s'était arrêté. Faites donc attention à l'ordre de vos appels `source`.

Nous allons donc créer un fichier `$HOME/.gpgrc` puis indiquer dans notre `.muttrc` la directive suivante;

```sh
source $HOME/.gpgrc
```

Libre à vous de vous organiser différemment bien entendu, sachez tout de même que c'est plutôt sain de "découper" sa configuration Mutt en plusieurs fichiers. C'est plus maintenable, plus clair, modulaire. Juste, pensez à backup aussi ces fichiers de configuration dérivés, si le `.muttrc` appelle des fichiers qui ne sont pas là, il y a erreur au lancement.

> Notez que dans cet article, quand on dit "signer" on parle bien de "signature cryptographique", et non pas de votre nom en bas d'un mail.

### A l'attaque de GPG

Comme dit plus haut, la configuration GPG dans Mutt n'est pas très digeste... Certaines directives sont longues, tout simplement parce qu'elle ne dépendent pas de Mutt, ou du moins certains aspects du chiffrement ne sont pas traités par Mutt.

Globalement, on va lier des commandes GPG à Mutt. C'est à dire que pour vérifier des signatures, signer, chiffrer, c'est pas vraiment Mutt qui va le faire, mais `gpg2`. Le but de ce fichier de configuration sera donc de lier les actions de Mutt avec les bonnes commandes GPG, en plus de fixer quelques directives de configuration puremment "Muttesque" si vous me permettez l'expression. D'ailleurs on va commencer par là, car cette partie est plutôt simple.

#### Quelques configurations en vrac, la partie facile

On va commencer par présenter les lignes de configurations `crypt_`, qui sont plutôt simple, l'extrême majorité d'entre elles sont uniquement des lignes comportementales:

```sh
set crypt_use_gpgme = yes
```

En gros: _Est-ce que je dois utiliser GPGME ?_ et on lui indique que oui, parce que GPGME c'est chouette.

GPGME, ou GPG Made Easy dans son nom complet est une bibliothèque (un emsemble de fonction déjà prête, pour les néophytes) qui permet **facilement** d'exploiter GPG depuis du code. Beaucoup de logiciels se basent sur cette bibliothèque, et Mutt vient avec une option de compilation activée par défaut, qui permet justement de le supporter.

Pour les amis Gentooistes, [voyez la page du paquet Mutt](https://packages.gentoo.org/packages/mail-client/mutt) qui contient le _useflag_ gpgme. Activer ce _useflag_ de manière globale sur votre système, et toutes les applications proposant une option de compilation gpgme seront compilées avec.

Poursuivons avec trois directives qui se complètent vraiment bien :

```sh
set crypt_replysign = yes
set crypt_replyencrypt = yes
set crypt_replysignencrypted = yes
```

Pour faire court, parce que ce n'est pas extrêmement intéressant non plus:

  * `crypt_replysign` permet de signer vos réponses à des messages signés
  * `crypt_replyencrypt` permet de chiffrer vos réponses à des messages chiffrés
  * `crypt_replysignencrypted` permet de signer vos réponses à des messages chiffrés

Sauf que, Mutt n'est pas capable de savoir si un message chiffré est également signé, donc `crypt_replysign` ne fonctionnera pas pour les messages signés **et** chiffrés. Sauf qu'avec `crypt_replyencrypt` les réponses aux messages chiffrés sont chiffrés et qu'avec `crypt_replysignencrypted` elles sont signées, donc le problème se règle de lui-même.


Pour continuer, une ligne tout simple:

```sh
set crypt_autosign = yes
```

Ligne plutôt explicite, elle permet de signer automatiquement vos messages envoyés. Rassurez-vous, avant d'envoyer votre mail vous aurez le choix de désactiver ou non cette option, dans le même écran qui permet d'ajouter des pièces-jointes, de modifier les champs Cc:, Cci, etc. On y reviendra.

La ligne suivante est également très simple:

```sh
set crypt_verify_sig = yes
```

Par défaut elle est positionnée à `yes`, donc vous n'êtes pas obligé de l'indiquer dans votre configuration, elle permet de vérifier automatiquement la signature d'un message signé. J'en parle, même si elle est par défaut, car j'étais surpris que Mutt vérifie les signatures alors que je ne lui avait pas demandé.

Fini pour la partie simple, passons maintenant au plus gros de la configuration, celle qui permet de signer et ou chiffrer en faisant des liens entre Mutt et les commandes `gpg`.

#### La partie moins facile

On commence par une ligne facile mais nécessaire :

```sh
set pgp_sign_as=ID
```

On indique ici l'ID de votre clef privée. Votre `ID` doit être de la forme suivante `0xABCDEFGH`. Donc les deux derniers blocs des 10 affichés lors de la commande `gpg --fingerprint`.

Rien de bien compliqué.

La seconde picote un peu plus:

```sh
set pgp_decode_command="gpg2 %?p?--passphrase- 0? --no-verbose --batch --output - %f"
```

Concrètement `pgp_decode_command` spécifie la commande à utiliser pour déchiffrer non pas des mails, mais bien des pièces-jointes du type `application/pgp`. Car après tout, c'est ce qu'on manipule dans Mutt.

En plus de la description de la commande, il y a des instructions relatives au format des commandes PGP qui sont au format `printf`, dont Mutt est si friand, à savoir:

`%p` qui est remplacé par `PGPPASSFD=0` quand une phrase de passe est nécessaire, ou par une chaine vide.

`%f` qui est tout simplement le nom du fichier qui contient le message, donc notre contenu chiffré.

`%s` qui est remplacé par le nom d'un fichier qui contient la partie signature d'une pièce-jointe de type `multipart/signed` au moment de sa vérification.

`%a` qui est valeur de `pgp_sign_as` déclarée au-dessus.

`%r` une (ou plusieurs) ID de clef.

Donc en gros, ça déchiffre en vous demandant votre passphrase si nécessaire. On enchaîne avec une définition de commande similaire:

```sh
set pgp_verify_command="gpg2 --no-verbose --batch --output - --verify %s %f"
```

Elle se charge de vérifier les signatures.

Sur celle-ci on retrouve des choses communes à la commande d'avant comme `%s` et `%f`. Le reste n'étant que des arguments de la commande `gpg2`, je les explique maintenant, mais ils ont le même effet sur la première commande. C'est juste que ce n'est pas forcément "digeste" donc j'étale les différents points d'explications:

`--no-verbose` c'est l'inverse de `--verbose`, donc "non bavard", ça évite d'avoir plein de retours à gérer, vu que le _verbose level_ est fixé à 0 avec cet argument.

`--batch` plutôt explicite, c'est un mode batch qui par défaut empêche:

  * Que le programme vous pose des questions
  * Qu'il ne soit pas interactif

Un équivalent informatique du "fais ton travail et chut".

`--output` écrit tout simplement la sortie sur le fichier passé en paramètre.

Et enfin `--verify` qui va vérifier le fichier de signature passé en paramètre, sans écrire sur la sortie.

Nous avons donc des arguments corrects pour gérer les différentes options des commandes `gpg2` sans avoir de retour, de sortie ou de chose verbeuse qui viendrait mettre le bazar dans les affaires de Mutt.

A suivre, la vraie commande principale de notre configuration:

```sh
set pgp_decrypt_command="gpg2 --passphrase- 0 --no-verbose --batch --output - %f"
```

La commande qui permet de déchiffrer un message chiffré PGP/MIME. Sans cette commande, point de lumière sur les mails chiffrés de vos amis dissidents. Je ne m'attarde pas plus dessus vu qu'on y retrouve des choses similaires à celles d'avant.

Ensuite nous avons une définition de commande qui nous permet de créer une signature pour un corps de message séparé au format `multipart/signed` PGP/MIME:

```sh
set pgp_sign_command="gpg2 --no-verbose --batch --output - --passphrase- 0 --armor --detach-sign --textmode %?a?-u %a? %f"
```

Quelques nouveautés, notamment `--armor` qui permet de créer une armure en ASCII. Pour faire court voyez ça comme du "binary to text" qui permet d'obtenir une version texte des données binaires. Tout simplement parce que le mail gère par défaut du texte, et non pas du binaire.

La même commande, mais en format PGP/INLINE pour certains vieux MUA. À noter que c'est obsolète:

```sh
set pgp_clearsign_command="gpg2 --no-verbose --batch --output - --passphrase- 0 --armor --textmode --clearsign %?a?-u %a? %f"
```

A la suite de ça, une définition de commande importante:

```sh
set pgp_encrypt_only_command="/usr/lib/neomutt/pgpewrap gpg2 --batch --quiet --no-verbose --output - --encrypt --textmode --armor --always-trust --encrypt-to VOTRE_ID -- -r %r -- %f"
```

Cela permet de chiffrer le corps d'un mail (la partie _body_) sans le signer. Attention toutefois, cette commande varie légèrement selon votre version de Mutt.

J'utilise une distribution rolling release et par conséquence je dispose de versions plutôt fraîches de mes logiciels préférés. Je dispose donc de Neomutt, qui est une réécriture au goût du jour de Mutt, j'y reviendrai plus tard. La différence principale entre les utilisateurs de Mutt est l'emplacement de `pgpewrap`, tout simplement parce que nous n'avons ni la même version, ni le même système.

Il y a fort à parier que chez les gens sous Debian ou dérivés, elle soit dans `/usr/lib/mutt/pgpewrap`. Dans tous les cas vous avez des moyens simples de trouver son emplacement :

  * Via `whereis pgpewrap`
  * Via `locate pgpewrap`
  * Via `(sudo) find / -name "pgpewrap"`

À noter qu'il n'est pas nécessaire d'installer ce logiciel, vu qu'il vient [avec Mutt](https://gitlab.com/muttmua/mutt/blob/master/pgpewrap.c).

Pensez donc à adapter le chemin vers `pgpewrap` ainsi que le votre **ID de clef** (la même chose que pour `pgp_sign_as`).

La prochaine définition est encore plus longue que celle d'avant, tout simplement parce qu'elle permet de chiffrer **et** de signer:

```sh
set pgp_encrypt_sign_command="/usr/lib/neomutt/pgpewrap gpg2 --passphrase- 0 --batch --quiet --no-verbose --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust --encrypt-to VOTRE_ID -- -r %r -- %f"
```

Idem, adaptez le chemin de `pgpewrap` et renseignez correctement votre ID de clef.

On enchaîne sur des lignes plus légères:

```sh
set pgp_import_command="gpg2 --no-verbose --import %f"
```

qui permet d'importer une clef publique dans votre trousseau.

La même chose mais dans l'autre sens, on peut exporter une clef:

```sh
set pgp_export_command="gpg2 --no-verbose --export --armor %r"
```

Notez la présence de `--armor` car là encore on manipule du texte, et non pas des données binaires.

On peut également vérifier des clefs (c'est un peu le but de la chose quand même):

```sh
set pgp_verify_key_command="gpg2 --no-verbose --batch --fingerprint --check-sigs %r"
```

On peut lister les clefs publiques de notre trousseau:

```sh
set pgp_list_pubring_command="gpg2 --no-verbose --batch --with-colons --list-keys %r"
```

Cette commande servira par exemple à choisir la clef avec laquelle chiffrer un message.

La même chose mais avec les clefs privées:

```sh
set pgp_list_secring_command="gpg2 --no-verbose --batch --with-colons --list-secret-keys %r"
```

Bref, rien de bien folichon hein, puisque les commandes console font pour ainsi dire la même chose, avec des arguments en moins pour éviter le côté verbeux de la chose:

```sh
$ gpg --list-keys
$ gpg --list-secret-keys
```

Pour terminer, quelques lignes de configurations dédiée au chiffrement PGP, en vrac:

```sh
set pgp_autosign=yes
set pgp_replyencrypt=yes
set pgp_replysign=yes
set pgp_timeout=300
set pgp_good_sign="^gpg: Signature correcte de "
```

La première ligne `pgp_autosign` impose à Mutt le fait d'essayer de toujours signer les messages sortant en PGP/MIME.

Les lignes `pgp_replyencrypt` et `pgp_replysign` sont plutôt explicites, on chiffre par défaut la réponse à un message chiffré, et on signe par défaut la réponse à un message signé.

`pgp_timeout` indique la durée en secondes au bout de laquelle la phrase de passe expire si elle n'est pas utilisée. Réglez comme il se doit cette ligne, surtout si vous n'êtes pas toujours devant votre PC et que vous laissez votre session ouverte (j'espère que vous ne faites pas ça hein !).

La dernière ligne `pgp_good_sign` est à mon sens la plus mal foutue. Souvenez-vous de `pgp_verify_command` qui permet de vérifier une signature. `pgp_verify_command` affiche une sortie. Et bien si le contenu de `pgp_good_sign` se trouve dans la sortie de `pgp_verify_command` alors la signature sera considérée comme correcte...

Regardez la nullité de cette phrase:

> Si la sortie de `pgp_verify_command` contient `pgp_good_sign` alors la signature est considérée comme valide.

C'est extrêmement mal foutue, parce que c'est dépendant de votre `locale`. Si votre système est en anglais, bah ça ne fonctionne pas si votre ligne de configuration est en français. Hérésie complète. Il faut donc adapter cette ligne à votre environnement. En sachant que cette ligne accepte les expressions régulières.

Donc pour nos amis anglophones, ou les francophones qui aiment bien avoir un système en anglais, ça sera cette ligne:

```sh
set pgp_good_sign="^gpgv?: Good signature from"
```

ou encore (à base de gettext)

```sh
set pgp_good_sign="`gettext -d gnupg -s 'Good signature from "' | tr -d '"'`"
```

Ça fonctionne (en vrai).

Si vous êtes encore vivant, bravo, essuyez donc la larme de sang sur le bord de votre oeil car nous sommes enfin au bout. Rassurez-vous, c'est aussi difficle pour vous à lire que pour moi à essayer d'expliquer grossièrement la bestiole.

#### Un bout de configuration de GPG

Il y a une subtilité dans tout ça. Pas grand chose parce que c'est logique, mais quand même, il faut s'y pencher.

Si on observe attentivement notre configuration, on se rend compte de quelque chose. Il y a un aspect "automatique" d'une fonction qui n'est pas liée à un raccourcis clavier. Celle-ci :

```sh
set pgp_import_command="gpg2 --no-verbose --import %f"
```

Cette ligne, comme indiquée plus haut, permet de récupérer **automatiquement** des clefs publiques. En effet, pas besoin de raccourci clavier pour le faire c'est automatique, mais à une seule condition, et cette condition n'est pas liée à Mutt mais... à Gnupg !

En effet, cette "possibilité" de récupérer des clefs est uniquement possible si vous dites à Gnupg de le faire. Et pour ça, il nous faut quelques petites choses:

  * Votre serveur de clef préféré
  * Un fichier de configuration GPG
  * Un éditeur de texte

Pour le serveur de clef, c'est plutôt simple, suffit de piocher dans la liste. L'ami Maxime Auvy en donne 3 en vrac avec les protocoles qui vont bien, j'en rajoute quelques uns:

  * pgp.mit.edu (HKP)
  * pool.sks-keyservers.net (HKP)
  * keyserver.pgp.com (LDAP)
  * wwwkeys.pgp.net (HKP)
  * subkeys.pgp.net (HKP)

Pour le fichier de configuration, c'est direction `$HOME/.gnupg` et dans ce dossier on crée un fichier `gpg.conf` si il n'existe pas. A l'intérieur on va y rajouter deux choses:

  * L'URI de notre serveur de clefs préféré
  * La directive permettant d'interroger notre serveur de clef préféré pour récupérer automatiquement les clefs

Cela se fait ainsi:

```sh
keyserver hkp://pgp.mit.edu
keyserver-options auto-key-retrieve
```

La première ligne déclare le serveur préféré et la deuxième indique de récupérer les clefs automatiquement. Si toutefois votre serveur de clef préféré est interrogeable via le protocole ldap, c'est `ldap://` en lieu et place de `hkp://` bien évidemment.

### TLDR

Pour ceux qui ont la flemme, un simple copier/coller d'une configuration qui fonctionne:

```sh
set crypt_use_gpgme=yes
set crypt_replysign=yes
set crypt_replyencrypt=yes
set crypt_replysignencrypted=yes
set crypt_autosign=yes
set crypt_verify_sig=yes
set pgp_sign_as=0xD9034A4E
set pgp_decode_command="gpg2 %?p?--passphrase- 0? --no-verbose --batch --output - %f"
set pgp_verify_command="gpg2 --no-verbose --batch --output - --verify %s %f"
set pgp_decrypt_command="gpg2 --passphrase- 0 --no-verbose --batch --output - %f"
set pgp_sign_command="gpg2 --no-verbose --batch --output - --passphrase- 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg2 --no-verbose --batch --output - --passphrase- 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg2 --batch --quiet --no-verbose --output - --encrypt --textmode --armor --always-trust --encrypt-to VOTRE_ID -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg2 --passphrase- 0 --batch --quiet --no-verbose --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust --encrypt-to VOTRE_ID -- -r %r -- %f"
set pgp_import_command="gpg2 --no-verbose --import %f"
set pgp_export_command="gpg2 --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg2 --no-verbose --batch --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg2 --no-verbose --batch --with-colons --list-keys %r"
set pgp_list_secring_command="gpg2 --no-verbose --batch --with-colons --list-secret-keys %r"
set pgp_autosign=yes
set pgp_replyencrypt=yes
set pgp_replysign=yes
set pgp_timeout=1800
set pgp_good_sign="^gpg: Signature correcte de "
```

N'oubliez pas de remplacer `VOTRE_ID` par l'ID de votre clef, et pensez également à modifier le cas échéant le chemin de `pgpewrap`.

Lisez la partie du dessus pour la récupération automatique des clefs.

### Ça fonctionne comment ?

C'est bien mignon de configurer mais encore faut-il savoir l'utiliser. Rassurez-vous, l'utilisation est enfantine. Tout est à base de clavier et des simples touches, comme tout le reste sous Mutt !

#### L'ouverture

En premier lieu, pour identifier un mail chiffré, il faut repéré le fameux _flag_ dans l'index. Le flag indiquant qu'un mail est chiffré est `P`, il est 100% du temps accompagné de son copain `+` qui indique que le message est destiné à vous et uniquement vous (normal, vu que c'est chiffré avec votre clef publique). Même si dans les faits je peux chiffrer un mail et mettre quelqu'un en copie juste pour le narguer, avouons tout de même que cela n'a que peu d'intérêt. Pour les messages signés, on rencontrera deux flags, le `s` pour indiquer que le message est signé mais que la signature n'a pas été vérifée, et `S` qui vous indique que c'est signé et vérifié.

Pour lire, comme tous les mails, positionnez vous sur le mail en question et appuyez simplement sur Entrée. Normalement la demande de passphrase apparaît. Entrez votre passphrase longue comme un jour sans café, et miracle, le mail s'affiche en clair devant vos yeux humides avec quelques bafouilles supplémentaires de la sorte :

```
[-- Début des informations sur la signature --]
Bonne signature de : ************
               aka : ************
               (autres champs aka selon le nombres d'identités déclarées sur la clef)
créée : *****
ATTENTION ! Nous n'avons AUCUNE indication informant si la clé appartient à la personne nommée ci-dessus
Empreinte : **** **** **** **** **** **** **** **** **** ****
[-- Fin des informations sur la signature --]

[-- Les données suivantes sont signées et chiffrées avec PGP/MIME --]

< liste des pièces jointes >

corps du mail

< pièces-jointes éventuelles >
[-- Fin des données signées et chiffrées avec PGP/MIME --]
```

Bien évidemment c'est variable selon les mails, et selon si les différentes étapes du déchiffrement se sont bien déroulées.

À ce moment si je veux lire un autre mail chiffré, et selon la valeur de `pgp_timeout` Mutt me demandera, ou pas, ma passphrase.

#### L'envoi

Pour l'envoi, c'est aussi simple. Rédigez un mail comme n'importe lequel en appuyant sur la touhe `m`, composez, enregistrez le fichier contenant votre message et Mutt vous présente la fenêtre d'envoi. Cette fenêtre on la connaît bien, on a vu dans les épisodes précédents qu'on peut y faire énormément de choses, comme ajouter des destinataires, que ce soit en "À" ou "Cc", "Cci", ajouter des pièces-jointes, etc.

Et bien c'est dans cet écran qu'on va gérer le chiffrement de notre mail.

Sauf que de base le menu est caché, comme énormément d'options de Mutt, d'une part parce que le pourcentage total de la population qui chiffre ses mails est hélas vraiment trop bas pour que l'option apparaisse par défaut et d'autre part parce qu'il y a vraiment beaucoup d'options. Le menu est caché derrière la touche `p`, comme PGP. Ce n'est pas compliqué.

Comme tout est bien défini dans la configuration (aussi bien la clef que les différentes commandes), le menu est vraiment très simple, il prend la forme d'une barre en bas de votre écran, qui varie selon la version de (Neo)Mutt:

En anglais, sur les anciennes versions:

```
(e)ncrypt, (s)ign, sign (a)s, (b)oth, select (m)ic algorithm, or (f)orget it?
```

En français sur des Neomutt récents:

```
Chiffrer pgp, Signer, En tant que, les Deux, s/Mime, ou Rien ?
```

À ce moment, plusieurs choix s'offrent à vous. C'est pas clair sur la version récente et française mais les lettre en majuscules sont les possibilités qui vous sont offertes. Nous avons donc le choix entre :

  * `c`: Chiffre le mail. Le _header_ "Sécurité" apparaît avec `Sécurité : Chiffrer (PGP/MIME)` au lieu de `Sécurité: Aucune`
  * `s`: Signe le mail. Le _header_ "Sécurité" apparaît avec `Sécurité : Signer (PGP/MIME)` puis en dessous `Signer avec : ID`
  * `e`: Permet de signer avec une autre clef que celle par défaut, une fois la touche `e` pressée, Mutt vous demande avec quelle ID signer
  * `d`: Permet de chiffrer **et** de signer. Le _header_ "Sécurité" apparaît avec `Sécurité : Signer, Chiffrer (PGP/MIME)` puis en dessous l'ID de la clef utilisé pour la signature
  * `m`: Permet de signer en S/MIME, clairement on s'en fiche dans le cas présent
  * `r`: Ça retire la signature ou le chiffrement déjà présent

Bien évidemment, il faut adapter les lettres si votre Mutt est en anglais.

Notez que si vous vous êtes trompés, par exemple vous avez signé un message mais que vous souhaitez annuler la signature, rappuyez sur `p` et indiquer `r` pour enlever signature et/ou chiffrement.

`y` pour envoyer et c'est tout, facile non ? En deux touches on envoie un mail signé et/ou chiffré !

Je ne l'ai pas précisé parce que ça me semblait évident, mais si vous affichez un mail déchiffré, la source, donc le mail, que votre boite soit au format Maildir ou mbox, reste chiffrée sur le disque. Un `cat` de votre fichier mbox affichera bien du contenu chiffré. Vous pouvez également aller gratter dans votre dossier temporaire dédié à Mutt, point de message en clair même pendant le temps où il est déchiffré dans Mutt.

### Fin

C'est ainsi que s'achève cet épisode 4 entièrement dédié au chiffrement de mail sous Mutt ! Je voulais le coupler avec d'autres choses, mais comme cet article est assez long et plutôt lourd, et bien on va rempiler pour un épisode 5 !

Au menu du prochain épisode ?

  * Un carnet d'adresse automatique (en vrai ce coup-ci)
  * Imprimer des mails en PDF
  * Créer un thème graphique pour Mutt
  * Un point sur comment appeler proprement votre éditeur favori pour rédiger vos mails, qu'il soit en console ou graphique

Comme à l'habitude, n'hésitez pas à me contacter ou à écrire sur les [listes](/communaute) en cas d'idée, de coup de main, ou pour papoter sur l'article !
