---
authors: ["zilkos"]
title: 'Mutt: les finitions !'
date: '2018-04-01'
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Mutt", "CLI", "Mail"]
toc: true
---

> Les autres épisodes sont [ici](/tags/mutt) !

Bienvenue dans ce cinquième épisode de la série d'article consacrée au célèbre client mail en console: **Mutt** !

L'épisode précédent était entièrement consacré à un gros morceau : le chiffrement et tout ce qui s'y rapporte. Dans ce cinquième et dernier épisode, on va travailler sur les finitions, à savoir quelques outils pratiques, mais pas forcément vitaux. On va voir comment mettre rapidement en place un système de gestion des contacts, comment pouvoir imprimer nos mails et configurer quelques éditeurs pour rédiger les mails.

### Un carnet d'adresses

Commençons par quelque chose de plutôt simple à mettre en place, pour gagner en confort sur un point que nous n'avons pas encore abordé : les contacts.

Actuellement quand nous souhaitons taper un mail, nous sommes obligés de taper entièrement l'adresse à la main. Fastidieux s'il en est, réglons ce problème. Tout ceci se fait autour d'un logiciel : le bien nommé _abook_, que nous allons configurer aux petits oignons pour qu'il soit un pilier de notre utilisation de Mutt.


#### abook

abook est un petit mais puissant utilitaire de carnet d'adresses. Uniquement en console et basé _ncurses_, il est très rapide et fait son boulot à la perfection. L'installation est extrêmement simple, suffit d'installer le paquet `abook`.

Une fois le paquet installé, lancez-le avec la commande éponyme `abook`. L'interface est on ne peut plus simple, de simples touches suffisent pour ajouter, modifier ou supprimer des contacts et leurs informations. Jouez un peu avec histoire de comprendre comme tout ceci fonctionne.

La page man nous offre un exemple de configuration commentée pour que vous puissiez voir ce qu'il est possible de faire avec :

```sh
# sample abook configuration file
#

# Declare a few custom fields
field pager = Pager
field address_lines = Address, list
field birthday = Birthday, date

# Define how fields should be displayed in tabs
view CONTACT = name, email
view ADDRESS = address_lines, city, state, zip, country
view PHONE = phone, workphone, pager, mobile, fax
view OTHER = url, birthday

# Preserve any unknown field while loading an abook database
set preserve_fields=all

# Automatically save database on exit
set autosave=true

# Format of entries lines in list
set index_format=" {name:22} {email:40} {phone:12|workphone|mobile}"

# Show all email addresses in list
set show_all_emails=true

# Command used to start mutt
set mutt_command=mutt

# Return all email addresses to a mutt query
set mutt_return_all_emails=true

# Command used to print
set print_command=lpr

# Command used to start the web browser
set www_command=lynx

# Address style [eu|us|uk]
set address_style=eu

# Use ASCII characters only
set use_ascii_only=false

# Prevent double entry
set add_email_prevent_duplicates=false

# Field to be used with "sort by field" command
set sort_field=nick

# Show cursor in main display
set show_cursor=false
```

Pour information, un tel fichier de configuration se place dans `$HOME/.abook/abookrc` !

Attention toutefois pour `set mutt_command=mutt` pour ceux disposant de `neomutt` bien évidemment.

#### Mutt et abook

Pour faire communiquer les deux, c'est très simple et ça se résume en quelques lignes de configuration.

Tout d'abord, on supprime dans notre `.muttrc` les lignes qui permettent à Mutt de gérer nos alias vu que nous allons utiliser abook pour les gérer.

Les lignes en question sont les deux suivantes :

```sh
set alias_file="$HOME/.mutt/.mutt-alias"
source .mutt-alias
```

Et à la place nous allons rajouter les trois lignes suivantes:

```sh
set query_command= "abook --mutt-query '%s'"
macro index,pager A "<pipe-message>abook --add-email-quiet<return>" "Ajouter l'expediteur dans abook"
bind editor <Tab> complete-query
```

La première ligne détermine la commande que Mutt utilise pour faire des requêtes externes d'adresses. Donc au lieu d'interroger le fichier d'alias de Mutt, on va interroger abook qui dispose de tout ce qu'il faut pour causer avec Mutt.

Sur la deuxième ligne on définit une macro, qui sera active sur l'index et le pager, et qui ajoutera l'adresse d'un mail reçu ou envoyé dans abook lors de l'appui sur `A`. Libre à vous, bien entendu, de la lier à une autre touche ou combinaison de touches.

La troisième ligne nous permet de définir un comportement simple: Quand je commence à taper une adresse dans Mutt, si j'appuie sur la touche Tab, abook sera appelé et me proposera des adresses commençant par ce que j'ai tapé.

#### Utilisation

Naviguez dans vos différentes boites et ajoutez des contacts via le raccourci `A` (ou celui que vous avez indiqué dans la configuration). Normalement vous avez des adresses dans votre carnet (vérifiable facilement en lançant abook depuis un terminal).

##### Envoyer un mail à une seule personne

  * Appuyez sur `m` pour faire un nouveau message
  * Quand Mutt affiche `À:` appuyez sur `Ctrl + t` pour faire apparaître abook dans Mutt
  * Naviguez avec les flèches pour vous positionner sur la personne en question
  * Appuyez sur `Entrée` pour valider l'adresse
  * Appuyez sur `Entrée` pour valider dans Mutt et passer à l'objet

Ou

  * Appuyez sur `m` pour faire un nouveau message
  * Quand Mutt affiche `À:`, commencez à taper l'adresse puis pressez `Tab` pour voir ce qu'abook vous propose
  * Validez avec `Entrée`

##### Envoyer un mail à plusieurs personnes

Le but sera de _taguer_ plusieurs personnes dans abook depuis Mutt et d'envoyer tout ce petit monde dans Mutt:

  * Appuyez sur `m` pour faire un nouveau message
  * Quand Mutt affiche `À:`, appuyez sur `Ctrl + t` pour faire apparaître abook dans Mutt
  * Naviguez avec les flèches pour vous positionner sur la personne en question
  * Pour chaque personne que vous souhaitez rajouter, appuyez sur `t` comme "tag"
  * Une fois que tout le monde est sélectionné, appuyez sur `;m` (oui, `;` suivi de `m`)
  * Mutt affiche toutes les adresses dans le champ `A:`, validez avec `Entrée`

Bien évidemment, ça reste modifiable quand vous êtes dans votre éditeur et que `set edit_headers` est indiqué dans votre `.muttrc`. Pour rappel, cela fait en sorte d'afficher les _headers_ dans votre éditeur.

Avec tout ceci et quelques exemples d'utilisation, nous avons maintenant Mutt qui communique facilement avec abook !

### Imprimer (avec modération)

Je ne suis pas un grand fan d'impression de mail. Généralement je me limite à de l'impression en PDF pour garder certains courriels qui sont communiquées uniquement par mail (institutions, administration, etc). Imprimer des mails correctement formatés depuis Mutt est très facilement possible et plutôt simple à mettre en place.

Ça tient dans un utilitaire et un fichier de configuration !

Plusieurs méthodes sont possibles pour arriver de l'impression sous Mutt mais j'ai choisi de vous présenter la plus simple, à base de Muttprint.

#### Muttprint

Nous allons donc installer Muttprint, le configurer et le lier à Mutt. Muttprint utilise LaTeX pour la mise en forme et la génération de PDF donc il vient avec un texlive minimal. Les versions 0.2x étaient basées sur du html2ps, nettement mieux à mon goût mais passons.

Installez donc le paquet `muttprint`, sans surprise.

Pour la configuration, nous avons plusieurs possibilités entre les fichiers suivants:

  * `/etc/Muttprintrc`
  * `$HOME/.muttprintrc`

Personnellement j'ai choisi `$HOME/.muttprintrc` car c'est un emplacement présent dans mes sauvegardes sans que j'ai à spécifier autre chose dans ma configuration de sauvegarde (de base, il prend tout le `$HOME`, moyennant quelques exclusions).

Créez donc ce fameux `.muttprintrc`.

##### Côté .muttrc

Avant de plonger dans la configuration de Muttprint, on va d'abord le lier à Mutt. Très simple, une ligne de configuration suffit dans notre `.muttrc` :

```sh
set print_command="muttprint -P A4 -p PDF"
```

On lui indique que nous souhaitons utiliser le programme `muttprint` pour imprimer nos mails et on passe quelques arguments à Muttprint:

  * `-P` qui est le format de papier, A4 dans notre cas
  * `-p` qui est le nom de l'imprimante, nommée PDF dans mon cas

[Pour le man, c'est par ici](https://linux.die.net/man/1/muttprint).

En effet, j'ai deux imprimantes, une physique et une virtuelle, la physique est appelée "Maison" parce que j'ai un côté madame Michu et l'autre appelée PDF, tout simplement parce que c'est une imprimante PDF. Tout ce beau monde est géré par CUPS.

Un `lpstat -a` renvoie donc ceci:

```
zilkos@hodor ~ > lpstat -a
Maison accepte des requêtes depuis [...]
PDF accepte des requêtes depuis [...]
```

Si je veux changer ma configuration de Mutt pour qu'il imprime sur l'imprimante physique par défaut, je n'ai qu'à changer `PDF` par `Maison` dans ma configuration. Bien évidemment, je laisse l'imprimante par défaut sur PDF dans Mutt et après j'imprime mon PDF sur l'imprimante physique, ça évite de modifier ma configuration parce que tout comme vous, je suis fainéant.

##### Côté .muttprintrc

Ce fichier n'est pas créé par défaut, car Muttprint est utilisable sans configuration et fonctionne très bien. Mais personnellement, j'ai modifié quelques petites choses pour qu'il soit plus adapté à mes envies.

Je n'ai pas rajouté grand-chose, uniquement ceci:

```sh
FONT="Palatino"
```

La police de caractère pour l'entête fixé à Palatino. De base vous avez le choix entre :

  * - Latex (EC-Fonts)
  * - Latex-bright (CM-bright)
  * - Times
  * - Utopia
  * - Palatino
  * - Charter
  * - Bookman

L'entête contient les champs comme "À", la date, le "From", etc.

```sh
FRONTSTYLE="fbox"
```

Détermine le cadre qui sera appliqué autour de l'entête. Là aussi vous avez plusieurs valeurs, libre à vous de les tester :

  * plain: rien
  * border: simple ligne après les _headers_
  * fbox: une boite autour des _headers_
  * shadowbox: une boite avec un peu d'ombre
  * ovalbox: une boite avec les coins arrondis
  * Ovalbox: une boite avec les coins arrondis et des traits plus épais
  * doublebox: une boite à double traits autour des _headers_
  * grey: fond gris sur les _headers_
  * greybox: fond gris et box aux traits noirs


```sh
PRINT_HEADERS="Date_To_From_CC_Newsgroup_*Subject*"
```

Détermine les headers à afficher et l'ordre d'affichage. Notez qu'un header entouré de `*` sera en gras.

```sh
FONTSIZE="10pt"
```

Tout simplement la taille de la police d'écriture utilisée.

Si toutefois vous connaissez LaTeX, vous pouvez spécifier du code LaTeX à intégrer en plus via ces lignes de configuration :

```sh
LATEXCODE=""
LATEXCODE1=""
LATEXCODE2=""
[...]
LATEXCODE5=""
```

##### On imprime comment ?

Sous Mutt, dans le pager ou dans l'index, il suffit d'appuyer sur la touche `p` ! Par défaut, il va écrire un PDF temporaire à l'emplacement indiqué par `set tmpdir` avec un nom imbitable.

Le carnet d'adresses c'est fait, l'impression c'est fait. Passons donc un peu de temps sur quelques éditeurs pour là aussi gagner en souplesse sur l'écriture de nos mails. Ce n'est pas grand-chose, mais ça peut aider.

### Les éditeurs

On va commencer par des éditeurs graphiques, avec trois fois rien d'explications, car c'est vraiment simple. On se gardera de juger l'utilisation d'un client graphique dans le cadre d'un logiciel de mail en console.

#### Atom et Sublime Text

Il y a un souci avec ce type d'éditeur. Faisons un simple test avec atom comme éditeur pour mutt :

```sh
set editor="atom"
```

Essayez donc de rédiger un mail, vous n'y arriverez pas. Tout simplement parce qu'atom renvoie un retour dès qu'il est ouvert. Mutt reprend donc la main directement après l'ouverture d'atom, ne voit pas de fichier sauvegardé et par conséquent vous gratifie d'un gentil "Non, t'as rien modifié, donc il n'y a pas de mail à envoyer".

Dans le cas de Sublime Text c'est pareil.

Il faut donc aller voir la documentation des arguments de ligne de commande pour leur dire d'attendre.

Dans le cas d'atom et de Sublime Text, c'est via un `--wait` en version longue ou `-w` en version courte.

Ainsi, l’appelé est en pause, le temps de rédiger le mail, ce qui règle le problème. Optez donc pour une configuration de ce type :

```sh
set editor="atom -w"
# OU
set editor="subl -w"
```

Une fois le fichier (donc le mail) enregistré et l'éditeur quitté, Mutt reprend correctement la main et tout se déroule bien.

Mais bon, les éditeurs graphiques sont généralement lents au démarrage et leur invocation depuis la ligne de commande relativement limitée, normal ils ne sont pas faits pour ça.

Penchez-nous sur deux mastodontes dans le domaine : Emacs et vim.

#### Emacs (et Spacemacs)

Troll à part, Emacs est mon éditeur préféré, je m'en sers pour à peu près tout.

Dans notre cas précis, nous allons faire deux choses :

  * Activer le `mail-mode` automatiquement pour les mails Mutt
  * Modifier le comportement de `mail-mode` pour lui ajouter du confort

Ça fonctionne pour tout le monde, sous Emacs ou Spacemacs.

##### Activation et modification du mail-mode

Si vous avez déjà utilisé sérieusement Emacs dans votre vie, vous devriez normalement être capable de le faire. Il s'agit simplement d'activer un mode pour un fichier en particulier.

Ça se passe comme ça, dans votre `.emacs` ou `.spacemacs` :

```lisp
(add-to-list 'auto-mode-alist '("/mutt" . mail-mode))
```

Attention, quelques subtilités:

  * Pour les utilisateurs de Spacemacs, c'est à mettre dans la fonction `(defun dotspacemacs/user-config ())`
  * Pour les utilisateurs de Neomutt, remplacez `/mutt` par `/neomutt`

À partir de ce moment-là, nous pouvons modifier le comportement de `mail-mode` comme nous le voulons.

Par exemple pour couper les lignes intelligemment, sans avoir besoin de marteler `M-q` toutes les deux minutes, on peut rajouter sur ce `mail-mode`, le mode mineur `auto-fill-mode`:

```lisp
(add-hook 'mail-mode-hook 'turn-on-auto-fill)
```

Vous pouvez bien sûr modifier ce que vous voulez à la suite de tout ça, notamment un bon vieux

```sh
(setq make-backup-files nil))
```

pour éviter de vous faire polluer de fichier `~` ou `.bak`.

##### Démoniaque !

Personnellement je n'utilise pas Spacemacs, je préfère monter mon éditeur avec mes trucs à moi, que je maîtrise correctement. Quand j'avais testé Spacemacs, je le trouvais plus que super lent au démarrage.

Je ne sais pas si ça s'est amélioré avec le temps, mais je vais vous présenter une méthode qui permet de _daemoniser_ Emacs. Avec cette méthode, Emacs démarre en un rien de temps :

```sh
> time emacs -debug-init -eval '(kill-emacs)' -Q
emacs -debug-init -eval '(kill-emacs)' -Q  0,29s user 0,03s system
```

Dans un premier temps je rajoute le _daemon_ Emacs au démarrage de la machine. J'utilise i3 personnellement, donc ça se passe dans le `.i3/config` de la manière suivante :

```sh
exec_always --no-startup-id emacs --daemon
```

En bref, rajoutez `emacs --daemon` à la liste des programmes qui se lancent au démarrage.

Une fois ceci fait, le but ne sera pas de lancer Emacs, mais bien un **client** Emacs qu'on rattache au _daemon_ Emacs. Un alias pour le lancer en ligne de commande par exemple :

```sh
alias ec-"emacsclient -nw -c"
```

`-nw` pour _no window_ (donc en CLI) et `-c` pour créer une nouvelle fenêtre, textuelle dans notre cas, rattachée au serveur

Donc pour lancer un `emacsclient` depuis Mutt, il faudra écrire la ligne définissant l'éditeur de la sorte :

```sh
set editor = "emacsclient -nw -c +8"
```

Et hop, votre client Emacs s'affiche instantanément. Le `+8` permet de positionner le curseur à la ligne huit dès l'ouverture d'Emacs, car les six premières lignes sont les _headers_ et que je laisse une ligne vide entre les _headers_ et le début du mail.

Avec ces configurations, Emacs entrera directement en `mail-mode`, activera l'`autofill` et sera positionné à la bonne ligne pour commencer immédiatement la rédaction de mon mail.

#### Vim

Hélas je ne pratique que très peu Vim, donc mise à part vous indiquer la configuration existante depuis le début de la série sur Mutt, je ne vais pas faire grand-chose d'autres:

Définir l'éditeur dans `.muttrc` de la manière suivante:

```sh
set editor - "vim -c 'set tw=72' -c 'set wrap' -c ":8""
```

va nous permettre de :

  * Fixer la longueur de ligne à 72 caractères
  * Correctement gérer les coupures de lignes
  * Commence l'édition à la ligne 8

C'est déjà pas mal et si vous avez une configuration sympathique de Vim pour Mutt, n'hésitez pas à enrichir cet article en visitant le [dépôt](https://gitlab.com/Zilkos/blog).

### Fin

C'est ainsi que s'achève cette suite de cinq articles pour commencer à prendre en main Mutt. À l'origine, j'ai commencé cette suite d'articles qui compile et explique plusieurs choses, car je n'avais pas trouvé de ressources françaises explicatives sérieuses et complètes. Je m'étais donné pour but de construire une telle chose pour essayer d'aider au maximum le débutant qui viendrait par ici en quêtes d'explications et pas juste en quête de configurations à copier/coller.

J'espère que j'ai réussi (un peu) ce défi. J'espère que cette suite d'article en aidera certains à y voir plus clair dans le monde de Mutt. J'espère également que tout est suffisamment expliqué pour que tout le monde s'en sorte.

Cette suite d'article est un début sur Mutt car bien évidemment elle ne peut pas tout couvrir ni aborder tous les points et il manque probablement des choses, mais j'ose espérer qu'elle permet d'aborder sereinement ce magnifique client mail qu'est Mutt.

Si vous avez des retours, des problèmes, des améliorations, [n'hésitez pas](/communaute) !
