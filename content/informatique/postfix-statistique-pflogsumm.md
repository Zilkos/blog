---
authors: ["zilkos"]
title: "Postfix: Statistiques sur les logs avec Pflogsumm"
date: 2013-04-12
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["Postfix"]
toc: true
---

Souvent sur des forums on trouve des questions comme: _"Y a-t-il une commande permettant de savoir combien de mails ont été envoyés ou reçus ?"_

Non Postfix ne possède pas en natif un moyen d’extraire ce genre de données.

Postfix écrit juste d’excellents fichiers journaux et il existe plusieurs outils permettant de traiter ces logs dont un qui permet de tirer des statistiques très claires et informatives. L’outil présenté aujourd’hui est pflogsumm (pour _Postfix Log Summary_, en français, quelque chose comme: Résumé des logs de Postfix). C'est parti !

> Attention, Pflogsumm compte simplement tout ce qui passe. Si un même mail passe deux fois dans la chaine de traitement, il considère que ce sont deux mails différents. Les résultats peuvent ne pas être précis.

## Pflogsumm, cet outil merveilleux

Comme tout ce qui est merveilleux, c’est écrit en Perl.

On commence par récupérer le tar.gz de pflogsumm [par ici](http://jimsun.linxnet.com/postfix_contrib.html). Il vous faudra aussi le module Perl `Date::Calc` disponible depuis le CPAN ou [ici](http://search.cpan.org/dist/Date-Calc/).

Dans les commandes suivantes, les lignes ou blocs commençant par # sont des commentaires.

```bash
wget ftp://jimsun.linxnet.com/pub/postfix_contrib/pflogsumm-1.1.3.tar.gz # Version de production à l'écriture de cet article
cpan #On lance le CPAN pour aller installer notre module
>cpan #Le prompt CPAN s'affiche
>cpan install Date::Calc #On installe le module Date::Calc
tar -zxvf pflogsumm-1.1.3.tar.gz #On extrait les fichiers de l'archive
mv pflogsumm-1.1.3/ pflogsumm #On renomme le dossier extrait
```

On a tout ce qu’il nous faut !

### Automatisation de l'exécution

Le but: Lancer tous les jours à midi pflogsumm, récupérer le résultat et envoyer un mail avec le résultat. Tout d’abord, on déplace pflogsumm dans le `/usr/`:

```bash
mv pflogsumm/ /usr/
```

Ensuite, on lance `crontab` -e puis on entre cette ligne en guise de nouvel événement cron:

```bash
00 12 * * * /usr/pflogsumm/./pflogsumm.pl -u 5 -h 5 –problems_first -d today /var/log/mail.log | mail -s "Rapport Postfix du  `date`" admin@domain.tld
```

Pour ceux ne parlant pas le cron, une petite explication:

Tous les jours de toutes les semaines de tous les mois à 12h on lance le script Perl (`./pflogsumm.pl`) situé dans `/usr/pflogsumm/` avec les arguments (`-u 5 -h 5 –problems_first -d today /var/log/mail.log`) puis on envoie la sortie (le résultat) du script par mail (`|mail -s “Rapport Postfix du  `date`” admin@domain.tld`).

Notez que date insère la date dans l’objet du mail sous la forme `vendredi 12 avril 2013, 10:20:24 (UTC+0200)`. C’est au final, le résultat de la commande `date` dans un terminal ajouté à la suite de `Rapport Postfix du`.

Notez que la commande `mail` émane du paquet `mailutils`. Pensez à l’installer si la commande n’est pas reconnue.

Vous pouvez aussi en faire un alias de commande pour le lancer facilement et avoir un rapport quand vous le souhaitez. Ou lancez cette commande pour le voir en direct:

```bash
logsum -d today /var/log/mail.log | more
```

On reçoit un mail automatiquement tous les jours à midi sur l’adresse spécifiée dans la commande présente dans crontab avec les informations suivantes (extrait):

```bash
Grand Totals
------------
message deferral detail:
message bounce detail (by relay):
message reject detail:
message reject warning detail:
message hold detail:
message discard detail:
smtp delivery failures:
Warnings
Fatal Errors: none
Panics: none
Master daemon messages: none
Per-Hour Traffic Summary
Host/Domain Summary: Message Delivery (top 5)
Host/Domain Summary: Messages Received (top 5)
top 5 Senders by message count
top 5 Recipients by message count
top 5 Senders by message size
top 5 Recipients by message size
```

Ainsi, vous avez une vue complète de tout ce qui rentre et de tout ce qui sort en un coup d’oeil. Les personnes ayant peur de se faire détourner un serveur pourrons facilement voir le volume de mail sortant par jours, les problèmes rencontrés, etc… Bref, un bon moyen d’avoir une vue globale de Postfix en terme de flux de mails. Coupler ceci à un logcheck ou un logwatch vous permettra d’avoir une vue complète de ce qui se passe dans les moindres détails sur votre serveur.
