---
authors: ["zilkos"]
title: 'De belles présentations en markdown avec Marp '
date: '2018-11-17'
lastmod: '2018-11-18'
toc: true
categories: ["Logiciel"]
tags: ["Markdown"]
mathjax: true
emoji: true
---

{{% alert %}}
**Attention, cette page fait appel à un CDN.** 

Si vous disposez d'une extension bloquant les appels à des CDN, il est probable que le rendu ne soit pas cohérent.  
Unixmail n'aime pas ce genre de pratique, aussi, nous vous prévenons à chaque fois qu'une page utilise un CDN.  
Pour votre vie privée, nous vous recommandons l'extension [Decentraleyes](https://decentraleyes.org/). 
{{% /alert %}}


Il fallait quand même que je vous parle d'un outil que j'utilise dans mon boulot et qui m'a allégé de nombreux maux de tête.

Je suis souvent amené (à chaque fois que j'interviens en fait) à faire des présentations. Fut un temps j'utilisais le fabuleux couple LaTeX et Beamer, sauf que le problème dans l'équation, c'est LaTeX. On en a déjà parlé ici, il est assez lourd, les erreurs de compilations sont peu claires et comme je ne l'utilisais pas assez je perdais un temps monumental à ré-apprendre à chaque fois les commandes.

Je suis donc parti en quête d'un remplaçant et j'ai souhaité joindre l'utile à l'agréable en ne cherchant que des solutions à base de **markdown**. Tout simplement parce que je fais quasiment tout avec ce langage de balisage léger, standard (ou presque), en texte brut, extrêmement simple et très puissant à la fois. C'est avec une grande surprise que j'ai découvert **Marp** qui pourtant existe depuis un moment déjà.


### Marp

Marp est donc un logiciel, basé sur Electron (son seul défaut) qui permet de rédiger avec l'aide de la syntaxe Markdown des jolies présentations (certains disent "*slide*"). C'est un logiciel graphique  qui va beaucoup évoluer sous peu, mais je vous en parlerai après.

Quelques caractéristiques de la bête :

  * Multiplateforme (Electron hein)
  * 3 modes de pré-visualisation en direct
  * Support des emojis
  * Support des symboles et expressions mathématiques
  * Support d'image de fond avec ajustement de la transparence
  * Export des présentations en PDF
  * Deux thèmes graphiques

Quelques informations essentielles :

 * [Le site du projet](https://yhatt.github.io/marp/ "Site de Marp")
 * [Le dépôt Github](https://github.com/yhatt/marp/ "Github Marp")


### Comment ça fonctionne ?

Comme n'importe quel éditeur markdown, avec quelques spécificités...


#### Les bases

En premier lieu il convient de délimiter les *slides* avec `---`. Exemple simple, deux slides avec des niveaux de titre différents en guise de contenu :

```
---

# Titre 1
## Titre 2
### Titre 3

---

#### Titre 4
##### Titre 5
###### Titre 6

```

Je vous passe les basiques, comme les mises en forme, les listes à puces ou numérotées, imbriquées, etc. Pour les emojis, certains peuvent être utiles, comme `heavy_check_mark` (c'est ça -> :heavy_check_mark:). Vous pouvez en retrouver une liste complète [ici](https://www.webpagefx.com/tools/emoji-cheat-sheet/).

Marp permet également une gestion assez fine des images. Par défaut, une image sera affichée à 100 % de sa taille originale et alignée à gauche. Pour la redimensionner à 45 % et la centrer, c'est comme ceci :

```
![40% center](images/mon_image.jpg)
```

Pour mettre une image en fond de votre *slide*, c'est comme ceci :

```
![bg 50%](images/background.jpg)
```

Si vous voulez que l'image ne soit pas adaptée aux dimensions de votre *slide*, optez pour :

```
![bg original](images/background.jpg)
```


#### Les directives

Les directives sont des informations à indiquer pour définir certains comportements, avec une syntaxe de commentaire HTML. Elles peuvent être locales ou globales, donc s'appliquer sur une partie du document, ou son intégralité.

Vous pouvez par exemple indiquer le thème que vous souhaitez utiliser :

```
<!-- $theme: default -->
```

Personnellement, et selon les sujets, j'opte généralement pour le thème *gaia* que je trouve un peu moins fade. Tout dépend du contenu (et de la qualité du projecteur aussi). C'est bien évidemment une directive globale, qui s'appliquera à chaque *slide*.

On peut intégrer un pied de page qui sera rappelé sur toutes les *slides* de votre présentation, de cette manière :

```
<!-- footer: Contenu sous Licence Creative Commons BY-SA-->
```

Le *footer* est positionné en bas à gauche de la *slide* donc attention à votre contenu, notamment les images. C'est une directive locale, ce qui fait que vous pouvez l'appeler à la deuxième ou troisième *slide* pour ne pas gâcher votre *slide* de titre.

On peut également inclure la numérotation des pages, qui elle se met à droite, comme ceci :

```
<!-- page_number: true -->
```

Un simple booléen pour l'activer ou non, c'est également une directive locale.

Qui dit projecteur nul, dit souvent ratio de 4:3 au lieu d'un bon vieux 16:9. On peut préciser ce ratio via une directive (globale, donc on peut pas changer de ratio en cours de route):

```
<!-- $size: 4:3 -->
```

Si vous voulez générer un PDF non pas pour la présentation mais uniquement pour la lecture, sachez que différents formats sont supportés :

```
<!-- $size: a4 -portrait -->
```

Plusieurs options sont disponibles : 4:3, 16:9, de A0 à A8, de B0 à B8. Si vraiment cela ne vous convient pas, libre à vous de déterminer la hauteur et la largeur comme ceci :

```
<!-- $width: 1920 -->
<!-- $height: 1080 -->
```

#### Des maths !

Marp se base sur KaTeX pour intégrer des formules mathématiques dans les *slides*. Donc rien de bien compliqué :

```
Loi de Fitts (bloc) :

$$T = a + b * log_2 ({D \over L} +1) $$
```

Ça donne ceci :

Formule (*inline*) : si \\( a \ne 0 \\) alors \\(ax^2 + bx + c = 0\\)

Loi de Fitts (bloc) :

$$T = a + b * log_2 ({D \over L} +1) $$

On indique de l'*inline* avec un seul `$` et un bloc avec deux.

### En conclusion

Donc en gros, tout ce que vous pouvez faire en GFM (*Git Flavored Markdown*), vous pouvez le faire sur Marp ! C'est un excellent outil, couplé à un outil de présentation comme [Impressive](http://impressive.sourceforge.net/) même si il est un peu vieillissant, vous aurez des présentations homogènes et plutôt classes. Oui, Marp ne génère que les *slides*, il ne gère pas l'affichage de celles-ci. Cet article est un peu court, et tant mieux, ça veut dire que le produit est simple, abordable par tous, pas besoin de bidouiller quoique ce soit pour ça fonctionne !

La prise en main est extrêmement rapide si vous connaissez déjà le Markdown, et si vous ne le connaissez pas, il est également très rapide à apprendre.

Sachez également que c'est un projet qui va beaucoup bouger dans les temps à venir. En gros, la version actuelle ne bouge pas, et l'équipe en charge du projet réécrit totalement le produit en développement un framework ([Marpit](https://marpit.marp.app/)) qui va faire visiblement beaucoup de choses, notamment de la transformation de *slide* pour le web ou l'impression, des outils en CLI pour exporter en PDF ou en HTML, bref plein de belles choses !

Récemment, une entrée [CVE](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-15685) pour Electron est parue pour la version 0.0.13 mais l'équipe a patché rapidement, donc ne vous inquiétez pas quand à la viabilité de la chose, même si l'équipe développent le futur Marp, ils sont plutôt réactifs en cas de souci sur le produit actuel.

Je pratique Marp depuis pas mal de temps, j'attends avec grande impatience la suite, car il y aura probablement moyen de faire un mode quelconque pour Emacs et de profiter des modes existants pour avoir de la live-preview grâce aux outils en CLI. Bref, des bonnes choses à venir !

N'hésitez pas à poke la mailing list en cas de problème ou de question sur le sujet !
