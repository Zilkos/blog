---
authors: ["zilkos"]
title: 'Profanity, un client XMPP à la sauce Irssi'
date: 2016-11-29
lastmod: 2018-11-18
categories: ["Linux", "Tutoriel"]
tags: ["XMPP", "Jabber"]
toc: true
---

Présentation de Profanity, un client XMPP très efficace qui a le bout goût d'Irssi !

## Impiété !

Derrière ce nom évoquant la cause de la mort de Socrate se cache un client XMPP (ou historiquement Jabber) en console très efficace et simple. Profanity, codé en C avec ncurses et la libstrophe est clairement inspiré d'Irssi, le célèbre client IRC, lui aussi en console et lui aussi écrit en C.

Profanity tourne partout ou presque, toutes les distributions Linux, FreeBSD, Android via Termux, Windows via Cygwin et même OSX (enfin macOS maintenant). La dernière version à l'heure où je rédige ces lignes est la 0.5.0 et supporte actuellement [18 XEPs](http://profanity.im/xeps.html) du protocole XMPP.

Au menu, l'utilisation basique, l'ajout des contacts, l'utilisation globale jusqu'au chiffrement via OTR ou OpenPGP.

### Profanity, la base

Présent dans les dépôts, je vous fais grâce de l'installation enfantine. Quelques précisions tout de même, si vous souhaitez disposer du support d'OTR, il vous faudra la `libotr` en vesion 3.2.X ou 4.0.X. Si vous disposez de `libotr-dev`, le support d'OTR sera inclus par défaut, installez le paquet avant d'installer Profanity. Autrement il existe un argument pour la config si vous souhaitez avoir le support OTR en l'installant depuis les sources: `./configure --enable-otr`

Pour le lancer, un simple `profanity` dans votre terminal. On y retrouve une fenêtre très ressemblante à Irssi, avec plusieurs blocs. Tout en haut, la barre de titre, qui change selon vos usages et qui affiche votre présence tout à droite (Online, Offline, etc) ainsi que l'état d'une session de discussion (chiffrée ou non, session OTR authentifiée ou non, etc).

Au milieu, des messages d'activités (connexion, notification de réception de message, configuration). En bas, une fenêtre de statut, avec l'heure, l'utilisateur loggué et l'état des différentes fenêtres de Profanity. Attention, cette fenêtre n'affichera pas vos conversations, elle se contente d'afficher les messages relatifs à vos intéractions (discussion entrante, ajout de contact et toutes ces choses).

* Se connecter

Simple: `/connect vous@domaine.tld`, Profanity devrait vous demander votre mot de passe, bien évidemment la frappe est cachée, validez avec la touche Entrée.

* Papoter et jongler entre les fenêtres

Si vous avez déjà des contacts, un simple `/msg contact@domain.tld` lance une nouvelle fenêtre de discussion avec votre contact. Si vous êtes déjà dans la fenêtre de conversation, inutile de précéder votre message avec `/msg`. Pour passer de fenêtre en fenêtre, plusieurs options :

  * Les raccourcis `Alt 0`, `Alt 1`, etc
  * Les touches de fonctions de `F1` à `F10`
  * Les raccourcis `Alt + flèche droite` ou `Alt + flèche gauche`
  * Avec la commande `/win X`, X étant le numéro de la fenêtre à afficher (disponible en bas à droite de l'écran)

Attention, ça dépend aussi de votre terminal et des raccourcis propres au terminal.

Pour fermer une fenêtre, tapez simplement `/close` dans la fenêtre à fermer.

### Gestion des contacts

Profanity se base sur un système de liste (roster). Pour ajouter un contact, c'est la commande `/roster add contact@domain.tld`. Relativement simple, on ajoute (_add_) à la liste (_roster_) le contact. Si on souhaite supprimer un contact, c'est évidemment la commande `/roster remove contact@domain.tld`. Il est bon d'ajouter des gens, mais il est encore mieux de savoir quelle est leur présence (en ligne, déconnecté, occupé, etc). On va donc "s'abonner" à leur présence, ce qui nous permettra d'être notifié quand ils se connectent ou se déconnectent.

Nous utilisons la commande `/sub request contact@domain.tld`. _sub_ comme "subscription" (abonnement en français), _request_ comme requête. Littéralement, on s'abonne à leur présence. Pour approuver les demandes de personnes qui souhaitent s'abonner à notre présence, c'est la même commande de base, avec quelques changements. On autorise l'abonnement du contact qui demande, via la commande `/sub allow contact@domain.tld`. Pour annuler une autorisation, c'est la commande `/sub deny contact@domain.tld`

Pour lister les demandes en attentes, c'est la commande `/sub sent` pour voir les demandes envoyées et `/sub received` pour les demandes reçues.

Avoir les JID complets des contacts c'est bien mais c'est un peu moche. On va renommer les contacts pour voir apparaitre uniquement _Contact_ et non plus _contact@domain.tld_. Là encore, c'est la gestion de la liste, donc la commande sera basée sur `/roster` avec quelques paramètres: `/roster nick contact@domain.tld Contact`

Au final, c'est un simple alias.

En cas de faute de frappe, on peut réinitialiser le pseudo attribué à la personne avec la commande `/roster clearnick contact@domain.tld`

#### Un récapitulatif des commandes pour la gestion des contacts

 * Ajouter un contact : `/roster add contact@domain.tld`
 * Supprimer un contact : `/roster remove contact@domain.tld`
 * Renommer un contact : `/roster nick contact@domain.tld Contact`
 * Enlever le nom donné à un contact : `/roster clearnick contact@domain.tld`
 * S'abonner à sa présence : `/sub request contact@domain.tld`
 * Voir les demande d'abonnement envoyées : `/sub sent`
 * Voir les demande d'abonnement reçues : `/sub received`
 * Accepter une demande d'abonnement : `/sub allow contact@domain.tld`
 * Refuser une demande d'abonnement : `/sub deny contact@domain.tld`

Toutes ces commandes sont plutôt logiques et bien hiérarchisées.

### Thème de base

Le thème graphique de base est efficace, mais peut-être pas à votre goût. Profanity embarque déjà des thèmes, libre à vous d'en choisir un !

Pour lister les thèmes disponibles: `/theme list`. Si vous voulez activer un thème présent dans la liste: `/theme load <nom-du-theme>`. Le changement est immédiat.

### Off The Record

Comme vous vous souciez du caractère privé de vos conversations, vous souhaitez forcément activer OTR avec vos correspondant. Si ce n'est pas le cas, je vous invite **fortement** à le faire... La gestion est enfantine. En premier lieu il vous faut générer une clef privée. La commande `/otr gen` vous le permet. Profanity devrait vous gratifier d'une ligne du type `Private key generation complete.` Pour afficher l'empreinte de cette clef, c'est la commande `/otr myfp`.

Bien, maintenant que nous avons ce qu'il faut, initialisons une discussion avec quelqu'un.

Simple, on veut démarrer une session OTR avec un contact, c'est la commande: `/otr start contact@domain.tld`. Vous pouvez vous passer du JID du contact si vous êtes déjà dans une fenêtre de conversation avec lui. Certes la conversation est chiffrée, mais votre contact n'est pas authentifié. Ceci est montré dans la barre de titre via l'information `[untrusted]`.

L'authentification fonctionne sur un système de question/réponse. Je vais poser une question à laquelle seul mon contact peut répondre. Evitez les questions du type "2+2 ?" qui ne vérifie en rien l'identité de la personne. La commande est là aussi assez simple, elle est de la forme suivante: `/otr question "La question ?" réponse`.

Exemple nul: `/otr question "Quel est le nom de mon cochon d'Inde ?" Bernie`

Une fois ceci envoyé, une ligne de la sorte est affichée dans la conversation: `Awaiting authentification from contact@domain.tld...` Si le contact répond correctement à la question (donc en indiquant "Bernie"), Profanity vous l'indique avec deux lignes dans la fenêtre de conversation:

`Authentication successful.`
`OTR session trusted`

Et la barre de titre affiche fièrement un `[trusted]` comme quoi le contact est bien authentifié. Pour consulter la fingerprint du contact: `/otr theirfp`

Et si vous recevez une demande d'authentification ? Cela se présente de cette manière :

`contact@domain.tld wants to authenticate your identity with the following question:`
`   Quel est la couleur de ton vélo ?`

Il vous suffit de répondre à la question avec la commande suivante: `/otr answer rouge`

Pour arrêter une session OTR, c'est la commande `/otr end`

#### Un récapitulatif des commandes pour la gestion d'OTR

* Générer une clef privée: `/otr gen`
* Afficher la fingerprint de la clef: `/otr myfp`
* Démarrer une session OTR avec un contact: `/otr start contact@domain.tld` (ou `/otr start` si vous discutez déjà avec lui)
* Arrêter une session OTR: `/otr end`
* Répondre à la question d'une demande d'authentification OTR: `/otr answer REPONSE`
* Consulter la fingerprint du contact: `/otr theirfp`

Petite information en passant, lors d'une conversation sans OTR, tous les messages sont précédés du caractère `-` dans la fenêtre de discussion. Pendant une session OTR, ce `-` est remplacé par un `~`. Si toutefois vous voulez le changer, c'est la commande `/otr char <carac>`. Personnellement j'utilise la commande `/otr char #` pour afficher un croisillon sur mes sessions OTR.

Aussi, si vous souhaitez gérer si les sessions OTR sont journalisée ou non, il existe la commande `/otr log`. Avec 3 paramètres:

* `on`: Les messages OTR seront enregistrés, en clair.
* `<texte>`: Les message OTR seront enregistrés mais le contenu chiffré sera remplacé par `<texte>`.
* `off`: Les messages OTR ne seront pas enregistrés.

Attention à activer ou non la journalisation des message via la commande `/chlog on|off` !

### PGP

Pour le chiffrement via PGP, c'est presque le même principe qu'OTR.

Il faut en premier lieu assigner une clef PGP à votre compte. On peut lister les clefs présentes sur le système via la commande `/pgp keys`.

Une fois la clef identifiée, on l'assigne au compte en passant la keyID (et non pas le fingerprint hein !): `/account set moi@mondomaine.tld pgpkeyid XXXXXXXXXXXXXXXX`

Concernant vos contacts, deux solutions: Soit ils signent déjà leur présence et à ce moment vous n'avez rien à faire, soit ils ne la signent pas et à ce moment-là vous devez leur assigner une clef (publique, évidemment).

S'ils ne signent pas leur présence, assignez leur une clef publique avec cette commande (attention, toujours la keyID et non pas la fingerprint): `/pgp setkey contact@domain.tld XXXXXXXXXXXXXXXX`.

Pour consulter tous les contacts qui ont une clef publique d'assignée (soit par vous, soit par eux-mêmes), utilisez la commande `/pgp contacts`

Débutons une conversation chiffrée avec PGP, comme toutes les clefs ont été assignées avant, vous n'avez rien à faire à part un `/pgp start contact@domain.tld`, ou un simple `/pgp start` si vous discutiez déjà avec cette personne. C'est la commande `/pgp end` pour mettre un terme à la session chiffrée.

Là encore, pour modifier le caractère affiché lors d'une session PGP, c'est la commande `/pgp char P`. Vos lignes de conversations chiffrées via PGP seront précédées d'un `P` au lieu du `~` par défaut. Les fonctions de logging des messages sont aussi les mêmes: `/pgp log` avec les arguments au choix: `on | [texte] | off` !

#### Un récapitulatif des commandes pour la gestion de PGP

* Lister les clefs présentes sur le système: `/pgp keys`
* Assigner une clef à votre compte: `/account set moi@domain.tld pgpkeyid XXXXXXXXXXXXXXXX`
* Assigner une clef à un contact: `/pgp setkey contact@domain.tld XXXXXXXXXXXXXXXX`
* Lister les contacts ayant des clefs associées: `/pgp contacts`
* Démarrer une session chiffrée via PGP: `/pgp start contact@domain.tld` (ou `/pgp start` si vous discutez déjà avec ce contact)
* Arrêter la session chiffrée: `/pgp end`


Plutôt facile non ?

### Fin

Un merci à Feliwyn qui m'a mentionné ce logiciel. J'étais parti en quête d'un serveur XMPP pour me créer un JID, j'ai trouvé le service chez [Blah.im](https://blah.im/server.html) qui a le bon gout de proposer du STARTTLS uniquement, ainsi que du PFS forcé sur les connexions SSL/TLS. Inscription simple, rapide et efficace sans laisser une seule info (mail ou autre) !
