---
authors: ["zilkos"]
title: 'Le retour du retour du retour'
date: 2018-03-03
lastmod: 2018-11-18
categories: ["Unixmail"]
toc: true
---

_Mise à jour du 5 mars 2018:_

  * _Le flux [RSS](/blog.rss) est de retour, avec la même URL qu'avant_
  * _Les menus sont retravaillés_

Bien le bonjour !

Et non, Unixmail n'est pas mort. Certes il a passé un certain temps dans un état végétatif, pour diverses raisons, mais il est (encore) de retour.

## Encore ?

Oui, encore. Il a failli être complètement supprimé, mais un peu de raison et de bon sens ont fini par le garder en ligne. Une fois ma situation stabilisée et une injection de motivation (voyez ça comme un tout), j'ai décidé de reprendre les choses en main et de reprendre un rythme "normal" pour continuer à alimenter ce bout de toile comme il se doit. Sauf qu'un bout de toile comme celui-ci au final j'en avais deux. Un pour la musique et un pour l'informatique (avec une sous-partie pour quelques histoires). Maintenir deux blogs, si minimes soient-ils est agaçant. Le markdown utilisé pour écrire les articles n'étaient pas le même, les architectures étaient radicalement différentes, la mise en page également. Tellement agaçant que la partie musique était à l'abandon depuis bien plus longtemps que le "vrai" Unixmail.

## Fusion

Dans le coup j'ai tout réuni sur une seule plateforme pour bénéficier d'un socle commun qui me permet de tout gérer d'un seul point. Les parties musique, informatique et histoire sont maintenant sur le même moteur, le même domaine, le même [dépôt git](https://gitlab.com/Zilkos/blog). Plus de souplesse, plus de cohérence, aussi bien pour vous que pour moi.

Cette fusion a comme à chaque fois entrainé un profond ménage dans la base d'article qui a grandement diminuée. 41 articles étaient présents sur l'ancienne version (sur les deux blogs cumulés), pour seulement 28 aujourd'hui. Des articles vieux, hors-propos ou tout simplement mauvais sont supprimés. Tous les articles ont été revues, corrigés et souvent reformulés dont beaucoup sur la partie musique (quasiment tous en fait). Si toutefois vous cherchez des articles anciens, j'ai un backup du dépôt git bien évidemment.

## _What's up doc' ?_

### Contenu

Je reste toujours sur mon idée de blog à base de fichiers plats, donc sans base de données. J'ai enlevé la _sidebar_ inutile qui n'apportait rien à part un ennui visuel. L'architecture est modifiée, dans le sens où le blog est généré statiquement et uniquement les fichiers plats sont poussés sur l'espace web, là où avant le moteur était intégré dans l'espace web. Ce qui fait que l'intégralité du blog actuel tient sur 4 Mo tout compris (contenu, image, styles, etc).

Pour être cohérent avec toi, visiteur, je me suis affranchi de tout CDN ou chargement de style distant. Ainsi, à la génération du blog les feuilles de styles sont compilées et poussées statiquement sur l'espace web local et non plus chargée depuis un CDN distant. Idem pour le _processing_ markdown des articles, c'est fait hors de l'espace web, seul la version html calculée est poussée. Ca permet de garder un site rapide, peu gourmand en ressources et cela permet également de ne pas mettre en péril votre hygiène numérique en appellant des domaines tiers.

Comme indiqué, le contenu est séparé en "catégories" au nombre de 3 pour l'instant:

  * Informatique

Plutôt clair, ça cause informatique au sens large.

  * Musique

Les articles du blog musique qui ont été réécrits, retravaillés. Il n'y en a pas énormément pour l'instant, mais rassurez-vous, ça va changer rapidement...

  * Histoire

C'est le contenu indiqué comme "Histoire" sur l'ancienne version d'Unixmail. Il est fort probable que cette catégorie soit renommé dans l'avenir car à la relecture des articles de cette catégorie, je me rends compte que c'est pas tant des histoires mais surtout des points de vue et des réflexions sur diverses choses. J'ai bien envie de creuser ça car j'ai quelques réflexions à vous partager sur divers sujets. A voir par la suite donc mais ces réflexions ne sont peut-être pas à catégoriser comme des "histoires", peut-être plus comme des "pensées", je ne sais pas encore.

Côté mise en page, on garde un côté responsive pour les utilisateurs de périphériques mobiles, du code coloré, des images et toute l'étendue des possibilités qu'offre une rédaction en Markdown et même un peu plus grâce à moteur de blog, comme une table des matières disponible en début d'article.

La barre de recherche est supprimée et remplacée par une page de [tags](/tags). J'ai pris un soin tout particulier a correctement tagguer tous les articles présents pour que ce système soit le plus pertinent possible.

### Communauté

Une nouvelle page est arrivée, la page [communauté](/communaute), elle résume les différents moyens d'interactions entre les visiteurs, mais aussi comment contribuer à ce site.

Vous pouvez venir écrire, hormis le fait de le dire j'ai mis des choses en place pour que ce soit plus simple, à savoir:

  * Des choix pour les auteurs sur la licence du contenu
  * Un système de page d'auteur qui résume le contenu pour chaque auteur
  * Un sous-dépôt git pour gérer vos articles moyennant un compte Gitlab
  * Une complète liberté sur les améliorations ou corrections proposée par les lecteurs sur votre dépôt

Si certains auteurs souhaitent écrire ici, venez, c'est ouvert. À terme il est probable qu'une mailing list dédiée aux auteurs ouvre s'ils en ressentent le besoin.

J'ai également mis en place un système de don parce que j'ai trouvé une plateforme libre et respectueuse pour ceux qui veulent éventuellement contribuer financièrement.

## Bienvenue

En bref, j'espère que cette nouvelle version vous plait. J'ai déjà une petite feuille de route pour les articles à venir, aussi n'hésitez pas à soumettre vos idées ou même vos ébauches. Les mailing list sont ouvertes et n'attendent que vos retours, vos idées, ou vos points de vue sur cette nouvelle version mais aussi sur les autres sujets que vous souhaitez, parce qu'au final vous êtes **libres**.

Et être libre, c'est tout ce qui compte.
