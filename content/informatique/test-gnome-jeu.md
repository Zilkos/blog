---
authors: ["zilkos"]
title: 'Jouons sous Linux !'
date: 2018-05-27
lastmod: 2018-11-18
categories: ["Linux", "Expériences"]
toc: true
tags: ["Gnome", "Jeu", "Lutris"]
---

Vous me connaissez, en terme d'environnement de bureau je suis plutôt difficile. En fait j'utilise plus un gestionnaire de fenêtre qu'un environnement de bureau. Tout simplement parce que c'est plus léger, configurable et adaptable, à défaut d'être un peu rebutant pour les débutants. La mort dans l'âme et avec la ferme intention de formater mon PC de jeu, je me suis dit que j'allais en profiter pour tester un vrai environnement de bureau grand-public. Voyons ça.

# Le but ?

Tester plusieurs choses :

  - Un environnement de bureau "classique"
  - La possibilité de jouer sous Linux

Donc je n'ai pas cherché à comprendre, j'ai pris la solution *michu-compliant* à savoir un bon vieux Ubuntu 18.04 avec un Gnome-shell par défaut.

L'installation on ne peut plus classique avec les éternelles étapes de gestion du disques, chiffrement, utilisateurs et tout le reste suffisamment simple même pour un utilisateur de Facebook.

## Impressions

Gnome-shell, ça fonctionne. Mais par contre, ça prend énormément de place à l'écran. Venant d'un bureau i3 le "dock" ou la barre dégueulasse sur la gauche qui prend une place **monstrueuse**, impossible de la supporter. Donc bien évidemment, obligé de modifier un peu tout ça.

En premier lieu, installer `gnome-tweaks` pour modifier les comportements et affichages par défaut, rajouter des raccourcis clavier pour éviter trop de clic. Ensuite on passe aux extensions, notamment une super importante : [dash to dock](https://micheleg.github.io/dash-to-dock/release/2018/04/02/new-release-v63.html). Le lien est volontairement vers le Github de l'auteur, vu que le site des extensions Gnome ne propose que la version pour Gnome-shell 3.26 et que celle d'Ubuntu actuellement est la 3.28.

En bref, ça permet d'avoir un dock supportable en bas de l'écran (ou où vous voulez), plutôt discret avec un masquage intelligent.

Faites donc un tour sur le site des extensions [Gnome-shell](https://extensions.gnome.org/) pour trouver votre bonheur et palier ce que vous n'aimez pas.

On aime ou on n'aime pas, mais Gnome-shell vient avec sa propre logique de ce que doit être un bureau "ergonomique", personnellement je n'aime pas, mais avec quelques bidouilles on arrive à peu près à quelque chose de confortable. Cette barre en haut de l'écran qui est peu utile. Quand vous lancez un logiciel, vous avez donc deux barres, la *top-bar* d'Ubuntu et la barre de l'application, beaucoup de perte de place pour **aucun** intérêt.

En bref, faut pas mal de choses pour gommer tous ces petits défauts mais c'est faisable.

Le thème. Faut vraiment dégager le thème par défaut qui est juste immonde. Installez donc `arc-theme` qui est bien plus agréable.

## On plug le téléphone ?

Sous KDE, il existe KDEConnect, qui permet de faire diverses choses avec votre téléphone sous Android. Contrôler le PC à distance (pour la musique, ou autre), avoir les notifications bidirectionnelles, envoyer des fichiers, avoir un *clipboard* partagé, envoyer des SMS, etc.

J'ai donc commencé à chercher un équivalent Gnome, parce que pourrir la bête avec huit tonnes de bibliothèque KDE non merci.

J'ai trouvé deux solutions, donc une qui pue :

### MConnect

Mconnect c'est une implémentation du protocole KDE Connect en Vala/C avec du libnotify pour les notifications. Un démon `mconnect` et un client D-Bus `mconnectctl`. Bref un joli bazar de bibliothèque qui marchouille vite fait, mais qui marchouille.

### GSConnect

GSConnect c'est aussi une implémentation du protocole KDE Connect... en JS. Gnome-shell 3.24 ou supérieur, sshfs, intégration Nautilus, Gsound.

Ça fonctionne très bien, à ma grande surprise. Bien plus rapide, léger, aucun problème.

C'était juste pour tester, déjà qu'un téléphone me gonfle, si en plus c'est pour qu'il vienne m'embêter jusque sur mon PC, rien ne va plus.

## Au final

Pour quelqu'un de non informaticien, c'est plutôt joli. Même si par défaut, certains choix sont discutables. Je n'ai pas pris assez de recul pour savoir si quelqu'un de non initié pouvait se plonger dans les extensions Gnome sans prendre peur, mais c'est plus ou moins nécessaire à mon sens, non pas pour effacer certaines choses mais surtout pour augmenter la possibilité de personnalisation de l'environnement.

Ça fonctionne bien, reste à voir comment ça se passe sur des configurations plus petites.

# Jouons ?

J'ai donc décider d'essayer de jouer sous cet environnement. La config, plutôt classique pour une config de jeu :

  - Intel Core™ i5-6400 CPU @ 2.70GHz × 4
  - GeForce GTX 970
  - 8Go de RAM
  - Disque SATA3 classique

Le point primordial, c'est bien évidemment les pilotes graphiques, Nvidia dans mon cas.

## Pilotes graphiques

Bon c'est un peu le bazar pour s'y retrouver mais au final ce n'est pas très compliqué. Il faut juste connaître l'existence d'un PPA dédié à ça, et une commande.

Déjà, un petit `lspci | grep VGA` peut être utile:

```sh
$ lspci | grep VGA
01:00.0 VGA compatible controller: NVIDIA Corporation GM204 [GeForce GTX 970] (rev a1)
```

Le PPA en question est officiel et contient des drivers propriétaires et testés.

On l'ajoute comme ceci:

```sh
sudo add-apt-repository ppa:graphics-drivers/ppa
```

Oui, mais quel driver choisir ? Tout simplement en lançant la commande qui va vous le dire et qui est `ubuntu-drivers devices`, ça se passe comme ceci:

```sh
$ ubuntu-drivers devices
== /sys/devices/pci0000:00/0000:00:01.0/0000:01:00.0 ==
modalias : pci:v000010DEd000013C2sv00001019sd00001029bc03sc00i00
vendor   : NVIDIA Corporation
model    : GM204 [GeForce GTX 970]
driver   : nvidia-driver-390 - third-party free
driver   : nvidia-driver-396 - third-party free recommended
driver   : xserver-xorg-video-nouveau - distro free builtin
```

J'ai donc trois solutions :

  - `nvidia-driver-390`
  - `nvidia-driver-396`
  - `xserver-xorg-video-nouveau`

Comme je suis bête et discipliné, j'opte pour le `nvidia-driver-396`.

J'ouvre donc le soft qui me permet de gérer mes pilotes graphiques qui se trouve dans l'endroit le plus logique du monde: LE GESTIONNAIRE DE SOURCES DE LOGICIEL.

{{% center %}}
![Pilotes additionnels](/static/img/informatique/driver.png)
{{% /center %}}

Un petit reboot avec une prière et c'est bon.

Pour être sûr que tout se passe et surtout par nostalgie on installe le paquet `mesa-utils` et on lance `glxgears` pour vérifier que les FPS soient corrects (60 constant dans mon cas).

## Support Vulkan

C'est quelque chose que je ne connaissais pas plus que ça, mais dans le monde du jeu et de la 3D, tout le monde connait au choix :

  - Directx (9, 11)
  - OpenGL

Sous Linux on préfère OpenGL tout simplement parce que DirectX émane de Microsoft et que c'est une saloperie de purge à faire fonctionner, du moins il y a quelques années.

Sauf que maintenant, il y a Vulkan. Vulkan c'est une API graphique qui a le mérite de fonctionner partout, à savoir Windows, Linux, Mac, Android, iOS, c'est supporté par Nvidia et AMD, c'est compatible avec tous les systèmes qui supportent OpenGL ES 3.1. La version actuelle est la 1.1 puisque c'est un projet relativement récent (février 2016).

Sous Linux, c'est du côté de `mesa-vulkan-drivers` qu'il faut aller voir et également `vulkan-utils`.

Dans le paquet `vulkan-utils` on trouve deux commandes sympathiques:

`vulkaninfo` qui vous affichera plein d'informations concernant Vulkan et votre système:

```sh
===========
VULKAN INFO
===========

Vulkan Instance Version: 1.1.73
```

Attention le retour est trèèès long. Il vient également avec un outil de test, nommé `vulkan-smoketest` qui vous affichera une animation psychédélique et une sortie en FPS un peu à la `glxgears`.

## On résume

On a des drivers pour jouer (parce que en vrai `nouveau` juste pour le bureau et la vidéo c'est parfait hein). On a un support Vulkan.

Bien, manque plus que les jeux.


# On joue à quooooiiiii ?

Personnellement j'adore les RPG et quelques jeux de tirs "arcade" pour me détendre/défouler. Seul soucis, ce sont des jeux non natifs Linux. Bien pour ça que j'avais un PC sous Windows uniquement dédié à ça...

Sous Linux, pour émuler ce genre de jeux on a plusieurs solutions :

  - Wine pour émuler
  - PlayOnLinux qui simplifie le tout
  - ./play.it qui fonctionne bien
  - Sans doute d'autres que je ne connais pas

Et enfin...

## Please welcome Lutris !

[Lutris](https://lutris.net/) est un gestionnaire de jeu pour Linux qui supporte énormément de plateformes et d'émulateurs. Il permet aussi d'installer des jeux de différentes plateformes en ajout manuel.

Installons donc Lutris, le mieux étant de passer par le PPA dédié, tout est expliqué sur la [page du projet](https://lutris.net/downloads/) selon votre distribution.

Une fois installé il suffit d'aller chercher votre jeu dans l'immense bibliothèque des jeux supportés (pas loin de 6000) et surtout de bien lire la description car il y a parfois quelques surprises.

Allez on tente !

## Exemple: World of Warcraft

Jeu historique s'il en est, c'est là où j'ai rencontré des gens formidable dont certains qui sont devenus, au choix:

  - Des amis fidèles
  - Des amis courants
  - Ma femme

Lutris est donc installé si vous avez suivi la page du projet expliquant l'ajout du PPA. Vous aurez également remarqué qu'il installe Vulkan au passage. Attention, pour WoW, c'est DirectX 11 uniquement (le DirectX 9 va être abandonné).

Allons donc sur la [page dédiée à l'installation](https://lutris.net/games/world-of-warcraft/) qui nous invite chaleureusement à aller voir la [page wiki du jeu](https://github.com/lutris/lutris/wiki/Game:-World-of-Warcraft) indiquant que le jeu vient avec Battle.net et que donc, il faut également aller voir la page dédiée à [Battle.net](https://github.com/lutris/lutris/wiki/Game:-Blizzard-App).

Pfiou. Sur cette page on trouve la liste des [dépendances](https://github.com/lutris/lutris/wiki/Game:-Blizzard-App#system-dependencies-for-fixing-font-rendering-errors) à installer. On installe tout ce bazar puis on se rend sur la [page dédiée à l'installation](https://lutris.net/games/world-of-warcraft/) et enfin, on clique sur "Install".

Lutris se lance et installe une version spécifique de Wine avec des configurations dédiée à ce jeu. Suivez l'installateur et faites ce qu'il vous demande (au final, pas grand chose). Une fois installé, lancez-donc le jeu :

{{% center %}}
![Pilotes additionnels](/static/img/informatique/battlenet.png)
{{% /center %}}

J'ai volontairement mis la sortie d'un `uname -a` en haut à droite pour indiquer que c'est bien sous Linux que ça se passe.

Idem sur la capture suivante prise en jeu :

{{% center %}}
[![WoW](/static/img/informatique/wow.png)](/static/img/informatique/wow.png)
{{% /center %}}

Ensuite, il suffit d'installer le jeu ou de copier une installation existante dans le _Program Files_ situé dans `/home/zilkos/Games/world-of-warcraft/drive_c`.

Je vous conseille fortement de supprimer votre dossier WTF pour repartir sur des réglages par défaut. Personnellement je ne l'ai pas fait, parce que j'étais plutôt confiant.

Au final, peu de souci.

Un, à la rotation de la caméra ça sautait un peu, complètement réglé par le paramétrage suivant, dans WTF\Config.wtf, rajoutez:

```
SET gxTextureCacheSize "600"
```

Ca permet d'allouer de la mémoire à la carte graphique pour mettre des textures en caches, avec ça, plus aucun souci de caméra, attention la valeur varie selon votre carte, prenez la moitié de la mémoire de votre carte pour commencer et augmenter / baisser pour ajuster.

Pour réduire vos temps de chargement, ajoutez dans le même fichier :

```sh
worldPreloadNonCritical 0
```

Faites mumuse avec les paramètres graphiques du jeu mais globalement il n'y a rien à modifier, optez pour un jeu fenêtré plein écran (ce qui permet des alt tab instantanés).

Je dispose de la même qualité graphique et les mêmes performances que sous Windows à savoir 100 FPS en zone "calme", 70 / 80 en raid 25 avec un niveau de qualité positionné à 7, le tout avec DirectX 11.

Pas de problème de son, ni de texture ou autre, tout est nickel, même au lancement de Battlenet.


## Ça fonctionne

Et ça fonctionne bien, c'est pas un délire de Linuxien qui accepte un jeu qui lag à 30fps quand le personnage bouge. Non, ça **fonctionne**.

J'ai également installé Overwatch qui a fait couler beaucoup d'encre sur les forums Lutris car il n'est pas encore totalement supporté, personnellement ça passe après quelques bidouilles :

  - Mettez tout en "Low"
  - Mettez la même résolution que votre desktop
  - Mettez le même ratio que votre desktop (16:9, 16:10, etc)

Tout en bas graphisme ça tourne à 70 fps constant, j'essaierai de monter un peu pour voir ce que cela donne.

D'autres personnes font tourner d'autres "gros" jeux du type Skyrim, The Witcher sans problème avec une configuration matérielle similaire à la mienne via Lutris.

Pour les gens sous Steam, il y a une version native sous Linux qui permet de jouer à tous les jeux Steam OS, soit 80% des jeux dans le top 10 Steam. Pour le reste, Lutris peut faire tourner certains jeux Steam Windows.


# Au final

Lutris est vraiment excellent car il permet de jouer sans trop se prendre la tête, suffit de suivre la page wiki quand il y a une et de dérouler. A la rigueur en cas de problème, le forum est super réactif.

Donc _"Linux sapu on pe pa joué lol"_ est maintenant faux. Il y a quelques années c'était plutôt vrai hormis quelques rares jeux natifs.

Un bureau _michu-compliant_, des jeux installables facilement via Lutris ou Steam, des logiciels comme Discord ou autre disponibles dans la logithèque Ubuntu. Avec l'arrivée de Vulkan qui va permettre des performances encore augmentées nativement sous Linux, tout ceci est de bonne augure pour nos systèmes libres (violées avec des jeux proprio).

Là encore, tout est question de besoin. Actuellement même si ça fonctionne correctement c'est pas encore super simple mais c'est déjà largement mieux qu'il y a quelques années.

C'est donc à surveiller de près !

# Liens

Pour aller plus loin sur le sujet, quelques liens en vrac :

  - [Lutris](https://lutris.net/)
  - [dxvk](https://github.com/doitsujin/dxvk)
  - [Linux Gaming sur Reddit](https://www.reddit.com/r/linux_gaming/)
  - [WineHQ](https://www.winehq.org/)
